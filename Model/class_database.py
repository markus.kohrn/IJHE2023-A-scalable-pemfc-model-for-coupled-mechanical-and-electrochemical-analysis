# -*- coding: utf-8 -*-
"""
Created on Fri Oct 29 14:24:21 2021

@author: Markus Kohrn
         RWTH Aachen University, Germany        

"""

from __future__ import division, absolute_import, \
                       print_function#, unicode_literals

# %matplotlib qt

                       
'''
===============================================================================
                    Imports
===============================================================================
'''


''' import standard libraries'''                   
from contextlib import contextmanager
import os, sys, io, tempfile
import math
import numpy             as np
import matplotlib.pyplot as plt
import matplotlib        as mpl
import multiprocessing   as mp
import copy
import time
import pickle
from   scipy             import optimize
from   scipy.interpolate import interp1d
from   scipy.ndimage     import gaussian_filter1d

''' imoprt own function classes '''

import MultiPhysModel.MP_main as MP_main
import MultiPhysModel.plot    as MP_plot
import MultiPhysModel.plot_paper as MP_plot_paper
import AnalytModel.AM2_main   as AM_main
import AnalytModel.AM2_plot   as AM_plot


'''
===============================================================================
                   Class
===============================================================================
'''

dbFolderStr = "DataBase"
 
class database:
    
    def __init__(self, createNewDatabase=False):
        
        
        if createNewDatabase:
            self.dataList = self.create_newDataBase()
        else:          
            try: # try: load database
                self.dataList = self.load_dataBaseFile()
            except: # create new database
                self.dataList = self.create_newDataBase()
                
        self.maxIdx = self.nrOfRes()
                
        #========================================================
        # attribute lists
        
        self.keyList_AMres = ['am_xNodes_reg', 
                              'am_xNodes_hom', 
                              'am_yNodes',
                              'gc_threshold_relChange',
                              
                              'd_GDLa', 'd_GDLc',
                              'd_CLa',  'd_CLc',
                              'l_RIBa', 'l_RIBc', 
                              'l_CHAa', 'l_CHAc', 
                              'R_RIBa', 'R_RIBc',
                              
                              'Del_H0', 
                              
                              'Ey_GDL','Gxy_GDL', 
                              'k_Timo',
                             ]
        
        self.keyList_SMres = ['sm_threshold_Fmean', 
                              'sm_threshold_Fmax',
                              
                              'yMinElemsPerLayer', 
                              'yyMaxRatio', 
                              'xyMaxRatio',
                              
                              'Ey_CL',  'Ey_PEM', 
                              'Gxy_CL', 'Gxy_PEM', 
                             ]
            
        self.keyList_MPres = ['mp_threshold_curBalRel_Mean', 
                              'mp_threshold_curBalRel_Max',
                                                  
                              'zElemLen',
                              
                              'T', 
                              'OCV_fract', 
                              's_clamp_const', 
                              'p_tot_a', 'p_tot_c',
                              'RH_ano',  'RH_cat',
                              'x_H2',    'x_O2',    'x_N2',
                              
                              'f_por0_GDLa','f_por0_GDLc',
                              'f_por0_CLa', 'f_por0_CLc', 
                              'f_ion0_CLa', 'f_ion0_CLc',
                              
                              'contAngle_CLa',  'contAngle_CLc',
                              'contAngle_GDLa', 'contAngle_GDLc',
                              
                              'D_H2_H2O', 'D_N2_H2O', 'D_O2_H2O', 'D_N2_O2', 
                              'M_W',      'dens_Wl',
                              'dens_PEM', 'EW_PEM',   'N_H2O_per_SO3',
                              'Ry_surfTransChem',
                              'T_ref',
                              
                              'm_Brug_x', 'm_Brug_y', 
                              'elCon_GDL_bulk',
                              
                              'z_HOR', 'E0_HOR_ref', 'ECSA_HOR', 'L_Pt_HOR', 'i2_ex0ref_HOR', 'beta_HOR', 'Q_HOR', 'ctc_HOR', 
                              'z_ORR', 'E0_ORR_ref', 'ECSA_ORR', 'L_Pt_ORR', 'i2_ex0ref_HOR', 'beta_ORR', 'Q_ORR', 'ctc_ORR',
                             ]

        return None

#==============================================================================                
        
    def store_dataBaseFile(self):
        "store whole dataBase to file"
        fileName = os.path.join(dbFolderStr, "_dataBase")  
        with open(fileName, 'wb') as file:
            pickle.dump(self.dataList, file)
        print("-> database successfully stored")
        return True

#===========================    
    
    def store_resultFile(self, para, setIdx, AMres, MPres, OCV_fract_list):
        "store set of result data into individual file"
        fileName = os.path.join(dbFolderStr, "resFile_%d" %setIdx)      
        dataSet  = [para, setIdx, AMres, MPres, OCV_fract_list]
        
        with open(fileName, 'wb') as file:
            pickle.dump(dataSet, file)
        return True

#==============================================================================                
        
    def load_dataBaseFile(self):
        "load database from file"
        fileName = os.path.join(dbFolderStr, "_dataBase") 
        with open(fileName, 'rb') as file:
            dataList = pickle.load(file)
        print("-> existing database loaded")
        return dataList

#===========================    
    
    def load_resultFile(self, setIdx):
        "laod set of result data from individual file"        
        fileName = os.path.join(dbFolderStr, "resFile_%d" %setIdx)        
        with open(fileName, 'rb') as file:
            dataSet = pickle.load(file)
        return dataSet

#===========================    

    def create_newDataBase(self):
        "create a empty database (=empty list)"
        dataList = list()
        print("-> new database created")
        return dataList

#==============================================================================                
        
    def add(self, para, AMres, MPres, OCV_fract_list):
        "add result data set to database and store"
        
        if self.doesExist_MPres(para):
            _, setIdxOld, _, MPresOld, OCV_fract_listOld = self.getSet(para)
            
            MPres          = sorted(MPresOld       + MPres      )
            OCV_fract_list = sorted(OCV_fract_listOld + OCV_fract_list)
            
            setIdx = setIdxOld
            
        else:
            self.maxIdx += 1
            setIdx = self.maxIdx
            
        # store dataBaseFile
        newSet   = [para, setIdx, OCV_fract_list]
        self.dataList.append(newSet)
        self.store_dataBaseFile()
        
        # store result file
        self.store_resultFile(para, setIdx, AMres, MPres, OCV_fract_list)
        
        return True

#==============================================================================                
        
    def nrOfRes(self):
        "Returns number of results in database"
        return len(self.dataList)

#============================================================================== 
  
    def doesExist_AMres(self, paraNew):
        'Bool: is the general AM result already in database?'
        return self.__getX(paraNew, "AMexist")

    def doesExist_AMresSM(self, paraNew):
        'Bool: is the mp-specific and smootehd AM result already in database?'
        return self.__getX(paraNew, "SMexist")
    
    def doesExist_MPres(self, paraNew):
        'Bool: is the final MP result already in database?'
        return self.__getX(paraNew, "MPexist")    

#================================
      
    def getAMres(self, paraNew):
        "return AM result: allRegs"
        return self.__getX(paraNew, "AMres")

    def getMPres(self, paraNew):
        "return MP result: MP_res_dict"
        return self.__getX(paraNew, "MPres")

    def get_OCV_fract_list(self, paraNew):
        "return MP result: MP_res_dict"
        return self.__getX(paraNew, "OCV_fract_list")
    
    def getSet(self, paraNew):
        "return whole result data set"
        return self.__getX(paraNew, "set")

#================================

    def __getX(self, paraNew, whatToGet):
        "executes all get and doesExist functions"
        
        # check always
        attrToBeCompared_list = copy.deepcopy(self.keyList_AMres)
        
        # check if smoothed AMres or higher     
        if whatToGet in ["SMexist", "MPexist", "MPres", "set", "OCV_fract_list"]:
            attrToBeCompared_list += self.keyList_SMres
            
        # check if complete MPres    
        if whatToGet in ["MPexist", "set", "OCV_fract_list"]:
            attrToBeCompared_list += self.keyList_MPres
            
        
        # loop over all known results
        for paraOld, setIdxOld, OCV_fract_listOld in self.dataList:
            
            # extract attribute dictionarys from objects         
            paraNew_dict = copy.deepcopy(paraNew.__dict__)
            paraOld_dict = copy.deepcopy(paraOld.__dict__)
            
            # loop over all attributes to be compared
            for key in attrToBeCompared_list:
                
                # one or more values are NOT equal
                if paraNew_dict[key] != paraOld_dict[key]:
                    break
            
            # else = no break-statement executed
            else: 
                # all values of current old set is equal                
                if   whatToGet in ["AMexist", "SMexist", "MPexist"]:
                    return True
                
                # load result file data
                paraOld, _, AMresOld, MPresOld, _ = self.load_resultFile(setIdxOld)
                
                if   whatToGet == "set":
                    return [paraOld, setIdxOld, AMresOld, MPresOld, OCV_fract_listOld]
                elif whatToGet == "OCV_fract_list":
                    return OCV_fract_listOld
                elif whatToGet == "AMres":
                    return AMresOld
                elif whatToGet == "MPres":
                    return MPresOld
        
        # none of the old sets is equal to the new set
        return False
        
        
 
#==============================================================================

