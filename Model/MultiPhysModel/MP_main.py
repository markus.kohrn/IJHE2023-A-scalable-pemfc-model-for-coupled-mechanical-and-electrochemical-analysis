# -*- coding: utf-8 -*-
"""
Created on Fri Nov  4 17:57:57 2022

@author: kohrn
"""

from __future__ import division, absolute_import, \
                       print_function#, unicode_literals

'''=== general imports ==='''
import os, sys, time
import math
import numpy as np
import copy
import matplotlib.pyplot   as     plt


'''=== user defined imports ==='''

# swith cwd to parent directory, if not yet done
if os.path.split(os.getcwd())[1] == "MultiPhysModel":
    os.chdir("..")

import MultiPhysModel.plot as plot
import MultiPhysModel.GxGy as GxGy
import MultiPhysModel.elch as elch

from Parameter.class_para_base import Parameters as c_para

'''=== pre-settings ==='''

npAND = np.logical_and

np.seterr(all='raise')

# %matplotlib qt

'''
==============================================================================
        Set global calculation modifiers
==============================================================================
'''

#SET_MODE = "UIchar"       # select for creating U-I-characteristic curve
SET_MODE = "singleSim"    # select for single simulation 

# List of OCV fractions for UIchar
OCV_FRACT_LIST_default = [1.0, 0.95, 0.9, 0.85, 0.8, 0.75, 0.7]#, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 0.05]
#OCV_fract_LIST_default = [1.0, 0.9,   0.75,   0.6,   0.45,   0.3,         ]

'''
==============================================================================
        Main
==============================================================================
'''

def main(mode="singleSim", para=None, OCV_fract_list=OCV_FRACT_LIST_default, 
         doSuppressPrint=False, doSuppressPlots=False):
    '''main method, running the calculation loop for the selected mode'''
    
    print("\n=================================================")                  
    print(">>>>>>>>>>>> Multi-Physics Simulation <<<<<<<<<<<")
    print("=================================================\n") 

#====================
# mode selection
    
    if mode == "singleSim":
        myPrint("==\n Selected Mode: Single Simulation!\n===", doSuppressPrint)
        OCV_fract                = para.OCV_fract
        loops                    = para.mp_loops_max
        loops_check              = para.mp_loops_check
        threshold_curBalRel_Mean = para.mp_threshold_curBalRel_Mean
        threshold_curBalRel_Max  = para.mp_threshold_curBalRel_Max
        threshold_relDel_Max     = para.mp_threshold_relDel_Max
        
    elif mode == "UIchar":
        myPrint("==\n Selected Mode: UI-Characteristic!\n===", doSuppressPrint)
        OCV_fract                = OCV_fract_list[0]
        loops                    = para.mp_loops_max
        loops_check              = para.mp_loops_check
        threshold_curBalRel_Mean = para.mp_threshold_curBalRel_Mean
        threshold_curBalRel_Max  = para.mp_threshold_curBalRel_Max
        threshold_relDel_Max     = para.mp_threshold_relDel_Max
    else:
        myPrint("ERROR: unknown simulation mode: %s in MP_main.py\n" % mode, doSuppressPrint)
        return False       
    
    myPrint( frame("OCV fraction = %.2f %%" %(OCV_fract*100) ), doSuppressPrint) 

#====================
# extracting comen parameters from parameter-set 

    fNum_pot,  fNum_actPot = para.mp_fNum_pot,  para.mp_fNum_actPot
    fNum_glEq, fNum_gmEq   = para.mp_fNum_glEq, para.mp_fNum_gmEq
    fNum_EOD               = para.mp_fNum_EOD
    EPS                    = para.mp_EPS
    iE,  iI,               = para.iE,  para.iI
    iWg, iWl, iWm, iW      = para.iWg, para.iWl, para.iWm, para.iW
    iH2, iO2, iN2, iG, iC  = para.iH2, para.iO2, para.iN2, para.iG, para.iC
    F, R, T                = para.F, para.R, para.T 
    geoDict                = para.geoDict
    nSpecs                 = para.nSpecs  
    yV_GDLa, yV_CLc        = para.yV_GDLa, para.yV_CLc 
    yV_GDLc, yV_CLa        = para.yV_GDLc, para.yV_CLa

#====================
# create loop arrays    

    # initial concentrations
    pot = init_pot(para, OCV_fract)

    # Resistance arrays / EOD coefficient
    Gx, Gy = GxGy.calc_GxGy(pot, para, plotConductances=True)
    t_EOD  = GxGy.calc_t_EOD(pot, para)
    
    # plot
    if not doSuppressPlots:
        plot.concentrations(pot, para, winTitle="concentrations initial")
        plot_elPot(pot, Gy, para, "el.pot.dist. init.")
    
    # currents
    (curReact, curEOD, curPC_gl, curPC_gm, 
     cur_bal, curX, curY                   ) = init_cur(para)
    
    # activation overpotentials
    U_act_CLa, U_act_CLc = init_Uact(para)

#====================
# loop - preparation
    
    time_start = time.time()
    
    curReact_list = list()
    curPC_gm_list = list()
    curPC_gl_list = list()
    curEOD_list   = list()
    curBalRel_MeanMax_list = list()
    #curBalRel_MeanMax_list2= list()
    
    V_tot_frame = addFrame(para.V_tot) 
    V_por_frame = addFrame(para.V_por)
    
    use_PCgl        = True#False#
    use_PCgm        = True#False#
    use_EOD         = True#False#
    
    currentCoupling = False#True
    
    safed = dict()
    
    afterSoftCB   = False
    doHardCutback = False
    
    fNum_cutback_init = para.mp_fNum_cutback_init
    
    returnDict_list = list()  
    
    curReact_list_a = list()
    curReact_list_c = list()

    
#==============================================================================
# loop
    
    fNum_cutback = fNum_cutback_init
    time_start   = time.time()
    isConverged  = False  
    loop         = 0     
      
    np.seterr(under='ignore')

    
    while True:
        
        try:
        #if True:
        
            loop       += 1
            error       = False
            isConverged = False
            
            # copy -> old for progression calculation
            sum_curReact_old = np.sum(np.abs(curReact))          
            sum_curPC_gm_old = np.sum(np.abs(curPC_gm))
            sum_curPC_gl_old = np.sum(np.abs(curPC_gl))
            sum_curEOD_old   = np.sum(np.abs(curEOD  ))

        #=================================  
        # electrochemical reaction and potential
             
            E_eq_CLa = elch.E_equilibrium(pot[1:-1,yV_CLa,:], "HOR", para)
            E_eq_CLc = elch.E_equilibrium(pot[1:-1,yV_CLc,:], "ORR", para)
            
            U_CLa =  E_eq_CLa + U_act_CLa
            U_CLc =  E_eq_CLc + U_act_CLc            
            
            # Current from Potential
            cur_CLa = elch.i3_ButlerVolmer(pot[1:-1,yV_CLa,:], U_act_CLa, "HOR", para) *V_tot_frame[1:-1,yV_CLa]
            cur_CLc = elch.i3_ButlerVolmer(pot[1:-1,yV_CLc,:], U_act_CLc, "ORR", para) *V_tot_frame[1:-1,yV_CLc]
            
            # Reaction currents
            curReact[1:-1,yV_CLa,:] = np.repeat(cur_CLa[:,:,np.newaxis], nSpecs, axis=2) * para.ReactVec_a
            curReact[1:-1,yV_CLc,:] = np.repeat(cur_CLc[:,:,np.newaxis], nSpecs, axis=2) * para.ReactVec_c
        
        #=================================  
        # water equilibrium: gas/membrane
                
            if use_PCgm:
                
                DEL_c_PC_gm_a = (GxGy.cWg_from_cWm(pot[1:-1,yV_CLa,:], para) - pot[1:-1,yV_CLa,iWg])
                DEL_c_PC_gm_c = (GxGy.cWg_from_cWm(pot[1:-1,yV_CLc,:], para) - pot[1:-1,yV_CLc,iWg])        
                
                n_PC_gm_a = DEL_c_PC_gm_a *V_por_frame[1:-1,yV_CLa] *fNum_gmEq 
                n_PC_gm_c = DEL_c_PC_gm_c *V_por_frame[1:-1,yV_CLc] *fNum_gmEq
                  
                curPC_gm[1:-1,yV_CLa,iWg] -= n_PC_gm_a
                curPC_gm[1:-1,yV_CLa,iWm] += n_PC_gm_a
                curPC_gm[1:-1,yV_CLc,iWg] -= n_PC_gm_c
                curPC_gm[1:-1,yV_CLc,iWm] += n_PC_gm_c

                
        #===============================
        # water equilibrium: gas/liquid
        
            if use_PCgl:
                
                DEL_c_sat   = GxGy.p_sat(para)/(R*T) - pot[:,:,iWg]
                # DEL_c_sat < 0: too much vapor -> condensation
                # DEL_c_sat > 0: undersaturated -> evaporation
         
                DEL_c_PC_gl = np.minimum(DEL_c_sat, pot[:,:,iWl])
                 
                n_PC_gl_a = DEL_c_PC_gl[1:-1,yV_GDLa] *V_por_frame[1:-1,yV_GDLa] *fNum_glEq
                n_PC_gl_c = DEL_c_PC_gl[1:-1,yV_GDLc] *V_por_frame[1:-1,yV_GDLc] *fNum_glEq
     
                curPC_gl[1:-1,yV_GDLa,iWg] -= n_PC_gl_a
                curPC_gl[1:-1,yV_GDLa,iWl] += n_PC_gl_a
                curPC_gl[1:-1,yV_GDLc,iWg] -= n_PC_gl_c
                curPC_gl[1:-1,yV_GDLc,iWl] += n_PC_gl_c
                
        #===============================
        # electroosmotic drag
        
            if use_EOD:
                
                n_EOD_set = - (+ curX[ :-1,1:-1,iI] - curX[1:  ,1:-1,iI]
                               + curY[1:-1, :-1,iI] - curY[1:-1,1:  ,iI] ) *t_EOD /F
                
                curEOD[1:-1,1:-1,iWm] += ( n_EOD_set - curEOD[1:-1,1:-1,iWm] ) *fNum_EOD
        
        #===============================                
        # ionic potential at electrodes  
    
            
            pot[1:-1,yV_CLa,iI] = pot[1:-1,yV_CLa,iE] - U_CLa
            pot[1:-1,yV_CLc,iI] = pot[1:-1,yV_CLc,iE] - U_CLc   
    
 
        #==============================
        # potential iteration

            for iterLoop in range(10):
                
                pot[1:-1,1:-1,:] += ((  pot[1:-1,0:-2,:]*Gy[1:-1,0:-1,:] + pot[1:-1,2:,:]*Gy[1:-1,1:,:]  # y-diffusion
                                      + pot[0:-2,1:-1,:]*Gx[0:-1,1:-1,:] + pot[2:,1:-1,:]*Gx[1:,1:-1,:]  # x-diffusion
                                     
                                      + curReact[1:-1,1:-1,:]                          # reaction 
                                      + curPC_gm[1:-1,1:-1,:]                          # phase change gas->mem
                                      + curPC_gl[1:-1,1:-1,:]                          # phase change gas->liquid
                                      + curEOD[  1:-1,1:-1,:]                          # electro-osmotic drag
                                      
                                      ) / (
                                                         Gy[1:-1,0:-1,:] +                Gy[1:-1,1:,:] 
                                      +                  Gx[0:-1,1:-1,:] +                Gx[1:,1:-1,:] + EPS                         
                                      )
                                    
                                    - pot[1:-1,1:-1,:] ) *fNum_pot

                if iterLoop >= 3 and np.min(pot[1:-1,1:-1,iO2]) >= 0:
                    break
    
    
        #===========================
        # Current balance
    
            curY = (pot[ :,1:,:] - pot[:  ,:-1,:]) * (Gy[:,:,:])
            curX = (pot[1:, :,:] - pot[:-1,:  ,:]) * (Gx[:,:,:])
     
    
            cur_bal[ :  ,1:-1,: ]  =(curY[:  , :-1,: ] - curY[:,1:   ,: ] - curReact[:,1:-1,:] 
                                                                          - curEOD[  :,1:-1,:] 
                                                                          - curPC_gl[:,1:-1,:] 
                                                                          - curPC_gm[:,1:-1,:] )
            cur_bal[1:-1,1:-1,: ] += curX[:-1,1:-1,: ] - curX[1:,1:-1,: ] 
            

        #==========================
        # Manipulate activation overvoltage
            
            U_act_CLa += np.clip(-cur_bal[1:-1,yV_CLa,iI], -0.05,0.05 )  *fNum_actPot      /para.i3_ex0_HOR  *fNum_cutback
            U_act_CLc += np.clip(-cur_bal[1:-1,yV_CLc,iI], -0.01,0.01 )  *fNum_actPot *100 /para.i3_ex0_ORR  *fNum_cutback 
            
            U_act_CLa = np.clip( U_act_CLa,  0, 2 )
            U_act_CLc = np.clip( U_act_CLc, -2, 0 )
            
            
            
            # current coupling
            if currentCoupling:
                try: 
                    f_a = (1 - np.sum(cur_CLc) / np.sum(cur_CLa) ) /2
                    f_c = (1 - np.sum(cur_CLa) / np.sum(cur_CLc) ) /2
                except: 
                    f_a = 1
                    f_c = 1
            
                U_act_CLa = np.maximum( 0, U_act_CLa ) * (0.999 + 0.001*f_a)
                U_act_CLc = np.minimum( 0, U_act_CLc ) * (0.999 + 0.001*f_c)
    
    
    #==============================================================================
    # Update Conductance arrays
    
            # update conductance arrays
            Gx, Gy = GxGy.calc_GxGy( pot, para)
            t_EOD  = GxGy.calc_t_EOD(pot, para)


#==============================================================================
# Catch Error
            
        except Exception as exception:
            myPrint("Lp:%7d /%3d%% /%4ds | => ERROR: %s ===" 
                    %(loop, loop/loops*100, time.time()-time_start, str(exception) ), 
                    doSuppressPrint)
            error = True

#==============================================================================
# progress calculation

        if error or afterSoftCB or (loop in [1,loops]) or loop%loops_check == 0:

            #plot_elPot(pot, Gy, para, "loop = %d" %(loop))

            afterSoftCB = False

            # store progress data
            storeSimProgress(curReact, curPC_gl, curPC_gm, curEOD,
                             curReact_list, curPC_gm_list, curPC_gl_list, curEOD_list)
            
            # store reaction current data
            curReact_list_a.append( [np.min(cur_CLa), np.mean(cur_CLa), np.max(cur_CLa)] )
            curReact_list_c.append( [np.min(cur_CLc), np.mean(cur_CLc), np.max(cur_CLc)] )

            # calculate relative error
            curBalRel_Mean, curBalRel_Max = calc_curBalRel_MeanMax(curX, curY, cur_bal, para)
            curBalRel_MeanMax_list.append([curBalRel_Mean, curBalRel_Max])

            cur2_mean_CLa_cm2 = np.sum(cur_CLa) /(geoDict['xTot'] * para.zElemLen) *10**(-4)
            cur2_mean_CLc_cm2 = np.sum(cur_CLc) /(geoDict['xTot'] * para.zElemLen) *10**(-4)
            
            # calculate relative delta of source/sink terms
            def calc_relDel(cur, sum_cur_old):
                try:    return np.sum(np.abs(cur)) / sum_cur_old - 1
                except: return 0
            
            relDel_React = calc_relDel(curReact, sum_curReact_old)
            relDel_PC_gl = calc_relDel(curPC_gl, sum_curPC_gl_old)
            relDel_PC_gm = calc_relDel(curPC_gm, sum_curPC_gm_old)
            relDel_EOD   = calc_relDel(curEOD  , sum_curEOD_old  )

            # print simulation progress
            myPrint("Lp:%7d /%3d%% /%4ds |I-bal[%%Imean] mn=%6.3f, mx=%8.3f |i[A/cm2] A=%.4f, C=%+.4f |rel.DEL(R/gl/gm/EOD)=%+.1E/%+.1E/%+.1E/%+.1E"      #U[V] A=%.7f, C=%.7f"
                  %(loop, loop/loops*100, time.time()-time_start,
                  np.mean(curBalRel_Mean)*100, np.max(curBalRel_Max)*100,
                  cur2_mean_CLa_cm2, cur2_mean_CLc_cm2 ,
                  relDel_React, relDel_PC_gl, relDel_PC_gm, relDel_EOD
                  ), doSuppressPrint)


            #=============
            # check for surpasing convergence threshold
            if (np.mean(curBalRel_Mean)                    < threshold_curBalRel_Mean and 
                np.max( curBalRel_Max )                    < threshold_curBalRel_Max  and
                max( abs(relDel_React), abs(relDel_PC_gl),
                     abs(relDel_PC_gm), abs(relDel_EOD  )) < threshold_relDel_Max    ):
                
                isConverged = True            
            
            
            #=============
            # cause for report is ERROR -> hard-cutback
            elif error:
                
                # restore pre-error states                
                (pot, Gx, Gy, t_EOD, curReact, 
                  curEOD, curPC_gl, curPC_gm, cur_bal, 
                  curX, curY, U_act_CLa, U_act_CLc) = restore_prevRes(safed)                
                
                if fNum_cutback > para.mp_fNum_cutback_min:                   
                    doHardCutback = True
                else:
                    # exit loop
                    pass

      
            #=============
            # maximum number of loops reached
            elif loop >= loops:
                myPrint("=> Max. number of loops reached without surpassing threshold!\n",
                        doSuppressPrint)
                
                if fNum_cutback_init > para.mp_fNum_cutback_min:
                    doHardCutback = True
                    pass
            
            
            #=============
            # just progress interval -> continue
            else:
                # safe values for potential soft-cutback
                safed = safe_state(pot, Gx, Gy, t_EOD, curReact, curEOD, curPC_gl, 
                                    curPC_gm, cur_bal, curX, curY, U_act_CLa, U_act_CLc)                
                continue
            
            
            # Hard Cutback was triggerde (error or out-of-soft cutback)
            # if doHardCutback:

            #     # reinitialize
            #     pot                            = init_pot(para, OCV_fract)
            #     Gx, Gy                         = GxGy.calc_GxGy(pot, para)                 
            #     t_EOD                          = GxGy.calc_t_EOD(pot, para)
            #     (curReact, curEOD, curPC_gl, 
            #     curPC_gm, cur_bal, curX, curY) = init_cur(para)
            #     U_act_CLa, U_act_CLc           = init_Uact(para)
                
            #     # cutback & reset loop/error
            #     myPrint("=============================================", doSuppressPrint)
            #     fNum_cutback_init      = doCutback(fNum_cutback_init, doSuppressPrint, "HARD")
            #     fNum_cutback           = fNum_cutback_init
            #     curBalRel_MeanMax_list = list()
            #     loop                   = 0
            #     doHardCutback          = False
                
            #     continue

#==============================================================================
# create return dictionary            
            
            if isConverged:
                
                # store simulation data in return dictionary
                returnDict = {"pot"         : pot,
                              
                              "OCV_fract"   : OCV_fract,
                              
                              "E_eq_CLa"    : E_eq_CLa,         "E_eq_CLc"     : E_eq_CLc,  
                              "U_CLa"       : U_CLa,            "U_CLc"        : U_CLc,
                              "cur2_CLa_cm2": cur2_mean_CLa_cm2,"cur2_CLc_cm2" : cur2_mean_CLc_cm2,
                              
                              "Gx"          : Gx,               "Gy"           : Gy,           
                              "curX"        : curX,             "curY"         : curY, 
                            
                              "curReact"    : curReact,         "curReact_list": curReact_list, 
                              "curEOD"      : curEOD,           "curEOD_list"  : curEOD_list,
                              "curPC_gl"    : curPC_gl,         "curPC_gl_list": curPC_gl_list,  
                              "curPC_gm"    : curPC_gm,         "curPC_gm_list": curPC_gm_list,
                              
                              "cur_bal"     : cur_bal,     
                              "curBalRel"   : calc_curBalRel(curX, curY, cur_bal),
                              "curBalRel_MeanMax_list" : curBalRel_MeanMax_list
                              }
                
                # prevents override (relevant if next UI-char point is subsequently calculated)
                returnDict = copy.deepcopy(returnDict)
                
            else:
                returnDict = False
                
            # append return dict to list
            returnDict_list.append( [OCV_fract, returnDict] )
 
#==============================================================================
# end loop or run next R_load
               
            # sim mode: single sim           
            if mode == "singleSim":
                myPrint("=> MP simulation converged! -> early exit\n",
                        doSuppressPrint)
                break
        
            # sim mode: UI char
            elif mode == "UIchar":
                myPrint("\n=> UIchar point solved ===> OCV_fract=%f %%, i2=%f A/cm2, U=%f V" 
                      %(OCV_fract, (cur2_mean_CLa_cm2-cur2_mean_CLc_cm2)/2, pot[1,-1,iE]),
                      doSuppressPrint)              
                try:
                    OCV_fract_old = OCV_fract
                    OCV_fract     = OCV_fract_list[OCV_fract_list.index(OCV_fract)+1]
                    myPrint( frame("Next OCV_fract = %f %% " %OCV_fract), doSuppressPrint)
                except:
                    myPrint("=> List of OCV_fract completed!", doSuppressPrint)
                    break
                
                # reduce cell voltage to next point in UI-char list
                U_cell_new      = pot[1,-1,iE] * OCV_fract/OCV_fract_old
                pot[1:-1,-1,iE] = np.where(para.geoDict['xElemsIsRIBc'], U_cell_new, 0) # Rib c

                # reset cutback parameters
                fNum_cutback_init      = para.mp_fNum_cutback_init
                fNum_cutback           = fNum_cutback_init
                
                # reset calauclation counter
                curBalRel_MeanMax_list = list()
                isConverged            = False
                loop                   = 0

                continue



            
#==============================================================================
# end of loop -> plot

    myPrint("= Tot. sim time = %.2f" %(time.time()-time_start), doSuppressPrint)
        
    curBalRel = calc_curBalRel(curX, curY, cur_bal)
    
    if not doSuppressPlots:
        plot_elPot(pot, Gy, para, "el.pot.dist final")
        
        plot.errorProgress(curReact_list, curPC_gm_list, curPC_gl_list, curEOD_list, 
                        curBalRel_MeanMax_list, para, curReact_list_a, curReact_list_c,
                        winTitle="Error Progression")
        plot.currentBalance(cur_bal, curBalRel, para)
        plot.currents(curX, curY, curReact, curBalRel, para,
                        winTitle="currents final")
        plot.concentrations(pot, para, 
                        winTitle="concentrations final")
        plot.conductances(Gx, Gy, geoDict, para, 
                        winTitle="conductances final")
        plot.linePlot(pot, curX, curY, curReact, curBalRel,
                        E_eq_CLa, E_eq_CLc, U_CLa, U_CLc, 
                        geoDict, para, 
                        winTitle="line plots final")
        plot.water(curX, curY, curReact, curPC_gl, curPC_gm, para, 
                        winTitle="Water currents")
        plot.water2(pot, para, 
                        winTitle="water 2")        
        
#==============================================================================
# return statement

    return returnDict_list


'''
==============================================================================
        small helper functions
==============================================================================
'''

def addFrame(arr):
    xEl, yEl  = arr.shape
    arr_frame = np.zeros((xEl+2, yEl+2))
    arr_frame[1:-1,1:-1] = arr
    return arr_frame

#==============================================

def delFrame(arr_frame):
    arr = arr_frame[1:-1,1:-1]
    return arr

#==============================================

def doCutback(fNum_cutback, doSuppressPrint, hardSoft):
    fNum_cutback /= 2
    myPrint("----> %s-Cutback to fNum_cutback = %f\n" %(hardSoft, fNum_cutback), doSuppressPrint)
    return fNum_cutback
    
'''
==============================================================================
        safe/restore intermediate results
==============================================================================
'''

def safe_state(pot, Gx, Gy, t_EOD, curReact, curEOD, curPC_gl, 
               curPC_gm, cur_bal, curX, curY, U_act_CLa, U_act_CLc):
                
    safed = {"pot"       : copy.deepcopy( pot      ),
             "Gx"        : copy.deepcopy( Gx       ),           
             "Gy"        : copy.deepcopy( Gy       ),
             "t_EOD"     : copy.deepcopy( t_EOD    ),
             "curReact"  : copy.deepcopy( curReact ),     
             "curEOD"    : copy.deepcopy( curEOD   ),
             "curPC_gl"  : copy.deepcopy( curPC_gl ),     
             "curPC_gm"  : copy.deepcopy( curPC_gm ),
             "cur_bal"   : copy.deepcopy( cur_bal  ),
             "curX"      : copy.deepcopy( curX     ),         
             "curY"      : copy.deepcopy( curY     ),
             "U_act_CLa" : copy.deepcopy( U_act_CLa),    
             "U_act_CLc" : copy.deepcopy( U_act_CLc),  
            }
    
    #myPrint("   -> safe previouse state")
    
    return safed

#==============================================

def restore_prevRes(safed):

    # return to previouse
    pot       = safed["pot"]
    Gx        = safed["Gx"] 
    Gy        = safed["Gy"]         
    t_EOD     = safed["t_EOD"]
    curReact  = safed["curReact"]
    curEOD    = safed["curEOD"]
    curPC_gl  = safed["curPC_gl"]
    curPC_gm  = safed["curPC_gm"]
    cur_bal   = safed["cur_bal"]
    curX      = safed["curX"]
    curY      = safed["curY"]
    U_act_CLa = safed["U_act_CLa"]
    U_act_CLc = safed["U_act_CLc"]
    
    myPrint("   -> restore previouse state")
    
    return (pot, Gx, Gy, t_EOD, curReact, curEOD, curPC_gl, 
            curPC_gm, cur_bal, curX, curY, U_act_CLa, U_act_CLc)


'''
==============================================================================
        calculate simulation progression
==============================================================================
'''

def update_UIchar_dict(UIchar_dict, R_load, cur2_mean_CLa_cm2, pot, para):
    UIchar_dict["R_load"].append(R_load)
    UIchar_dict["i2_cm2"].append(cur2_mean_CLa_cm2)
    UIchar_dict["U_cell"].append(pot[1,-1,para.iE])
    return True

#==============================================

def storeSimProgress(curReact, curPC_gl, curPC_gm, curEOD,
                     curReact_list, curPC_gm_list, curPC_gl_list, curEOD_list):
    '''storing the characteristig iteration values in given lists '''
            
    calc_minMeanMax = lambda cur: [np.min(cur), np.mean(cur), np.max(cur)]
    
    curReact_list.append(calc_minMeanMax(curReact))
    curPC_gm_list.append(calc_minMeanMax(curPC_gm))
    curPC_gl_list.append(calc_minMeanMax(curPC_gl))
    curEOD_list.append(  calc_minMeanMax(curEOD  ))
    
    return True
 
#==============================================

def calc_curBalRel_MeanMax(curX, curY, cur_bal, para):
    '''calculating the mean/max relative current balances for each species '''
    
    curBalRel = calc_curBalRel(curX, curY, cur_bal)
    
    error_relMean_vec= list()
    error_relMax_vec = list()
    
    for iX in range(para.nSpecs):
        error_relMean_vec.append(np.mean(curBalRel[:,:,iX]))
        error_relMax_vec.append( np.max( curBalRel[:,:,iX]))
    
    return error_relMean_vec, error_relMax_vec
    

#==============================================

def calc_curBalRel(curX, curY, cur_bal):
    '''calculating the relative current balances for each species'''
    
    cur_bal_rel      = np.zeros(cur_bal.shape)
    
    for iSpec in range(cur_bal.shape[2]):
        meanValue_iSpec        = ( np.mean(np.abs(curY[:,:,iSpec])) + np.mean(np.abs(curX[:,:,iSpec])) ) /2
        if abs(meanValue_iSpec) < 10**-10:
            cur_bal_rel[:,:,iSpec] = 0 
        else:
            cur_bal_rel[:,:,iSpec] = np.abs(cur_bal[:,:,iSpec]) / meanValue_iSpec
    
    return cur_bal_rel

'''
===============================================================================
        Set initial potentials / and iteration quantities
===============================================================================
'''

def init_pot(p, OCV_fract):
    
    # extract from parameters
    xElems,yElems,nSpecs = p.xElems, p.yElems, p.nSpecs
    
    # crate empty potential array
    pot = np.zeros((xElems,yElems,nSpecs))
    
    # fill potential array
    pot = init_pot_chem(p, pot, OCV_fract)
    pot = init_pot_elec(p, pot, OCV_fract)
    
    # return statement
    return pot


#==============================================================================


def init_pot_chem(p, pot, OCV_fract):
    '''creating the initial potential field (concentrations)'''
    
    # extract from parameteres
    R, T                         = p.R, p.T
    xElems,yElems,nSpecs         = p.xElems, p.yElems, p.nSpecs
    iH2, iO2, iN2, iWg, iWl, iWm = p.iH2, p.iO2, p.iN2, p.iWg, p.iWl, p.iWm 
    gD                           = p.geoDict
    
    # Initial concentrations
    p_WgSat = GxGy.p_sat(p)
    
    c_Wg_init_a =                       p.RH_ano * p_WgSat   /R/T  # 5 a
    c_Wg_init_c =                       p.RH_cat * p_WgSat   /R/T  # 5 c
    c_H2_init   = p.x_H2 * (p.p_tot_a - p.RH_ano * p_WgSat)  /R/T  # 2
    c_O2_init   = p.x_O2 * (p.p_tot_c - p.RH_cat * p_WgSat)  /R/T  # 3
    c_N2_init   = p.x_N2 * (p.p_tot_c - p.RH_cat * p_WgSat)  /R/T  # 4

    c_Wl_init_a = 0                                          # 6 a
    c_Wl_init_c = 0                                          # 6 c
    
    # anode (GDL+CL) 
    pot[1:-1,    0   ,iH2] = c_H2_init
    pot[1:-1,    0   ,iWg] = c_Wg_init_a
    pot[1:-1,    0   ,iWl] = c_Wl_init_a    
    pot[1:-1,p.yV_ELa,iH2] = c_H2_init
    pot[1:-1,p.yV_ELa,iWg] = c_Wg_init_a
    pot[1:-1,p.yV_ELa,iWl] = c_Wl_init_a
    
    # cathode (GDL+CL)
    pot[1:-1,p.yV_ELc,iO2] = c_O2_init
    pot[1:-1,p.yV_ELc,iN2] = c_N2_init
    pot[1:-1,p.yV_ELc,iWg] = c_Wg_init_c
    pot[1:-1,p.yV_ELc,iWl] = c_Wl_init_c
    pot[1:-1,    -1  ,iO2] = c_O2_init
    pot[1:-1,    -1  ,iN2] = c_N2_init
    pot[1:-1,    -1  ,iWg] = c_Wg_init_c
    pot[1:-1,    -1  ,iWl] = c_Wl_init_c
        
    # Ionomer (PEM+CL)
    pot_yV_PEM_moc = np.zeros(pot[1:-1,p.yV_PEM,:].shape)
    pot_yV_PEM_moc[:,:,iWg] = (c_Wg_init_a + c_Wg_init_c)/2
    
    pot[1:-1,p.yV_CLa,iWm] = GxGy.cWm_from_cWg(pot[1:-1,p.yV_CLa,:], p)
    pot[1:-1,p.yV_PEM,iWm] = GxGy.cWm_from_cWg(pot_yV_PEM_moc,       p)
    pot[1:-1,p.yV_CLc,iWm] = GxGy.cWm_from_cWg(pot[1:-1,p.yV_CLc,:], p)
      
    # BPP Rib
    pot[1:-1, 0,:] = np.where(np.repeat(gD['xElemsIsRIBa'][:,np.newaxis],nSpecs,axis=1), 0, pot[1:-1, 0,:]) # Rib a
    pot[1:-1,-1,:] = np.where(np.repeat(gD['xElemsIsRIBc'][:,np.newaxis],nSpecs,axis=1), 0, pot[1:-1,-1,:]) # Rib c
    
    # return statement
    return pot

#==============================================================================
    # init Electric

def init_pot_elec(p, pot, OCV_fract):
    '''creating the initial potential field (electric/ionic)'''
    
    # extract from parameters
    gD             = p.geoDict
    xElems, yElems = p.xElems, p.yElems
    iE, iI         = p.iE, p.iI

    # calculate resistance arrays
    Gx, Gy = GxGy.calc_GxGy(pot, p, plotConductances=False)
    
    Gy_el_x0 = Gy[1,:,iE]
    Gy_io_x0 = Gy[1,:,iI]
    
    # calc OCV electrode potential
    DEL_E_a =  elch.E_equilibrium(pot[1:-1,p.yV_CLa,:], "HOR", p)[0,0]
    DEL_E_c =  elch.E_equilibrium(pot[1:-1,p.yV_CLc,:], "ORR", p)[0,0]      
    
    # calc OCV and total Voltage loss
    U_OCV  = - DEL_E_a + DEL_E_c
    U_loss = U_OCV * (1 - OCV_fract)

    # create resistance vector at x=0, y=1
    Ry_x0 = np.zeros(yElems-1)    
    
    Ry_x0[p.yBPPa    : p.yCLa_0   ] = 1 / Gy_el_x0[p.yBPPa    : p.yCLa_0   ]                                        # 19
    Ry_x0[p.yCLa_0   : p.yPEM_0-1 ] = 2 /(Gy_el_x0[p.yCLa_0   : p.yPEM_0-1 ] + Gy_io_x0[p.yCLa_0  : p.yPEM_0-1 ] )  #  2
    Ry_x0[p.yPEM_0-1 : p.yCLc_0   ] = 1 /                                      Gy_io_x0[p.yPEM_0-1: p.yCLc_0   ]    #  7
    Ry_x0[p.yCLc_0   : p.yGDLc_0-1] = 2 /(Gy_el_x0[p.yCLc_0   : p.yGDLc_0-1] + Gy_io_x0[p.yCLc_0  : p.yGDLc_0-1] )  #  5
    Ry_x0[p.yGDLc_0-1: p.yBPPc    ] = 1 / Gy_el_x0[p.yGDLc_0-1: p.yBPPc    ]                                        # 19

    # calc electric potential (el. and ion.)
    E_x0  = np.zeros(yElems)
    
    for iy in range(1,len(E_x0)):
        E_x0[iy] = E_x0[iy-1] - U_loss * Ry_x0[iy-1] / np.sum(Ry_x0)
    
    E_el_x0 = copy.deepcopy(E_x0)
    E_io_x0 = copy.deepcopy(E_x0)
    
    E_el_x0[p.yPEM_0 : p.yCLc_0]  = 0
    E_el_x0[p.yCLc_0 :         ] -= (DEL_E_a - DEL_E_c)
    
    E_io_x0[         :p.yCLa_0 ]  = 0
    E_io_x0[p.yCLa_0 :p.yGDLc_0] -= DEL_E_a  
    E_io_x0[p.yGDLc_0:         ]  = 0
    
    pot[1:-1, :,iE] = np.repeat( E_el_x0[np.newaxis,:], xElems-2, axis=0)
    pot[1:-1, :,iI] = np.repeat( E_io_x0[np.newaxis,:], xElems-2, axis=0)
    
    # set el.pot. zero in channel
    pot[1:-1, 0,iE] = np.where(gD['xElemsIsRIBa'], pot[1:-1, 0,iE], 0) # Rib a
    pot[1:-1,-1,iE] = np.where(gD['xElemsIsRIBc'], pot[1:-1,-1,iE], 0) # Rib c
    
    # return statement
    return pot


#==============================================================================

def plot_elPot(pot, Gy, p, winTitle=""):
    
    fig, ax = plt.subplots(2,2)
    fig.canvas.manager.set_window_title(winTitle)
    ax = [ax[0,0], ax[0,1], ax[1,0], ax[1,1]]
        
    ax[0].plot(range(p.yBPPa,  p.yPEM_0 ), pot[1, p.yBPPa :p.yPEM_0 , p.iE], "x--")
    ax[0].plot(range(p.yCLc_0, p.yBPPc+1), pot[1, p.yCLc_0:p.yBPPc+1, p.iE], "x--")
    ax[0].plot(range(p.yCLa_0, p.yGDLc_0), pot[1, p.yCLa_0:p.yGDLc_0, p.iI], "x:" )
    
    ax[0].vlines([p.yGDLa_0 - 0.5 ,
                  p.yCLa_0  - 0.5 ,
                  p.yPEM_0  - 0.5 ,
                  p.yCLc_0  - 0.5 ,
                  p.yGDLc_0 - 0.5 ,
                  p.yBPPc   - 0.5 , ] , -1.2, 1.2)

    titles = ["el.pot. at left boundary", "el.potential (electric)", "el.potential (ionic)", "el. resistance (ionic) Y"]
    im = [None, None, None, None]
    im[1] = ax[1].imshow(pot[:,:,p.iE].T, cmap="jet")
    im[2] = ax[2].imshow(pot[:,:,p.iI].T, cmap="jet")
    im[3] = ax[3].imshow(Gy[ :,:,p.iI].T, cmap="jet")
    
    
    ax[0].set_title(titles[0])
    
    for idx in range(1,4):
        plt.colorbar(im[idx], ax=ax[idx], orientation="horizontal")
        ax[idx].invert_yaxis()
        ax[idx].set_title(titles[idx])
        
    return True
    



#==============================================================================

def init_cur(para):
    "initialize all current arrays"
    
    xElems,yElems,nSpecs   = para.xElems, para.yElems, para.nSpecs
    
    # currents: iteration
    curReact = np.zeros((xElems,yElems,nSpecs)) 
    curEOD   = np.zeros((xElems,yElems,nSpecs))
    curPC_gl = np.zeros((xElems,yElems,nSpecs))
    curPC_gm = np.zeros((xElems,yElems,nSpecs))
    
    # currents: balance
    cur_bal = np.zeros((xElems,   yElems,   nSpecs)) 
    curX    = np.zeros((xElems-1, yElems  , nSpecs))
    curY    = np.zeros((xElems,   yElems-1, nSpecs))    

    return curReact, curEOD, curPC_gl, curPC_gm, cur_bal, curX, curY

#==============================================================================

def init_Uact(para):
    "initialize activation voltage arrays"
    
    xElems  = para.xElems
    geoDict = para.geoDict 
    
    # potentials
    U_act_CLa = np.zeros((xElems-2,geoDict["yElemsCLa"]))
    U_act_CLc = np.zeros((xElems-2,geoDict["yElemsCLc"]))
    
    return U_act_CLa, U_act_CLc


'''
===============================================================================
                    Helper functions
===============================================================================
'''

def myPrint(toPrint, suppress=False):
    if not suppress:
        print(str(toPrint))


def frame(string):
    mid_line = "# " + string + " #"
    top_line = "#" + "="*(len(string)+2) + "#"
    bot_line = top_line
    return "\n" + top_line + "\n" + mid_line + "\n" + bot_line + "\n" 

'''
===============================================================================
                    Start-up Routine
===============================================================================
'''

if __name__ == "__main__":
    
    paraSet = c_para()
    
    if SET_MODE == "singleSim":
        main(mode="singleSim", para=paraSet, R_load_list=None)
    
    elif SET_MODE == "UIchar":
        main(mode="UIchar",    para=paraSet, OCV_fract_list=OCV_FRACT_LIST_default)
        
        
        
        
        
        
        