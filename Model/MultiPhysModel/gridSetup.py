# -*- coding: utf-8 -*-
"""
Created on Fri Nov  4 17:57:57 2022

@author: kohrn
"""

from __future__ import division, absolute_import, \
                       print_function#, unicode_literals
                       
# %matplotlib qt

'====== general imports ======'

import numpy                        as np
import matplotlib.pyplot            as plt
from scipy.optimize import minimize as sp_minimize
from scipy.optimize import root     as sp_root


'''
==============================================================================
        Creating parameters for testing routine
==============================================================================
'''

class testParaClass:
    
    def __init__(self):
    
        self.yMinElemsPerLayer =   3
        self.yyMaxRatio        =   1.5
        self.xyMaxRatio        =  10
        
        self.d_GDLa            = 200 * 10**(-6) # m
        self.d_GDLc            = 200 * 10**(-6) # m
        self.d_CLa             =   4 * 10**(-6) # m
        self.d_CLc             =   8 * 10**(-6) # m
        self.d_PEM             =   9 * 10**(-6) # m
        
        self.l_RIBa            = 500 * 10**(-6) # m
        self.l_RIBc            = 300 * 10**(-6) # m
        self.l_CHAa            = 250 * 10**(-6) # m
        self.l_CHAc            = 200 * 10**(-6) # m    


'''
==============================================================================
        main function: calculate x and y lengths
==============================================================================
'''

def calc_xLen_yLen(para, mode="MPonly", allRegs=None, doPlot=False):

    # calculate y-grid
    d_yTot, y_min, yLenElems, yElems_PEM, yElems_CLa, yElems_CLc, yElems_GDLa, yElems_GDLc = calc_yLen(para)
    
    print(yLenElems)
    print(np.sum(yLenElems))
    
    # calculate x-grid     
    x_tot, xLenElems, xSEG, xSEGa, xSEGc, xElemsPerSeg_list = calc_xLen(para, y_min, mode, allRegs)
    
    # if mode=="coupledSim":
    #     print(x_tot)
    #     print(xLenElems)
    #     print(xSEG)
    #     print(xSEGa)
    #     print(xSEGc)
    #     print(xElemsPerSeg_list)
    #     exit()
    
    xElems_tot = xLenElems.size
    yElems_tot = yLenElems.size

    
# === print command ===   
        
    print("PEM : %3d x %3d = %3d" %(yElems_PEM,  xElems_tot, yElems_PEM *xElems_tot))
    print("CLa : %3d x %3d = %3d" %(yElems_CLa,  xElems_tot, yElems_CLa *xElems_tot))
    print("CLc : %3d x %3d = %3d" %(yElems_CLc,  xElems_tot, yElems_CLc *xElems_tot))
    print("GDLa: %3d x %3d = %3d" %(yElems_GDLa, xElems_tot, yElems_GDLa*xElems_tot))
    print("GDLc: %3d x %3d = %3d" %(yElems_GDLc, xElems_tot, yElems_GDLc*xElems_tot))
    print("=====================")
    print("TOT : %3d x %3d = %3d" %(yElems_tot,  xElems_tot, yElems_tot *xElems_tot))

# === plot command ===   
        
    if doPlot:
        plot_grid(x_tot, xElems_tot, xLenElems, xSEG, 
              d_yTot, yElems_tot, yLenElems,
              yElems_GDLa, yElems_GDLc, yElems_PEM, yElems_CLa, yElems_CLc,
              winTitle="Discretization Grid")
       
      
# === return dictionary ===        
    
    def xElem_isRib(xSEGx):
        xElems_isRib = np.zeros(xLenElems.shape, dtype=bool)
        isRib = True
        i_seg = 1
        for i_xElem in range(len(xElems_isRib)):
            if abs(np.sum(xLenElems[0:i_xElem]) - xSEGx[i_seg]) < abs(np.min(xLenElems)*0.1):
                isRib  = not isRib
                i_seg += 1    
            xElems_isRib[i_xElem] = isRib
        return xElems_isRib


    xElems_isRIBa = xElem_isRib(xSEGa)
    xElems_isRIBc = xElem_isRib(xSEGc)
    
    xElems_isCHAa = np.invert(xElems_isRIBa)
    xElems_isCHAc = np.invert(xElems_isRIBc)
        
    returnDict = {"xLenElems"    : xLenElems,
                  "yLenElems"    : yLenElems,
                  "xElemsMEA"    : xElems_tot,
                  "yElemsMEA"    : yElems_tot,
                  "yElemsCLa"    : yElems_CLa,
                  "yElemsCLc"    : yElems_CLc,
                  "yElemsGDLa"   : yElems_GDLa,
                  "yElemsGDLc"   : yElems_GDLc,
                  "yElemsPEM"    : yElems_PEM ,
                  "xElemsIsRIBa" : xElems_isRIBa,
                  "xElemsIsRIBc" : xElems_isRIBc,
                  "xElemsIsCHAa" : xElems_isCHAa,
                  "xElemsIsCHAc" : xElems_isCHAc,
                  "xTot"         : x_tot
                 }
    
    
    return returnDict
    
    
'''
==============================================================================
        calculate y-Grid
==============================================================================
'''

def calc_yLen(para):
    
    d_GDLa            = para.d_GDLa
    d_GDLc            = para.d_GDLc
    d_CLa             = para.d_CLa
    d_CLc             = para.d_CLc
    d_PEM             = para.d_PEM
    yMinElemsPerLayer = para.yMinElemsPerLayer
    yyMaxRatio        = para.yyMaxRatio
    
    y_min = min(d_GDLa, d_GDLc, d_CLa, d_CLc, d_PEM) / yMinElemsPerLayer
    
    yElems_PEM = int(d_PEM / y_min)
    yElems_CLa = int(d_CLa / y_min)
    yElems_CLc = int(d_CLc / y_min)
    
    yLenElems_PEM = np.ones(yElems_PEM) * d_PEM / yElems_PEM
    yLenElems_CLa = np.ones(yElems_CLa) * d_CLa / yElems_CLa
    yLenElems_CLc = np.ones(yElems_CLc) * d_CLc / yElems_CLc
    
    #======================    
    def f1_find_yElems_GDL_min(yElems_GDL, d_CL, yElems_CL, d_GDL):
        yLen_GDL_vec = d_CL/yElems_CL * np.geomspace(yyMaxRatio, yyMaxRatio**yElems_GDL, yElems_GDL)
        
        if sum(yLen_GDL_vec) < d_GDL:
            return  f1_find_yElems_GDL_min(yElems_GDL+1, d_CL, yElems_CL, d_GDL)
        else:
            return yElems_GDL
    #====================== 
    def f2_find_yyRatio_min(yyRatio, d_CL, yElems_CL, d_GDL, yElems_GDL):
        yLen_GDL_vec = d_CL/yElems_CL * np.geomspace(yyRatio, yyRatio**yElems_GDL, yElems_GDL)
        return sum(yLen_GDL_vec) - d_GDL
    #====================== 
    
    yElems_GDLa    = f1_find_yElems_GDL_min(0, d_CLa, yElems_CLa, d_GDLa)    
    yyRatio_GDLa   = sp_root(f2_find_yyRatio_min, yyMaxRatio, args=(d_CLa, yElems_CLa, d_GDLa, yElems_GDLa)).x[0]
    yLenElems_GDLa = d_CLa/yElems_CLa * np.geomspace(yyRatio_GDLa**yElems_GDLa, yyRatio_GDLa, yElems_GDLa)
    
    yElems_GDLc    = f1_find_yElems_GDL_min(0, d_CLc, yElems_CLc, d_GDLc)    
    yyRatio_GDLc   = sp_root(f2_find_yyRatio_min, yyMaxRatio, args=(d_CLc, yElems_CLc, d_GDLc, yElems_GDLc)).x[0]
    yLenElems_GDLc = d_CLc/yElems_CLc * np.geomspace(yyRatio_GDLc, yyRatio_GDLc**yElems_GDLc, yElems_GDLc)  
    
    yLenElems = np.concatenate((yLenElems_GDLa, yLenElems_CLa, yLenElems_PEM, yLenElems_CLc, yLenElems_GDLc))    
    
    d_yTot  = d_GDLa + d_GDLc + d_CLa + d_CLc + d_PEM
    
    return d_yTot, y_min, yLenElems, yElems_PEM, yElems_CLa, yElems_CLc, yElems_GDLa, yElems_GDLc
    
'''
==============================================================================
        calculate x-Grid
==============================================================================
'''

def calc_xLen(para, y_min, mode, allRegs=None):
    
    xyMaxRatio = para.xyMaxRatio
    l_RIBa     = para.l_RIBa
    l_RIBc     = para.l_RIBc
    l_CHAa     = para.l_CHAa
    l_CHAc     = para.l_CHAc
    
    x_max = xyMaxRatio * y_min
    
    if mode == "MPonly":
        x_tot              = calc_x_TOT(     l_RIBa, l_RIBc, l_CHAa, l_CHAc)    
        xSEG, xSEGa, xSEGc = calc_x_Segments(l_RIBa, l_RIBc, l_CHAa, l_CHAc, x_tot)
        
    elif mode == "coupledSim":
        x_tot                      = allRegs.xMax() /2     
        xSepBot_list, xSepTop_list = allRegs.xSep_list()
        
        xSEGa = [0] + xSepBot_list[:int(len(xSepBot_list)/2)] + [x_tot]
        xSEGc = [0] + xSepTop_list[:int(len(xSepTop_list)/2)] + [x_tot]
        xSEG  = list(set(xSEGa) | set(xSEGc))
        xSEG.sort()

        xSEGa = [ x   *10**(-6) for x in xSEGa]
        xSEGc = [ x   *10**(-6) for x in xSEGc]
        xSEG  = [ x   *10**(-6) for x in xSEG ]
        x_tot = x_tot *10**(-6)
        
    xLenElems = np.array([])
    xElemsPerSeg_list = list()
    for idx in range(len(xSEG)-1):
        xLen          = xSEG[idx+1] - xSEG[idx]
        xElems        = int(xLen/x_max) 
        xLenElems_new = np.ones(xElems) * xLen /xElems
        xLenElems     = np.concatenate((xLenElems, xLenElems_new))
        xElemsPerSeg_list.append(xElems)

    return x_tot, xLenElems, xSEG, xSEGa, xSEGc, xElemsPerSeg_list



'''
==============================================================================
        Supporting functions
==============================================================================
'''
    
def calc_x_TOT(l_RIBa, l_RIBc, l_CHAa, l_CHAc):
    
    l_TOTa = (l_RIBa + l_CHAa ) / 2
    l_TOTc = (l_RIBc + l_CHAc ) / 2
    
    while abs(l_TOTa - l_TOTc) > min(l_RIBa, l_RIBc, l_CHAa, l_CHAc)/1000: # not equal but small error nessesary due to numerical inaccuracy
        if l_TOTa < l_TOTc:
            l_TOTa +=  (l_RIBa + l_CHAa ) / 2
        else:
            l_TOTc +=  (l_RIBc + l_CHAc ) / 2
    return l_TOTa # = l_TOTc


#==============================================================================
def calc_x_Segments(l_RIBa, l_RIBc, l_CHAa, l_CHAc, x_TOT):
    
    def func(l_RIB, l_CHA, x_TOT):
        x_SEG = [0, l_RIB/2]              
        while True:
            if x_SEG[-1] + l_CHA < x_TOT:
                x_SEG.append(x_SEG[-1] + l_CHA)
            else:
                x_SEG.append(x_TOT)
                return x_SEG
            if x_SEG[-1] + l_RIB < x_TOT:
                x_SEG.append(x_SEG[-1] + l_RIB)
            else:
                x_SEG.append(x_TOT)
                return x_SEG                
                
    xSEGa = func(l_RIBa, l_CHAa, x_TOT)
    xSEGc = func(l_RIBc, l_CHAc, x_TOT)
    xSEG  = list(set(xSEGa) | set(xSEGc))
    xSEG.sort()
    
    return xSEG, xSEGa, xSEGc

'''
==============================================================================
        Plot: discretization grid
==============================================================================
'''

def plot_grid(l_xTot, xElems_tot, xLenElems, xSEG,
              d_yTot, yElems_tot, yLenElems,
              yElems_GDLa, yElems_GDLc, yElems_PEM, yElems_CLa, yElems_CLc,
              winTitle):
    
    fig, ax = plt.subplots()
    
    fig.canvas.manager.set_window_title(winTitle)
    
    ax.set_xlim((0,l_xTot))  
    ax.set_ylim((0,d_yTot))
    
    for i in range(xElems_tot):
        x_value = sum(xLenElems[0:i])
        ax.axvline(x_value)
    ax.vlines(xSEG, ymin=0, ymax=d_yTot, color="red")
        
    for i in range(yElems_tot):
        y_value = sum(yLenElems[0:i])
        ax.axhline(y_value)
    ax.axhline(sum(yLenElems[0:yElems_GDLa]),                                  color="red")
    ax.axhline(sum(yLenElems[0:yElems_GDLa+yElems_CLa]),                       color="red")
    ax.axhline(sum(yLenElems[0:yElems_GDLa+yElems_CLa+yElems_PEM]),            color="red")
    ax.axhline(sum(yLenElems[0:yElems_GDLa+yElems_CLa+yElems_PEM+yElems_CLc]), color="red")


'''
==============================================================================
        Start-up statement for testing routine
==============================================================================
'''

if __name__ == "__main__":
    
    tpC = testParaClass()
    
    calc_xLen_yLen(tpC, doPlot=True)