# -*- coding: utf-8 -*-
"""
Created on Fri Nov  4 17:57:57 2022

@author: kohrn
"""

from __future__ import division, absolute_import, \
                       print_function#, unicode_literals

'''=== general imports ==='''
import os, sys
import numpy               as     np
from scipy.interpolate     import interp1d

import matplotlib.pyplot as plt

# swith cwd to parent directory, if not yet done
if os.path.split(os.getcwd())[1] == "MultiPhysModel":
    os.chdir("..")

'''=== user defined imports ==='''
import MultiPhysModel.plot as plot

'''=== pre-settings ==='''

npAND = np.logical_and

np.seterr(all='raise')

# %matplotlib qt

import matplotlib.pyplot as plt
'''
==============================================================================
        Functions: Water in Membrane
==============================================================================
'''
def lambda_from_cWm(c_Wm, p):
    'lambda as function of water conc. in membrane'
    return c_Wm / p.dens_PEM * p.EW_PEM


#======================================================================
def cWm_from_lambda(lam, p):
    'water conc. in membrane as function of lambda'
    return lam * p.dens_PEM / p.EW_PEM

 
'''
==============================================================================
        Phase-Equilibrium: Water gas <-> membran
==============================================================================
'''

def lambda_from_activity(a):
    'membrane water loading (lambda) in steady-state with gas concentration'   
    # S.-H. Ge et al. / Journal of Power Sources 124 (2003) 1–11
    # "A mathematical model for PEMFC in different flow modes"
    # https://doi.org/10.1016/S0378-7753(03)00584-6

    func1 = lambda a:  0.043 + 17.81 *a - 39.85 *a**2  + 36 *a**3
    func2 = lambda a: 12.6   +  1.4  *a
    func3 = lambda a: 16.8
    
    return (  np.where(      a <  1,         func1(a), 0)
            + np.where(npAND(a >= 1, a < 3), func2(a), 0)
            + np.where(              a >=3,  func3(a), 0)
           )         

#======================================================================
# inverse function of lambda_from_activity
act                   = np.linspace(-1,100,10000)
lam                  = lambda_from_activity(act)
activity_from_lambda = interp1d(lam,np.maximum(0,act), bounds_error=False, fill_value=(0,  3  ) )


#======================================================================
def activity_from_cWtot(pot, p):
    'Water activity calculated from concentration of water gas'
    c_Wg = pot[:,:,p.iWg]
    c_Wl = pot[:,:,p.iWl]
    
    s = poreSat_H2Ol(c_Wl, p)
    
    c_W = c_Wg*(1-s) + c_Wl
    
    return p.R *p.T *c_W / p_sat(p)


#======================================================================
def activity_from_cWg(pot, p):
    'Water activity calculated from concentration of water gas'
    
    return p.R *p.T *pot[:,:,p.iWg] / p_sat(p)


#======================================================================
def cWg_from_cWm(pot, p):
    'steady-state concentration of H2O gas = f(c H2O membrane)'
    
    # get lambda from concentration of adsorbed water
    lam = lambda_from_cWm(pot[:,:,p.iWm], p)
    
    # get equilibium activity from lambda
    act = activity_from_lambda(lam)
    
    # get saturation water gas concentration
    cWg_sat = p_sat(p) / (p.R * p.T)
    
    # get oversaturated water gas concentration
    cWg = act * cWg_sat
    
    return cWg



#======================================================================
def cWm_from_cWg(pot, p):
    'steady-state concentration of H2O membrane = f(c H2O gas)'
    
    # calculate total water activity (gas+liquid)
    act = activity_from_cWtot(pot, p)
    
    # calculate lambda from total activity (gas+liquid)
    lam = lambda_from_activity(act)
    
    # calculate adsorbed water concentration from lambda
    cWm = cWm_from_lambda(lam, p)
    
    return cWm

'''
==============================================================================
        Phase-Equilibrium: Water gas <-> liquid
==============================================================================
'''

def p_sat(para):
    'Water saturation pressure'   
    # Buck equation
    # A Buck / Journal of Applied Meteorology 20 (1981) 1527–1532
    # "New Equations for Computing Vapor Pressure and Enhancement Factor"
    # https://doi.org/10.1175/1520-0450(1981)020<1527:NEFCVP>2.0.CO;2
     
    T_degC = para.T - 273.15
    
    return 0.61121 * 1000 * np.exp((18.678-T_degC/234.5)*(T_degC/(257.14+T_degC)))


#======================================================================
def poreSat_H2Ol(c_Wl,p):
    'liquid water pore saturation'
    # s = 0 -> V_H2O_l = 0
    # s = 1 -> V_H2O_l = V_pore
    s_raw = c_Wl * p.M_W / p.dens_Wl # m3_w / m3_por
    
    # safety check:
    s = np.clip(s_raw, a_min=0.0, a_max=0.99)
    
    return s


'''
==============================================================================
        Conductivities / Diffusivities
==============================================================================
'''

def calc_map_eCond(por_arr, ion_arr, p, orientation):
    "returns map of MEA volumetric electric conductivity"
    # N. Zamel et al. / Applied Energy 93 (2012) 39–44
    # "Numerical estimation of the effective electrical conductivity in carbon paper diffusion media"
    # https://doi.org/10.1016/j.apenergy.2011.08.037
    
    if   orientation == "inPlane":
        m_Brug = p.m_Brug_x
    elif orientation == "throughPlane":
        m_Brug = p.m_Brug_y
    
    eCond_arr = p.elCon_bulk * (1 - (por_arr+ion_arr))**m_Brug
    return eCond_arr


#============================================================================== 
def calc_map_iCond(lam,p):
    "returns map of MEA volumetric ionic conductivity"
    # Meier & Eigenberger / Electrochimica Acta 49 (2004) 1731–1742
    # "Transport parameters for the modelling of water transport in ionomer membranes for PEM-fuel cells"
    # DOI: https://doi.org/10.1016/j.electacta.2003.12.004
    
    # activation energy
    Q_act = 9893.7 # J/mol
    
    # safty-adjustenment of lambda  
    lam = np.maximum(lam, 1)
    
    return (0.46*lam - 0.25) * np.exp( - Q_act/p.R * (1/p.T - 1/298.15) ) 


#==============================================================================
def calc_map_gDiff(pot, por, p, orientation):
    "returns map of diffusivities of gasses (H2, O2, N2, Wg)"     
  
    
    # correction due to porosity
    def D_eff(D0, por_corr_ac):
        "calculates effective diffusion coeff. in porouse media"
        # Zamel et al. / Energy Fuels 2009, 23, 6070–6078
        # "Correlation for the effective gas diffusion coefficient in carbon paper diffusion media"
        # DOI: https://doi.org/10.1021/ef900653x
        
        if   orientation == "throughPlane":
            A, B, C = 2.76, 3.00, 1.92
        elif orientation == "inPlane":
            A, B, C = 1.72, 2.07, 2.11
        
        func1 = lambda por: 1 - A*por * np.cosh(B*por - C) * 3*(1-por)/(3-por)
        func2 = lambda por: func1(0.35) * por/0.35
        
        return np.where(por_corr_ac > 0.35, func1(por_corr_ac), func2(por_corr_ac) ) * D0

    
    # correction due to press/temp
    def D_0corr(D0ref, p_tot):     
        "calculates diff. coeff. as funk of temp. und pressure"
        return D0ref * (p.atm/p_tot) * (p.T/p.T_ref)**1.5

    #===========================
    
    # all concentrations are >=0 (does not alter initial pot-array)
    pot      = np.maximum(0, pot)
    
    # corrected porosity   
    s        = poreSat_H2Ol(pot[1:-1,1:-1,p.iWl], p) # pore saturation
    
    por_corr = por * (1 - s)                         # corrected poreosity (reduced by liquid water)
    
    # slices    
    xV         = slice(1,-1)
    yVa        = p.yV_ELa
    yVc        = p.yV_ELc
    yVaNoFrame = np.arange(-1,pot.size)[yVa] # indices for por (has no frame)
    yVcNoFrame = np.arange(-1,pot.size)[yVc] # indices for por (has no frame)
    
    # total pressure (ano/cat)
    p_tot_a = (pot[xV,yVa,p.iH2] + pot[xV,yVa,p.iWg]                    ) * p.R * p.T
    p_tot_c = (pot[xV,yVc,p.iO2] + pot[xV,yVc,p.iN2] + pot[xV,yVc,p.iWg]) * p.R * p.T
    
    # molar fractions
    X_H2_a  = pot[xV,yVa,p.iH2] / (pot[xV,yVa,p.iH2] + pot[xV,yVa,p.iWg])
    X_H2O_a = pot[xV,yVa,p.iWg] / (pot[xV,yVa,p.iH2] + pot[xV,yVa,p.iWg])
    X_O2_c  = pot[xV,yVc,p.iO2] / (pot[xV,yVc,p.iO2] + pot[xV,yVc,p.iN2] + pot[xV,yVc,p.iWg])
    X_N2_c  = pot[xV,yVc,p.iN2] / (pot[xV,yVc,p.iO2] + pot[xV,yVc,p.iN2] + pot[xV,yVc,p.iWg])
    X_H2O_c = pot[xV,yVc,p.iWg] / (pot[xV,yVc,p.iO2] + pot[xV,yVc,p.iN2] + pot[xV,yVc,p.iWg])
    
    # standard bulk diffusion coefficients
    D_0ref_H2_a  = ( X_H2O_a / p.D_H2_H2O )**-1
    D_0ref_H2O_a = ( X_H2_a  / p.D_H2_H2O )**-1    
    D_0ref_O2_c  = ( X_N2_c  / p.D_N2_O2  + X_H2O_c / p.D_O2_H2O )**-1
    D_0ref_N2_c  = ( X_O2_c  / p.D_N2_O2  + X_H2O_c / p.D_N2_H2O )**-1
    D_0ref_H2O_c = ( X_N2_c  / p.D_N2_H2O + X_O2_c  / p.D_O2_H2O )**-1
    
    # effective diffusion coefficent (p-T-adjusted + pore corrected)
    cCond_arr = np.ones((p.xElems-2,p.yElems-2,p.nSpecs)) * 10**-50
    cCond_arr[:,yVaNoFrame,p.iH2] = D_eff( D_0corr(D_0ref_H2_a,  p_tot_a), por_corr[:,yVaNoFrame] )
    cCond_arr[:,yVaNoFrame,p.iWg] = D_eff( D_0corr(D_0ref_H2O_a, p_tot_a), por_corr[:,yVaNoFrame] )     
    cCond_arr[:,yVcNoFrame,p.iO2] = D_eff( D_0corr(D_0ref_O2_c,  p_tot_c), por_corr[:,yVcNoFrame] )
    cCond_arr[:,yVcNoFrame,p.iN2] = D_eff( D_0corr(D_0ref_N2_c,  p_tot_c), por_corr[:,yVcNoFrame] )
    cCond_arr[:,yVcNoFrame,p.iWg] = D_eff( D_0corr(D_0ref_H2O_c, p_tot_c), por_corr[:,yVcNoFrame] )    
    
    return cCond_arr


#==============================================================================
def calc_dynVisc_H2O(p):
    "returnes the dynamic viscosity of liquid water = f(Temperature)"    
    # Viswanath (1989)
    # "Data book on the viscosity of liquids"
    # ISBN: 978-0-89116-778-5
    
    A =   2.939e-5 # Pa.s
    B = 507.88     # K
    C = 149.3      # K
    return A * np.exp( B / (p.T - C) )


#==============================================================================
def calc_map_WlDiff(pot, por, p):
    "returns map of liquid water diffusivity in porouse media"
    # Z. Zhan et al. / Journal of Power Sources 160 (2006) 1041–1048
    # "Effects of porosity distribution variation on the liquid water flux 
    #  through gas diffusion layers of PEM fuel cells"
    # DOI: https://doi.org/10.1016/j.jpowsour.2006.02.060
    
    contAngle = np.deg2rad(p.contAngle)      # rad    - contact angle
    sigma     = 0.0625                           # N/m    - surface tension
    k_K       = 6                                # -      - Kozeny constant
    d_f       = 2*10**-6                         # m      - diameter of fiber
    
    s    = poreSat_H2Ol(pot[1:-1,1:-1,p.iWl], p) # -      - pore saturation
    mu   = calc_dynVisc_H2O(p)                   # N.s/m2 - dynamic viscosity of liquid water = f(T)
    rho  = p.dens_Wl                             # kg/m3  - density of liquid water
    M_W  = p.M_W                                 # kg/mol - molar mass of water
     
    dsdx_to_dcWldx = M_W / rho
    
    # clip pore saturation
    s = np.clip(s, 0.5, 1.0)
 
    D_Wl = -( sigma *np.cos(contAngle) *rho /mu 
             * por**2 * d_f /(4*(1-por)*k_K**0.5) *s**3
             * (1.417 - 4.24*s + 3.789*s**2)
             * dsdx_to_dcWldx
            )
    
    return D_Wl
 

#==============================================================================
def calc_map_WmDiff(lam, p):
    "returns map of diffusivity of adsorbed water in the membrane"     
    # Wang & Savinell / Electrochimica Acta 37 (1992) 2737-2745
    # "Simulation studies on the fuel electrode of a H2O2 polymer electrolyte fuel cell"
    # DOI: https://doi.org/10.1016/0013-4686(92)85201-U
    
    func1 = lambda lam: (-3.1   + 2.0  *lam)
    func2 = lambda lam: ( 6.89  - 1.33 *lam)
    func3 = lambda lam: ( 2.563 - 0.33 *lam + 0.0264 * lam**2 - 0.000671 * lam**3)
    
    # safety-adjustmend of lambda
    lam = np.clip(lam, 2, 22)
    
    return +(  np.where(      lam < 3,           func1(lam), 0)
             + np.where(npAND(lam < 4, lam >=3), func2(lam), 0)
             + np.where(               lam >=4,  func3(lam), 0) 
             
            ) * 10**-6 * np.exp(2416*(1/303 -  1/p.T)) / p.dens_PEM * p.EW_PEM


'''
==============================================================================
        Surface Resistances
==============================================================================
'''
    
def calc_Ry_surf(s_nodes, p):
    
    s_surf_a = ( s_nodes[:-1, 0] + s_nodes[1:, 0] ) /2
    s_surf_c = ( s_nodes[:-1,-1] + s_nodes[1:,-1] ) /2
    
    # interpolation function for surface resistance
    f_Rsurf = interp1d(p.TABLE_resSurf['p_clamp'],p.TABLE_resSurf['R_surf'])
    
    # creating  surface resistance vactors
    Ry_surf_a = -f_Rsurf(s_surf_a) * p.geoDict["xLenElems"] * p.geoDict["zLenElems"]
    Ry_surf_c = -f_Rsurf(s_surf_c) * p.geoDict["xLenElems"] * p.geoDict["zLenElems"]
    
    # outside rib R=R_inf
    Ry_surf_a[:] = np.where(p.geoDict['xElemsIsCHAa'], 1/p.mp_EPS, Ry_surf_a[:]) # Cha a
    Ry_surf_c[:] = np.where(p.geoDict['xElemsIsCHAc'], 1/p.mp_EPS, Ry_surf_c[:]) # Cha c
    
    # return statement
    return Ry_surf_a, Ry_surf_c

'''
==============================================================================
        Electro-osmotic drag coefficient
==============================================================================
'''


def calc_t_EOD(pot,p):
    "calculate electro-osmotic drag (EOD) coefficient"
    # Meier & Eigenberger / Electrochimica Acta 49 (2004) 1731–1742
    # "Transport parameters for the modelling of water transport in ionomer membranes for PEM-fuel cells"
    # DOI: https://doi.org/10.1016/j.electacta.2003.12.004
    
    lam    = lambda_from_cWm(pot[1:-1,1:-1,p.iWm], p)
    
    lam    = np.clip(lam, 2, 22)
    
    t_drag = 1.0 + 0.028 *lam + 0.0026 *lam**2         # electro-osmotic drag coefficient
    
    return t_drag


'''
==============================================================================
        Gx and Gy resistance arrays
==============================================================================
'''
     
def calc_GxGy(pot, p, plotConductances=False):

#==============================================================================
#   conductivitys cond (volumetric)
    
    por                    = np.maximum(p.por, 0)
    M_geomX, M_geomY       = p.M_geomX,  p.M_geomY
    xLenElems_mat          = p.xLenElems_mat
    yLenElems_mat          = p.yLenElems_mat
    zLenElems              = p.zLenElems
    s_nodes                = p.s_nodes
    iE, iI, iG, iWl, iWm   = p.iE, p.iI, p.iG, p.iWl, p.iWm
    xElems, yElems, nSpecs = p.xElems, p.yElems, p.nSpecs  
    V_tot, V_ion           = p.V_tot, p.V_ion
    geoDict                = p.geoDict
        
    # lambda (water content) array
    lam = lambda_from_cWm(pot[1:-1,1:-1,iWm], p)
    
    # helper function: same cond for x and y direction
    mtx_x2 = lambda mtx: np.repeat(mtx[:,:,np.newaxis], 2, axis=2)
    
    cond = np.ones([xElems, yElems, nSpecs, 2])
    
    cond[1:-1, 1:-1, iE,  0] =         calc_map_eCond(por,V_ion/V_tot, p, "inPlane"     )
    cond[1:-1, 1:-1, iE,  1] =         calc_map_eCond(por,V_ion/V_tot, p, "throughPlane")
    cond[1:-1, 1:-1, iI,  :] = mtx_x2( calc_map_iCond(lam,             p) )   
    cond[1:-1, 1:-1, iG,  0] =         calc_map_gDiff(pot, por,        p, "inPlane"     )[:,:,iG]      
    cond[1:-1, 1:-1, iG,  1] =         calc_map_gDiff(pot, por,        p, "throughPlane")[:,:,iG]
    cond[1:-1, 1:-1, iWl, :] = mtx_x2( calc_map_WlDiff(pot, por,       p) )
    cond[1:-1, 1:-1, iWm, :] = mtx_x2( calc_map_WmDiff(lam,            p) )
    
    # adjust diffusion within CL    
    cond[1:-1, p.yV_CLa, iI, :] *= p.f_ion0_CLa
    cond[1:-1, p.yV_CLa, iWm,:] *= p.f_ion0_CLa 

    cond[1:-1, p.yV_CLc, iI, :] *= p.f_ion0_CLc
    cond[1:-1, p.yV_CLc, iWm,:] *= p.f_ion0_CLc
    
    # cutout frame
    cond = cond[1:-1,1:-1,:]

#==============================================================================
#   conductions Gx Gy


    Gy = np.zeros((xElems,  yElems-1,nSpecs))
    Gx = np.zeros((xElems-1,yElems,  nSpecs))


    Gx[1:-1,1:-1,:] = (cond[ :-1, :  ,:,0]
                     + cond[1:  , :  ,:,0] ) / 2 *M_geomX
    Gy[1:-1,1:-1,:] = (cond[ :  , :-1,:,1]
                     + cond[ :  ,1:  ,:,1] ) / 2 *M_geomY
    
    # diffusion blockades
    Gx[:,p.yV_PEM, iE]  = 0
    Gx[:,p.yV_PEM, iG]  = 0
    Gx[:,p.yV_PEM, iWl] = 0
    Gx[:,p.yV_GDLa,iI]  = 0
    Gx[:,p.yV_GDLc,iI]  = 0
    Gx[:,p.yV_GDLa,iWm] = 0
    Gx[:,p.yV_GDLc,iWm] = 0
    
    Gy[:,p.yPEM_0-1 :p.yCLc_0  ,iE]  = 0
    Gy[:,p.yPEM_0-1 :p.yCLc_0  ,iG]  = 0
    Gy[:,p.yPEM_0-1 :p.yCLc_0  ,iWl] = 0
    Gy[:,    0      :p.yCLa_0  ,iI]  = 0
    Gy[:,p.yGDLc_0-1:p.xElems  ,iI]  = 0
    Gy[:,    0      :p.yCLa_0  ,iWm] = 0
    Gy[:,p.yGDLc_0-1:p.xElems  ,iWm] = 0
    
    # Surface Electric
    Ry_surf_a, Ry_surf_c = calc_Ry_surf(s_nodes, p)
    
    yGeom_surf_a = xLenElems_mat[:, 0] * zLenElems / (yLenElems_mat[:, 0] /2 )
    yGeom_surf_c = xLenElems_mat[:,-1] * zLenElems / (yLenElems_mat[:,-1] /2 )

    Gy[1:-1,  0   ,iE]  = 1/(1/(cond[:, 0,iE,1] *yGeom_surf_a) + Ry_surf_a)   # anode bpp contact
    Gy[1:-1, -1   ,iE]  = 1/(1/(cond[:,-1,iE,1] *yGeom_surf_c) + Ry_surf_c)   # cathode bpp contact
        
    # Surface Chemicals
    Gy[1:-1, 0 ,iG]  = 1/(1/(cond[:, 0,iG, 1] *np.repeat(yGeom_surf_a[:,np.newaxis],4,axis=1)) + p.Ry_surfTransChem)     # anode   bpp contact
    Gy[1:-1,-1 ,iG]  = 1/(1/(cond[:,-1,iG, 1] *np.repeat(yGeom_surf_c[:,np.newaxis],4,axis=1)) + p.Ry_surfTransChem)     # cathode bpp contact    
    Gy[1:-1, 0 ,iWl] = 1/(1/(cond[:, 0,iWl,1] *          yGeom_surf_a[:]                     ) + p.Ry_surfTransChem)     # anode   bpp contact liquid water
    Gy[1:-1,-1 ,iWl] = 1/(1/(cond[:,-1,iWl,1] *          yGeom_surf_c[:]                     ) + p.Ry_surfTransChem)     # cathode bpp contact liquid water
    
    
#=================================
# BPP
    
    Gy[1:-1, 0,iE] = np.where(geoDict['xElemsIsCHAa'], 0, Gy[1:-1, 0,iE]) # Cha a
    Gy[1:-1,-1,iE] = np.where(geoDict['xElemsIsCHAc'], 0, Gy[1:-1,-1,iE]) # Cha c
    
    Gy[1:-1, 0,iG]  = np.where(np.repeat(geoDict['xElemsIsRIBa'][:,np.newaxis],4,axis=1), 0, Gy[1:-1, 0,iG]) # Rib a
    Gy[1:-1,-1,iG]  = np.where(np.repeat(geoDict['xElemsIsRIBc'][:,np.newaxis],4,axis=1), 0, Gy[1:-1,-1,iG]) # Rib c
    Gy[1:-1, 0,iWl] = np.where(geoDict['xElemsIsRIBa'], 0, Gy[1:-1, 0,iWl]) # Rib a
    Gy[1:-1,-1,iWl] = np.where(geoDict['xElemsIsRIBc'], 0, Gy[1:-1,-1,iWl]) # Rib c


#=================================    
# negative resistances!
    Gx = -Gx
    Gy = -Gy  
    
#=================================
# Plotting
    
    if plotConductances:
        plot.conductances(Gx, Gy, geoDict, p, winTitle= "conductions initial")
    
    return Gx, Gy
