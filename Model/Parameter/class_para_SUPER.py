# -*- coding: utf-8 -*-
"""
Created on Wed Mar 22 11:14:56 2023

@author: Marku
"""

from __future__ import division, absolute_import, \
                       print_function#, unicode_literals

'''=== general imports ==='''
import numpy             as np
import matplotlib.pyplot as plt


'''=== user defined imports ==='''
import MultiPhysModel.gridSetup as gridSetup


'''
==============================================================================
        Super class: Parameters
==============================================================================
'''


class Parameters_super():
    """ Super class to all parameter classes. 
        Contains preprocessing and processing functions of input data. """
        
     
    '''
    #=====================================================================#
    #        Preprocessing                                                #
    #=====================================================================#
    '''
  
    def input_preprocessing(self):
        
        '''=== dictionaries ==='''
        
        self.geoDict     = self.create_geoDict()
        self.paraDict_AM = self.create_paraDict()

        self.update_xElems_yElems()               
        
        '''=== species indices ===''' 
        
        self.nSpecs =  8
        self.iE   = 0
        self.iI   = 1
        self.iH2  = 2
        self.iO2  = 3
        self.iN2  = 4
        self.iWg  = 5
        self.iWl  = 6
        self.iWm  = 7
        self.iG   = slice(2,6) # gases
        self.iW   = slice(5,8) # water
        self.iC   = slice(2,8) # chemicals = gas + water
        
        
        '''=== indices and slices ==='''
        
        self.yBPPa   = 0                                             #  0
        self.yGDLa_0 =  self.yBPPa   +  1                            #  1     1      \
        self.yCLa_0  =  self.yGDLa_0 +  self.geoDict["yElemsGDLa"]   # 11    10 \     |
        self.yPEM_0  =  self.yCLa_0  +  self.geoDict["yElemsCLa"]    # 14     3  |    |
        self.yCLc_0  =  self.yPEM_0  +  self.geoDict["yElemsPEM"]    # 20     6  |35  |37 
        self.yGDLc_0 =  self.yCLc_0  +  self.geoDict["yElemsCLc"]    # 26     6  |    |
        self.yBPPc   =  self.yGDLc_0 +  self.geoDict["yElemsGDLc"]   # 36    10 /     | 
                                                                    
        # xElems & yElems is with frame!
               
        # y-volume -> in FRAME!
        self.yV_GDLa = slice( self.yGDLa_0,  self.yCLa_0 )
        self.yV_CLa  = slice( self.yCLa_0,   self.yPEM_0 )
        self.yV_PEM  = slice( self.yPEM_0,   self.yCLc_0 )
        self.yV_CLc  = slice( self.yCLc_0,   self.yGDLc_0)
        self.yV_GDLc = slice( self.yGDLc_0,  self.yBPPc  )
        
        self.yV_ELa  = slice( self.yGDLa_0, self.yPEM_0 )
        self.yV_ION  = slice( self.yCLa_0,  self.yGDLc_0)
        self.yV_ELc  = slice( self.yCLc_0,  self.yBPPc  )
        
        # y-interface area
        self.yA_GDLa = slice(           0,  self.yCLa_0 -1)
        self.yA_CLa  = slice( self.yCLa_0,  self.yPEM_0 -1)
        self.yA_PEM  = slice( self.yPEM_0,  self.yCLc_0 -1)
        self.yA_CLc  = slice( self.yCLc_0,  self.yGDLc_0-1)
        self.yA_GDLc = slice( self.yGDLc_0, self.yElems -1)
        
        self.yA_ELa  = slice(           0, self.yPEM_0 -1)
        self.yA_ION  = slice( self.yCLa_0, self.yGDLc_0-1)
        self.yA_ELc  = slice( self.yCLc_0, self.yElems -1)
    


        '''=== chemical ==='''
        
        # exchange current density parameter
        self.i3_ex0_HOR = self.L_Pt_HOR * self.ECSA_HOR * self.i2_ex0ref_HOR * np.exp(- self.Q_HOR/(self.R*self.T_ref) * (1-self.T/self.T_ref) )
        self.i3_ex0_ORR = self.L_Pt_ORR * self.ECSA_ORR * self.i2_ex0ref_ORR * np.exp(- self.Q_ORR/(self.R*self.T_ref) * (1-self.T/self.T_ref) )
        
        # standard potential (temperature corrected)
        self.E0_HOR = self.E0_HOR_ref - 0.85*10**-3 * (self.T - self.T_ref)
        self.E0_ORR = self.E0_ORR_ref - 0.85*10**-3 * (self.T - self.T_ref)

        # reaction matrixes    
        #                  E   I                     H2                     O2 N2                   Wg Wl Wm
        self.ReactVec_a = [1, -1,  +1/self.z_HOR/self.F,                    0, 0,                    0, 0, 0] # stoichiometry vector: HOR (ano)
        self.ReactVec_c = [1, -1,                     0, -1/self.z_ORR/self.F, 0, +2/self.z_ORR/self.F, 0, 0] # stoichiometry vector: ORR (cat)
        
        '''=== hydrophobicity (contact anglem) ==='''
        
        self.contAngle  = self.calc_map_contAngle()
        
        '''=== hydrophobicity (contact anglem) ==='''
        
        self.m_Brug_x   = self.calc_map_mBrug("inPlane")
        self.m_Brug_y   = self.calc_map_mBrug("throughPlane")
        self.elCon_bulk = self.calc_map_elConGDLbulk()

        '''=== mechanical ==='''
        
        self.update_allMechanic(s_nodes=None)
    
        
        '''=== geometrical ==='''
        
        self.update_allGeometric()




    '''
    ==============================================================================
            Functions: Update
    ==============================================================================
    '''


    def update_allMechanic(self, s_nodes):
        
        if type(s_nodes) == type(None):
            self.s_nodes = self.create_s_nodes_const()
        else:
            self.s_nodes = s_nodes
        
        self.s          = self.calc_map_s(  self.s_nodes)
        self.e          = self.calc_map_e(  self.s)
        self.por        = self.calc_map_por(self.e)
        self.ion        = self.calc_map_ionFract(self.e)
        self.contAngle  = self.calc_map_contAngle()
        
        self.mBrug_x    = self.calc_map_mBrug("inPlane")
        self.mBrug_y    = self.calc_map_mBrug("throughPlane")
        self.elCon_bulk = self.calc_map_elConGDLbulk()
        
        return True


#==============================================================================
    def update_allGeometric(self):
                
        self.xLenElems_mat = np.tile(self.geoDict["xLenElems"], (self.yElems-2, 1)).transpose()
        self.yLenElems_mat = np.tile(self.geoDict["yLenElems"] ,(self.xElems-2, 1)) * (1+self.e[:,:])
        self.zLenElems     =         self.geoDict["zLenElems"]   
          
        self.len_xDiff  = (self.xLenElems_mat[:-1,:  ] + self.xLenElems_mat[1:, :]) /2
        self.len_yDiff  = (self.yLenElems_mat[:  ,:-1] + self.yLenElems_mat[ :,1:]) /2
        self.area_xDiff = (self.yLenElems_mat[:-1,:  ] + self.yLenElems_mat[1:, :]) /2 * self.zLenElems
        self.area_yDiff = (self.xLenElems_mat[:  ,:-1] + self.xLenElems_mat[ :,1:]) /2 * self.zLenElems
        
        self.M_geomX = np.repeat( (self.area_xDiff/self.len_xDiff)[:,:,np.newaxis], self.nSpecs,axis=2)
        self.M_geomY = np.repeat( (self.area_yDiff/self.len_yDiff)[:,:,np.newaxis], self.nSpecs,axis=2)  
        
        self.V_tot = self.xLenElems_mat * self.yLenElems_mat * self.zLenElems
        self.V_por = self.V_tot * self.por
        self.V_ion = self.V_tot * self.calc_map_ionFract(self.e)
        
        return True


#==============================================================================
    def update_xElems_yElems(self):
        
        self.xElems = self.geoDict["xElemsMEA"] + 2
        self.yElems = self.geoDict["yElemsMEA"] + 2
        
        return True


#==============================================================================
    def update_all(self, s_nodes):
        
        self.update_xElems_yElems()
        self.update_allMechanic(s_nodes)
        self.update_allGeometric()
              
        return True
    
    
#==============================================================================
    def update_geoDict(self, mode="MPonly", allRegs=None, doPlot=False):
        self.geoDict = self.create_geoDict(mode, allRegs, doPlot)
        self.xElems  = self.geoDict["xElemsMEA"] +2
        self.yElems  = self.geoDict["yElemsMEA"] +2
        return True


    '''
    ==============================================================================
            Functions: calculate / create
    ==============================================================================
    '''
 
    def create_s_nodes_const(self):    
        # dummy stress array @nodes -> this is how the array comes out of the analyt approach
        s_nodes = - np.ones((self.xElems-1, self.yElems-1)) * self.s_clamp_const
        return s_nodes


#==============================================================================  
    def calc_map_s(self, s_nodes):            
        # stress array inside elements
        s_arr   = (  s_nodes[ :-1, :-1] + s_nodes[1:  , :-1] 
                   + s_nodes[ :-1,1:  ] + s_nodes[1:  ,1:  ] ) / 4
        return s_arr


#==============================================================================
    def calc_map_e(self, s_arr):
        "returns map of MEA normal y-strain"
        e_arr      = s_arr / self.Ey_GDL
        return e_arr
    
    
#==============================================================================        
    def calc_map_por(self, e_arr):
        "returns map of MEA porosities (compressed)"
        
        # create a frame around e_arr to fit the index-slices
        e_frame = np.zeros((self.xElems,self.yElems))
        e_frame[1:-1,1:-1] = e_arr

        por_arr = np.zeros((self.xElems,self.yElems))
        
        por_arr[1:-1,self.yV_GDLa] = 1 - (1 - self.f_por0_GDLa) / (1 + e_frame[1:-1,self.yV_GDLa]) 
        por_arr[1:-1,self.yV_CLa ] = 1 - (1 - self.f_por0_CLa ) / (1 + e_frame[1:-1,self.yV_CLa ]) 
        por_arr[1:-1,self.yV_PEM ] = 0
        por_arr[1:-1,self.yV_CLc ] = 1 - (1 - self.f_por0_CLc ) / (1 + e_frame[1:-1,self.yV_CLc ]) 
        por_arr[1:-1,self.yV_GDLc] = 1 - (1 - self.f_por0_GDLc) / (1 + e_frame[1:-1,self.yV_GDLc]) 
        
        # correction to avoid porosities <= 0
        por_arr[1:-1,1:-1] = np.maximum( 10**(-50), por_arr[1:-1,1:-1] )
        
        return por_arr[1:-1,1:-1]


#==============================================================================        
    def calc_map_ionFract(self, e_arr):
        "returns map of MEA porositys"
        
        ion_arr = np.zeros((self.xElems,self.yElems)) 
        ion_arr[1:-1,self.yV_CLa] = self.f_ion0_CLa 
        ion_arr[1:-1,self.yV_PEM] = 1
        ion_arr[1:-1,self.yV_CLc] = self.f_ion0_CLc
        return ion_arr[1:-1,1:-1]


#==============================================================================
    def calc_map_contAngle(self):
        "creates map of contact angles of hydrophobicity"
        
        cA_arr = np.zeros((self.xElems,self.yElems))
        
        cA_arr[1:-1,self.yV_GDLa] = self.contAngle_GDLa 
        cA_arr[1:-1,self.yV_CLa ] = self.contAngle_CLa 
        cA_arr[1:-1,self.yV_PEM ] = 0
        cA_arr[1:-1,self.yV_CLc ] = self.contAngle_CLc 
        cA_arr[1:-1,self.yV_GDLc] = self.contAngle_GDLc 
        
        return cA_arr[1:-1,1:-1]        

#============================================================================== neu
    def calc_map_mBrug(self, orientation):
        "creates map of contact angles of hydrophobicity"
    
        mBrug_arr = np.zeros((self.xElems,self.yElems))        
        
        if   orientation == "inPlane":
            mBrug_arr[1:-1,self.yV_GDLa] = self.m_Brug_GDLa_x 
            mBrug_arr[1:-1,self.yV_CLa ] = self.m_Brug_CLa_x 
            mBrug_arr[1:-1,self.yV_PEM ] = 0
            mBrug_arr[1:-1,self.yV_CLc ] = self.m_Brug_CLc_x 
            mBrug_arr[1:-1,self.yV_GDLc] = self.m_Brug_GDLc_x 
        
        elif orientation == "throughPlane":
            mBrug_arr[1:-1,self.yV_GDLa] = self.m_Brug_GDLa_y
            mBrug_arr[1:-1,self.yV_CLa ] = self.m_Brug_CLa_y 
            mBrug_arr[1:-1,self.yV_PEM ] = 0
            mBrug_arr[1:-1,self.yV_CLc ] = self.m_Brug_CLc_y 
            mBrug_arr[1:-1,self.yV_GDLc] = self.m_Brug_GDLc_y 
        
        return mBrug_arr[1:-1,1:-1] 
    
#============================================================================== neu
    def calc_map_elConGDLbulk(self):
        "creates map of contact angles of hydrophobicity"
        
        elCon_arr = np.zeros((self.xElems,self.yElems))
        
        elCon_arr[1:-1,self.yV_GDLa] = self.elCon_GDLa_bulk
        elCon_arr[1:-1,self.yV_CLa ] = self.elCon_CLa_bulk
        elCon_arr[1:-1,self.yV_PEM ] = 0
        elCon_arr[1:-1,self.yV_CLc ] = self.elCon_CLc_bulk
        elCon_arr[1:-1,self.yV_GDLc] = self.elCon_GDLc_bulk
        
        return elCon_arr[1:-1,1:-1] 


#==============================================================================        
    def create_geoDict(self, mode="MPonly", allRegs=None, doPlot=False):
    
        geoDict = gridSetup.calc_xLen_yLen(self, mode=mode, allRegs=allRegs, doPlot=doPlot)

        geoDict.update({"zLenElems" : self.zElemLen})
        
        return geoDict


#==============================================================================  
    def create_paraDict(self):
        pD = {'X_ribB': self.l_RIBa *10**6/2,          'X_ribT' :self.l_RIBc *10**6/2, 
              'R_ribB': self.R_RIBa *10**6,            'R_ribT' :self.R_RIBc *10**6,
              'X_chB' : self.l_CHAa *10**6/2,          'X_chT'  :self.l_CHAc *10**6/2,
              'H_mea' :(self.d_GDLa
                       +self.d_GDLc 
                       +self.d_CLa 
                       +self.d_CLc 
                       +self.d_PEM ) *10**6,
              'Ey'    : self.Ey_GDL  *10**(-6),
              'G'     : self.Gxy_GDL *10**(-6),    
              'k'     : self.k_Timo,
                         
              'n_func'     : self.am_n_func,
              'xNodes_reg' : self.am_xNodes_reg,
              'xNodes_hom' : self.am_xNodes_hom,
              'yNodes'     : self.am_yNodes
              }
        
        # round geometrical parameters to full integers -> full micro-meters
        for key in ['X_ribB','X_ribT','R_ribB','R_ribT','X_chB','X_chT','H_mea']:
            pD[key] = int(round(pD[key],0))
        
        return pD


    '''
    ==============================================================================
            Functions: Plot
    ==============================================================================
    '''    
    
    def plot_Vol_and_Por(self):    
        
        fig, ax = plt.subplots(2,2)
        ax = [ax[0,0],ax[0,1],ax[1,0],ax[1,1]]
        im = [None]*4
        
        im[0] = ax[0].imshow(self.por.transpose(),   cmap='jet')
        im[1] = ax[1].imshow(self.V_tot.transpose(), cmap='jet')
        im[2] = ax[2].imshow(self.V_por.transpose(), cmap='jet')
        im[3] = ax[3].imshow(self.V_ion.transpose(), cmap='jet')
        
        title_list = ["porosity", "V_tot", "V_por", "V_ion"]
        
        for idx in range(4):
            ax[idx].set_title(title_list[idx])
            ax[idx].invert_yaxis()
            plt.colorbar(im[idx], ax=ax[idx], orientation="horizontal")

