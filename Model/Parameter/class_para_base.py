# -*- coding: utf-8 -*-
"""
Created on Fri Nov  4 17:57:57 2022

@author: kohrn
"""

from __future__ import division, absolute_import, \
                       print_function#, unicode_literals

'''=== general imports ==='''
import numpy             as np
import matplotlib.pyplot as plt


'''=== user defined imports ==='''
import MultiPhysModel.gridSetup as gridSetup

'''=== import super class ==='''
from Parameter.class_para_SUPER import Parameters_super as c_para_super


'''
==============================================================================
        Class: Parameters
==============================================================================
'''

class Parameters(c_para_super):
    
    def __init__(self, alterDict=None):

        '====== Geometry ==========='
        
        self.d_GDLa = 190 * 10**(-6)      # m        - thickness of GDLa
        self.d_GDLc = 190 * 10**(-6)      # m        - thickness of GDLc
        self.d_CLa  =   4 * 10**(-6)      # m        - thickness of CLa
        self.d_CLc  =   8 * 10**(-6)      # m        - thickness of CLc
        self.d_PEM  =   8 * 10**(-6)      # m        - thickness of PEM
        
        self.l_RIBa =1200 * 10**(-6)      # m        - lenght of half-rib (anode)
        self.l_RIBc =2000 * 10**(-6)      # m        - lenght of half-rib (cathode)
        
        self.l_CHAa = 800 * 10**(-6)      # m        - lenght of half-channel (anode)
        self.l_CHAc =1000 * 10**(-6)      # m        - lenght of half-channel (cathode)
        
        self.R_RIBa = 200 * 10**(-6)      # m        - BPP rib radius (anode)
        self.R_RIBc = 200 * 10**(-6)      # m        - BPP rib radius (cathode)
        
        self.yMinElemsPerLayer =  2       # -        - minimum nr. of y-elements in smallest layer
        self.yyMaxRatio        =  1.5     # -        - maximum y-ratio between connected elements
        self.xyMaxRatio        = 10       # -        - maximum ratio between x- and y-length
        
        self.zElemLen          =  1       # -        - "virtual" element lenght in z-direction      
        
        '====== Constants =========='
        
        self.F   =  96485.0               # A.s/mol  - Young's modulus of GDL 
        self.R   =      8.314             # J/mol.K  - Universal gas constant (molar)
        self.atm = 101325.0               # Pa       - Pressure of 1 atmosphere
        
        '==== Operating parameters ='
        
        self.T             = 273.15 + 70  # K        - Fuel Cell Temperature (constant)
        self.OCV_fract     =   0.5        # -        - fraction of open current voltage
        self.s_clamp_const =   2.0*10**6  # Pa       - constant clamping pressure -------> not used in coupled sim!!!       
        self.Del_H0        =  80.0        # mym      - constant clamping displacement
        self.p_tot_a       = self.atm *3  # Pa       - total pressure at anode
        self.p_tot_c       = self.atm *3  # Pa       - total pressure at cathode
        
        '====== Mechanical ========='
        
        self.Ey_GDL   = 8.79  * 10**(6)   # Pa       - Young's modulus of GDL (y-direction)
        self.Ey_CL    = 126.1 * 10**(6) 
        self.Ey_PEM   = 31.2  * 10**(6)
        
        self.Gxy_GDL  = 8.55  * 10**(6)   # Pa       - Shear modulus of GDL   
        self.Gxy_CL   = 50.44 * 10**(6)   # Pa       - ...           of CL 
        self.Gxy_PEM  = 12.48 * 10**(6)   # Pa       - ...           of PEM
               
        self.k_Timo   = 0.8333            # -        - Timoschenko parameter
        
        '====== Chemicals =========='
        
        # initial concentrations of chemicals (dry)
        self.x_H2 = 1.0                   # -        - molar fraction of H2 in anode - dry
        self.x_O2 = 0.21                  # -        - molar fraction of O2 in cathode - dry
        self.x_N2 = 0.79                  # -        - molar fraction of N2 in cathode - dry
        
        self.RH_ano = 0.90                # -        - Relative humidity at anode
        self.RH_cat = 0.90                # -        - Relative humidity at cathode
        
        # Diffusion coefficiens
        self.D_H2_H2O =  80.62 *10**-6    # m2/s  - Binary Diff.coeff. H2/H2O
        self.D_N2_H2O =  20.63 *10**-6    # m2/s  - Binary Diff.coeff. N2/H2O
        self.D_O2_H2O =  21.05 *10**-6    # m2/s  - Binary Diff.coeff. O2/H2O
        self.D_N2_O2  =  20.28 *10**-6    # m2/s  - Binary Diff.coeff. N2/O2
        
        self.M_W     =   0.01801528       # kg/mol   - molar weight of H2O
        self.dens_Wl = 997.0              # kg/m3    - density of liquid water
        
        self.Ry_surfTransChem = 1.0       # m/s      - Gas transfere coeff. Channel <-> GDL surface
        
        '====== Membrane ==========='
        
        self.dens_PEM      = 1970         # mol/m3   - charge density of PEM
        self.EW_PEM        =    1.1       #          - equivalent weight
        self.N_H2O_per_SO3 =   22         # mol/mol  - mole H2O / mol SO3- in PEM
        
        '====== Reactions =========='
        
        # HOR - anode
        self.z_HOR         =   2.0           # -        - electron valence (H2)
        self.E0_HOR_ref    =   0.0           # V        - Electrode standard potential
        
        self.ECSA_HOR      =  50   *10**+3     # m2/kg_Pt - Electrochemically active surface area
        self.L_Pt_HOR      =   1   *10**+3     # kg_Pt/m³ - Catalyst Loading
        self.i2_ex0ref_HOR =   1   *10**+1     # A/m2_Pt  - Exchange current density (per surface Pt)
        self.beta_HOR      =   0.5             # -        - exponent (a_red)**beta (exchange current density)
        self.Q_HOR         =  29.0 *10**+3     # J/kg     - Activation energy for exchange current density 
        self.ctc_HOR       =   0.5             # -        - Charge transfere coefficient (ButlerVolmer)
        
        
        # ORR - cathode
        self.z_ORR         =   4.0           # -        - electron valence (O2)
        self.E0_ORR_ref    =   1.23          # V        - Electrode standard potential
        
        self.ECSA_ORR      =  50   *10**+3     # m2/kg_Pt - EleElectrochemically active surface area
        self.L_Pt_ORR      =   2   *10**+3     # kg_Pt/m³ - Catalyst Loading
        self.i2_ex0ref_ORR =   1   *10**-5     # A/m2_Pt  - Exchange current density (per surface Pt)
        self.beta_ORR      =   0.5             # -        - exponent (a_red)**beta (exchange current density)
        self.Q_ORR         =  66.0 *10**+3     # J/mol    - Activation energy for exchange current density 
        
        self.ctc_ORR       =   0.5             # -        - Charge transfere coefficient (ButlerVolmer)     
        
        
        # overall 
        self.T_ref         = 273.15 + 25          # K        - Reference temperature for el-chem. reaction   

        '====== Gas Diff. Layer ========='
        
        self.m_Brug_x       =   1.7       # -        - Bruggeman exponent (in-plane)
        self.m_Brug_y       =   3.4       # -        - Bruggeman exponent (through-plane)
        self.elCon_GDL_bulk = 1000*10**3  # S/m      - electric conductivity of GDL bulk material
        
        self.f_por0_GDLa    = 0.63        # -        - initial porosity of GDL
        self.f_por0_GDLc    = 0.63        # -        - initial porosity of GDL
        
        # Interpolation dictionary for surface resistance f(y-stress)
        self.TABLE_resSurf = {'p_clamp':np.array([ +10.0, +0.001, -0.25, -0.35, -0.6, -0.85, -2.0, -2.5, -3.0, -3.5, -10.0]),  # clamping press  in MPa
                              'R_surf' :np.array([  -100,   -100,   -40,   -30,  -18, -14.5,   -8,   -7,   -6,   -5,    -3])}  # surf resistence in mOhm/cm^2            
        # Adjusting units for surface resistance -> Ohm/mym^2
        self.TABLE_resSurf['p_clamp'] *= 10**(6)      # MPa -> Pa
        self.TABLE_resSurf['R_surf' ] *= 10**(1)      # mOhm/cm2 -> Ohm/m2
        
        self.contAngle_GDLa = 140         # °        - water contact angle at GDLa
        self.contAngle_GDLc = 140         # °        - water contact angle at GDLc        
             
        
        '====== Catalyst Layer ====='
        
        self.f_por0_CLa = 0.38            # -        - pore volumen fraction in CLa
        self.f_por0_CLc = 0.38            # -        - pore volumen fraction in CLc
        self.f_ion0_CLa = 0.26            # -        - ionomer volumen fraction in CLa
        self.f_ion0_CLc = 0.26            # -        - ionomer volumen fraction in CLc
        
        self.contAngle_CLa = 140          # °        - water contact angle at CLa
        self.contAngle_CLc = 140          # °        - water contact angle at CLa        
              
        '====== Numerical =========='
        
        # Multi-physics Model
        self.mp_loops_max         = 1*10**(6)   # -        - maximum number of loops before giveup
        self.mp_loops_recalc_G    = 1           # -        - interval after witch G-Matrix is recalculated
        self.mp_loops_check       = 1000        # -        - interval after witch sim progress is checked
        
        self.mp_fNum_pot          =    0.95     # -        - Num. factor in potential calculation
        self.mp_fNum_glEq         =    0.95     # -        - Num. factor in water equilibrium: gas-liquid
        self.mp_fNum_gmEq         = 1000.0      # -        - Num. factor in water equilibrium: gas-pem
        self.mp_fNum_EOD          =    0.001    # -        - Num. factor in electro-osmotic drag
        self.mp_fNum_actPot       =    0.001    # -        - Num. factor in activation potential
        
        self.mp_fNum_cutback_init =    1.0      # -        - initial cutback value (should be 1)    
        self.mp_fNum_cutback_min  =    0.00001  # -        - Numerical factor minimum cutback
        
        
        self.mp_threshold_curBalRel_Mean = 0.001     # - threshold for mean relative currend balance
        self.mp_threshold_curBalRel_Max  = 0.100     # - threshold for max. relative current balance
        
        self.mp_EPS         = 2.2250738585072014e-308   # - smallest possible value >0 in python
        
        
        # Analyt Model               
        self.am_xNodes_reg =   50
        self.am_xNodes_hom = 1000
        self.am_yNodes     =  101
        self.am_n_func     = self.n_func_1
        
        
        # Gap Closure      
        self.gc_threshold_relChange = 0.01  # -        - Threshold for relative change of f_C_h and f_C_t


        # Smoothing (sm)     
        self.sm_loopsMax        = 10**6     # -        - Maximum number of iteration loops
        self.sm_f_adjust        = 10**-4    # -        - Adjustment factor (num. step-width)
        self.sm_threshold_Fmean = 0.01      # -        - Threshold for mean force inbalance
        self.sm_threshold_Fmax  = 0.1       # -        - Threshold for max. force inbalance
        
        
        '''=== alternate attributes with input dictionary ==='''
        
        if alterDict != None:
            self.__dict__.update(alterDict)
         
            
        '''=== Run data preprocessing ==='''
        
        self.input_preprocessing()

    #===========================================================================
    
    def n_func_1(self, paraDict, BotTop):#==============================    
        C_n, a_H, a_R  =  3.208258, 0.367815, -0.396213                #   
        if   BotTop == "Bot":                                          #
            n = C_n * paraDict['H_mea']**a_H * paraDict['R_ribB']**a_R #
        elif BotTop == "Top":                                          #
            n = C_n * paraDict['H_mea']**a_H * paraDict['R_ribT']**a_R #          
        return n #======================================================
