# -*- coding: utf-8 -*-
"""
Created on Fri Jan 28 15:19:38 2022

@author: kohrn
"""

from __future__ import division, absolute_import, \
                       print_function#, unicode_literals

# %matplotlib qt

''' import standard libraries'''
import os
import numpy             as np
import matplotlib.pyplot as plt

''' imoprt own function classes '''

# swith cwd to parent directory, if not yet done
if os.path.split(os.getcwd())[1] == "AnalytModel":
    os.chdir("..")
    
import AnalytModel.AM2_mapRegimes    as c_mapRegs

'''
===============================================================================
                    Create Plot Set-up
===============================================================================
'''

def get_plot2x2(pD, x_list, Del_h0, winTitle=None):
    
    fig, subPlts = plt.subplots(2,2)
    subPlts = (subPlts[0,0], subPlts[0,1], subPlts[1,0], subPlts[1,1])
    
    if winTitle != None: fig.canvas.manager.set_window_title(winTitle)
    
    titles  = ["Height after Deformation", "Displacement", "Normal Stress", "Shear Stress"]
    xlabels = [r'length $x$ / $\mu$m'] *4
    ylabels = [r"heigt $h$ / $\mu$m", r"disp $u$ / $\mu$m", r"y-stress $\sigma$ / MPa", r"shear $\tau$ / MPa"]
    #xlims   = [[0,max(x_list)/2]] *4
    xlims   = [[0,max(x_list)]] *4
    #ylims   = [[-420,420],[-55,55],[-5,1],[-4,4]]
    #ylims   = [[-420,420],[-130,130],[-14,1],[-8.5,8.5]]
    ylims   = [[-420,420],[-70,70],[-14,1],[-4.5,4.5]]   # standard
    #ylims   = [[-300,300],[-70,70],[-3,1],[-1,2]]
    
    for sP, title, xlabel, ylabel, xlim, ylim in zip(subPlts, titles, xlabels, ylabels, xlims, ylims):
        sP.set_title(title)
        sP.set_xlabel(xlabel)
        sP.set_ylabel(ylabel)
        sP.set_xlim(xlim)
        sP.set_ylim(ylim)
    
#=====================
# Shape of Ribs
            
    xT = np.linspace(0,max(x_list),10000)
    xB = xT
    yT = c_mapRegs.BPP(xT, pD["X_ribT"]*2, pD["X_chT"]*2, 250, pD["R_ribT"], pD["H_mea"]-Del_h0, "Top")
    yB = c_mapRegs.BPP(xB, pD["X_ribB"]*2, pD["X_chB"]*2, 250, pD["R_ribB"], pD["H_mea"]-Del_h0, "Bot")
    
    subPlts[0].plot(xT, yT, '--', c='k')
    subPlts[0].plot(xB, yB, '--', c='k')

#=====================
# regime boundaries
    
    for x in x_list:
        subPlts[0].axvline(x, c='grey')
        subPlts[1].axvline(x, c='grey')
        subPlts[2].axvline(x, c='grey')
        subPlts[3].axvline(x, c='grey')
    
    return fig, subPlts

'''
===============================================================================
                    Plot: From plotList | from Homogenized List
===============================================================================
'''               


fig2, subPlts2 = plt.subplots()
subPlts2.set_xlabel(r'length $x$ / $\mu$m')
subPlts2.set_ylabel(r'shear $\tau$/$ / MPa')
fig2.canvas.manager.set_window_title("shear - simulation vs. post-calculation")
subPlts2.set_xlim([350,850])
subPlts2.set_ylim([-1.8,1.8])


fig3, subPlts3 = plt.subplots()
subPlts3.set_xlabel(r'length $x$ / $\mu$m')
subPlts3.set_ylabel(r'deviation of shear $\partial\tau$/$\partial x$ / MPa/m$\mu$m')
fig3.canvas.manager.set_window_title("dtdx - simulation vs. post-calculation")


lineStyle = "-"
label     = 'initial'

'''
===============================================================================
'''   

def plot_fromPlotList(allRegs, Del_h0, mode="default", winTitle=None, extraPlots=False):
    "Plots results from disconnected regime maps-sets"
    
    reg_list = allRegs.reg_list
    x_list   = allRegs.xBdy_list()
    pD       = allRegs.pD
    
    fig, subPlts = get_plot2x2(pD, x_list, Del_h0, winTitle)

    for reg in reg_list:
        try:
            xElems,yElems = reg.maps['h'].shape
            if mode == "default" and yElems > 11:
                interval = int(yElems/10)
            else:
                interval = 1 
            
            subPlts[0].plot(reg.maps['x'], reg.maps['h'][:,::interval])
            subPlts[1].plot(reg.maps['x'], reg.maps['u'][:,::interval])
            subPlts[2].plot(reg.maps['x'], reg.maps['s'][:,::interval])    
            subPlts[3].plot(reg.maps['x'], reg.maps['t'][:,::interval])

        except:
            pass
        
        if extraPlots:
            __plot_extraPlots(allRegs, Del_h0)

    return True

#==============================================================================

def __plot_extraPlots(allRegs, Del_h0):
    "Plots results from disconnected regime maps-sets"
    
    reg_list = allRegs.reg_list
    x_list   = allRegs.xBdy_list()
    pD       = allRegs.pD
    
    for x in x_list:
        subPlts2.axvline(x, c='grey', linestyle=lineStyle)
        subPlts3.axvline(x, c='grey', linestyle=lineStyle)

    for reg in reg_list:

        try:            
            xElems,yElems = reg.maps['h'].shape
            
            def _isFirst(label):
                if reg.idx == 0:
                    return label
                else:
                    return None
            
            # extra plot: shear - post-calculated (from height) AND simulated
            t_calc_B = (reg.maps['h'][1:, 0] - reg.maps['h'][:-1, 0] ) /reg.xLen() * (xElems-1) *pD['k']*pD['G']
            t_calc_T = (reg.maps['h'][1:,-1] - reg.maps['h'][:-1,-1] ) /reg.xLen() * (xElems-1) *pD['k']*pD['G']
            x_forCalc= (reg.maps['x'][:-1] + reg.maps['x'][1:])/2
            
            subPlts2.plot( reg.maps['x'][:], reg.maps['t'][:, 0], "g"+ lineStyle, label=_isFirst("bot simulated "+label))
            subPlts2.plot( reg.maps['x'][:], reg.maps['t'][:,-1], "y"+ lineStyle, label=_isFirst("top simulated "+label))
            
            subPlts2.plot( x_forCalc, t_calc_B, "b"+ lineStyle, label=_isFirst("bot post-calc "+label))
            subPlts2.plot( x_forCalc, t_calc_T, "r"+ lineStyle, label=_isFirst("top post-calc "+label))


            # extra plot: change in shear - post-calculated (from height) AND simulated
            dtdx_calc_B = (reg.maps['t'][1:, 0] - reg.maps['t'][:-1, 0] ) /reg.xLen() * (xElems-1)
            dtdx_calc_T = (reg.maps['t'][1:,-1] - reg.maps['t'][:-1,-1] ) /reg.xLen() * (xElems-1)  
            
            subPlts3.plot( reg.maps['x'], reg.maps['dtdx'][:,0], "g"+ lineStyle, label=_isFirst("bot simulated "+label))
            subPlts3.plot( reg.maps['x'], reg.maps['dtdx'][:,1], "y"+ lineStyle, label=_isFirst("top simulated "+label))
            
            subPlts3.plot( x_forCalc, dtdx_calc_B, "b"+ lineStyle, label=_isFirst("bot post-calc "+label))
            subPlts3.plot( x_forCalc, dtdx_calc_T, "r"+ lineStyle, label=_isFirst("top post-calc "+label))

        except:
            pass

    #TODO: changelater
    #global lineStyle, label
    lineStyle = "--"
    label     = "corrected"
    subPlts2.legend()
    subPlts3.legend()

    return True

#==============================================================================

def plot_fromHomList(allRegs, Del_h0, whatToPlot, mode="default", winTitle=None):
    "Plots results from homogenized maps-set"

    x_list   = allRegs.xBdy_list()
    pD       = allRegs.pD

    fig, subPlts = get_plot2x2(pD, x_list, Del_h0, winTitle)
    
    if whatToPlot == "homogenized":
        maps = allRegs.maps_hom
    elif whatToPlot == "smoothed":
        maps = allRegs.maps_smoothed
    else:
        print("Error [in AM2_plot/plot_fromHomList]: unknown whatToPlot %s" %whatToPlot)
        return False
    
    
    x = maps['x']
    h = maps['h']
    u = maps['u']
    s = maps['s']
    t = maps['t'] 
    
    xElems, yElems = h.shape
    if mode == "default" and yElems > 11:
        interval = int(yElems/10)
    elif mode == "coupledSim":
        interval = 1
    else:
        print("ERROR [in AM2_plot/plot_fromHomList]: unknown mode %s" %mode)
        return False
    
    subPlts[0].plot(x,h[:,::interval])
    subPlts[1].plot(x,u[:,::interval])
    subPlts[2].plot(x,s[:,::interval])
    subPlts[3].plot(x,t[:,::interval])
        
    return True