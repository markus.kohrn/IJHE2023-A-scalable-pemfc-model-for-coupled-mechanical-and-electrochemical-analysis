# -*- coding: utf-8 -*-
"""
Created on Fri Oct 29 14:24:21 2021

@author: kohrn_simu
"""

from __future__ import division, absolute_import, \
                       print_function#, unicode_literals

# %matplotlib qt
                       

''' import standard libraries'''                   
import math, os, time
import numpy             as np
import matplotlib.pyplot as plt
import matplotlib        as mpl
import copy
from   scipy                 import optimize
from   scipy.interpolate     import interp1d
from   scipy.ndimage.filters import gaussian_filter1d

''' imoprt own function classes '''

# swith cwd to parent directory, if not yet done
if os.path.split(os.getcwd())[1] == "AnalytModel":
    os.chdir("..")

#import AnalytModel.AM2_calcSingleReg   as c_calcReg
import AnalytModel.AM2_mapRegimes      as c_mapRegs
import AnalytModel.AM2_postprocessing  as c_post
import AnalytModel.AM2_plot            as c_plot
import AnalytModel.AM2_physics         as c_phys
import AnalytModel.class_allRegimes    as c_allRegimes

''' import own object classes '''



'''
===============================================================================
                    GLOBALS
===============================================================================
'''

# RWTH color list
color_list12 = ["#00549F", "#E30066", "#FFED00", "#006165", "#0098A1",
                "#57AB27", "#BDCD00", "#F6A800", "#CC071E", "#A11035", 
                "#612158", "#7A6FAC"]
color_list11 = ["#00549F", "#E30066", "#FFED00", "#006165", "#0098A1",
                "#57AB27", "#BDCD00", "#F6A800", "#CC071E", "#A11035", 
                "#612158"]
color_list6  = ["#00549F", "#E30066", "#FFED00", "#006165", "#0098A1",
                "#57AB27"]
                
mpl.rcParams['axes.prop_cycle'] = mpl.cycler(color=color_list11) 

DEBUG_YN = False

'''
===============================================================================
                    main function
===============================================================================
'''
def main(input_paraDict = None):
#============= Parameter-Set =================================================    
    
    if input_paraDict == None:
        paraDict_base = {'X_ribB' :300, 'X_ribT' :500,
                         'X_chB'  :200, 'X_chT'  :250,   
                         'R_ribB' :100, 'R_ribT' :100, 
                         'H_mea'  :400,
                         'Ey'     :8.79,    'G'  :8.55,
                         'k'      :0.8333,
                         
                         'n_func'     : None,
                         'xNodes_reg' :   50,
                         'xNodes_hom' : 1000,
                         'yNodes'     :  101,
                         
                         }    # 1000 S/m = 0.001 S/µm  
    else:
        paraDict_base = input_paraDict

#============= Stresses =======================================================

    Del_h0_list     = [80]#40,80,130]#[40,60,80,100,120,140,160]

#============= Variations =====================================================
    
    R_rib_list      = [100]#10,75,150]#[10, 50, 100, 150]#200, 225] # [10,25,30,50,75,100,125,150,175,200,225]
    paraDict_list   = list()

    for R_rib in R_rib_list:
        paraDict_new = copy.deepcopy(paraDict_base)
        paraDict_new.update({'R_ribT' :R_rib, 'R_ribB' :R_rib,})
        paraDict_list.append(paraDict_new)
        

#============= DoPlot =========================================================

    doPlot_mapReg = False
    doPlot_orig   = True
    doPlot_corr   = True
    doPlot_extraPl= False
    doPlot_hom    = True
    doPlot_smooth = True 
    
    noKPI = True

#============= Result Arrays ==================================================

    Del_h0_arr      = np.zeros([len(Del_h0_list), len(paraDict_list)])
    p_clamp_arr     = np.zeros([len(Del_h0_list), len(paraDict_list)])
    R_areal_MEA_arr = np.zeros([len(Del_h0_list), len(paraDict_list)])
    s_max_arr       = np.zeros([len(Del_h0_list), len(paraDict_list)])
    t_max_arr       = np.zeros([len(Del_h0_list), len(paraDict_list)])
    
    failedSet_str   = ""

#============= Calculation Loop ===============================================

    t_start = time.time()
 
    for paraDict, i_pD in zip(paraDict_list, range(len(paraDict_list))):
        
        for Del_h0, i_Delh0 in zip(Del_h0_list, range(len(Del_h0_list))):
            print("||| Del_h0 = %d, ||| R_rib = %d |||" %(Del_h0, paraDict['R_ribB']))
            
            # Map initial regimes
            regimes = c_mapRegs.main(paraDict, doPlot = doPlot_mapReg)
            allRegs = c_allRegimes.AllRegimes(regimes, paraDict)  
            allRegs.print_regimes()
                      
            #try:       
            # perform calculation loop
            calculationLoop(allRegs=allRegs, Del_h0=Del_h0, plotRes = doPlot_orig)
            
            # perform correction loop
            correctionLoop(allRegs=allRegs, Del_h0=Del_h0, plotRes = doPlot_corr, extraPlots = doPlot_extraPl)
                
            try:
                pass
            
            except Exception as e:
                print(e)
                failedSet_str += "Del_h0 = %d, R_rib = %d (Error in Calc. or. Corr.Loop)\n" %(Del_h0, paraDict['R_ribB'])
            
            else:
                
                # homogenize results
                homogenization(allRegs=allRegs, Del_h0=Del_h0, plotRes = doPlot_hom)
                
                # smooth results
                smoothing(allRegs=allRegs, Del_h0=Del_h0, smoothMethod_list=["gaus"],       plotRes = doPlot_smooth)
                smoothing(allRegs=allRegs, Del_h0=Del_h0, smoothMethod_list=["gaus+yBal"],  plotRes = doPlot_smooth)
                smoothing(allRegs=allRegs, Del_h0=Del_h0, smoothMethod_list=["physics"],    plotRes = doPlot_smooth, 
                          mode="default", trueStiff=False, plotAdditional = False)
    
    return True



'''
===============================================================================
                        Calculation Loop
===============================================================================
'''
def calculationLoop(allRegs=None, Del_h0=None, plotRes = True):
    
    # extract from allRes
    nrOfRegs  = allRegs.nrOfRegs()

#==============================================================================
    
    print("=================================================\n")   
    print(">>>>>>>>>>>>>>>>> Calculation Loop <<<<<<<<<<<<<<<")    
    print("\n=================================================")    
    
    loopCount = 0
    while (not allRegs.allFinal()) and (loopCount < 20):
        loopCount += 1
        print(frame("Loop: %2d" %(loopCount)))
              
        for iReg in range(nrOfRegs):
            
            reg = allRegs.reg(iReg)
            
            # regime already calculated
            if reg.isFinal():                       
                continue
            
            # regime is not jet calculated
            print("%2d - %s" %(iReg, reg.BT()))
            if reg.isSolveable():
                isSolved, infoDict = reg.trySolve(Del_h0)
                
                # is a jump needed?
                if not isSolved and type(infoDict) == dict:
                    print("== JUMP NEEDED ==")
                    allRegs.jumpBounds(infoDict["jumpBdy"], infoDict["statBdy"])
                    break # exits for-loop -> directly next while-iteration
                    
    # plot results
    if plotRes:
        c_plot.plot_fromPlotList(allRegs, Del_h0, winTitle="Fig. 1 - Initial Plot")    
            
    return True


'''
===============================================================================
                        Correction Loop
===============================================================================
'''

def correctionLoop(allRegs=None, Del_h0=None, plotRes = True, extraPlots=False):

    print("\n=================================================")
    print(">>>>>>>>>>>>>>>>> Correction Loop <<<<<<<<<<<<<<<")
    print("=================================================\n")
    
    # Create correction dict list
    gap_list = allRegs.create_gapList()
          
    # loop over all gaps
    gapCount = 0
    for gap in gap_list: # Loop over all Gaps
        gapCount += 1
        
        # if gapCount == 2:
        #     continue
        
        print(frame("Air-Gap: %2d" %(gapCount)))
        
        # re-calculate all regimes in selected gap
        gap.recalcGapRegs(Del_h0)

        
    # plot corrected data
    if plotRes:
        c_plot.plot_fromPlotList(allRegs, Del_h0, winTitle="Fig. 2 - Corrected", extraPlots=extraPlots)       
    
    return True


'''
===============================================================================
                    Homogenization
===============================================================================
'''

def homogenization(allRegs=None, Del_h0=None ,mode="default", plotRes = True):
                  
    print("\n=================================================")
    print(">>>>>>>>>>>>>>>>> Homogenization <<<<<<<<<<<<<<<<")
    print("=================================================\n")
    
# ===== Homogenize ======   
    
    allRegs.homogenize(nrOfPoints=allRegs.pD['xNodes_hom'], mode=mode)   
    
    # plot results
    if plotRes:
        c_plot.plot_fromHomList(allRegs, Del_h0, whatToPlot="homogenized", 
                                mode=mode, winTitle="Fig. 3 - Homogenized")
    
    print("\nHomogenization completed!\n")        
    return True          

'''
===============================================================================
                    Smoothing
===============================================================================
'''

def smoothing(allRegs=None, Del_h0=None, smoothMethod_list=[None], mode="default", trueStiff=False,
              plotRes = True, plotAdditional = False):
                  
    print("\n=================================================")
    print(">>>>>>>>>>>>>>>>>>>> Smoothing <<<<<<<<<<<<<<<<<<")
    print("=================================================\n")

# ===== Smooth ==========
    
    for smMethod in smoothMethod_list:
        
        # sigma relative to GLD_height.
    
        if smMethod == "gaus": 
            # Gaussian : NO y-balancing          
            print(frame("Smooth: Gaussian"))
            winTitle="Fig. 4 - Smoothed (no y-balancing)"
            sigmaRel_Hgdl = 0.025 / allRegs.pD['xNodes_hom']

        elif smMethod == "gaus+yBal":    
            #Gaussian : WITH y-balancing 
            print(frame("Smooth: Gaussian + y-balancing"))   
            winTitle="Fig. 5 - Smoothed (y-balancing)"
            #sigmaRel_Hgdl = 0.03 / glob_nrOfPointsHom
            sigmaRel_Hgdl = 0.025 / allRegs.pD['xNodes_hom']
            #TODO: i changed the sigma
            
        elif smMethod == "gaus+yBal+xBdys":            
            #Gaussian : WITH y-balancing & x-Boundaries
            print(frame("Smooth: Gaussian + y-balancing + x-bounds"))
            winTitle="Fig. 6 - Smoothed (y-balancing AND x-boundaries)"
            sigmaRel_Hgdl = 0.025 / allRegs.pD['xNodes_hom']
            
        elif smMethod == "physics":
            print(frame("Smooth: physics-based: Force equilibrium"))
            winTitle="Fig. 6 - Smoothed (physics-based: Force Eq.)"
            sigmaRel_Hgdl=None
                   
        else:
            print("ERROR: Unknown smooting method")
            return False
        
        # calculating absolute sigma
        try:    
            sigma = int(round(sigmaRel_Hgdl * allRegs.pD['H_mea'] * allRegs.pD['xNodes_hom'],0))
            print("sigma=%d" %sigma)
        except: 
            sigma = None


        # Performing Smooting        
        allRegs.smooth(Del_h0, smMethod=smMethod, sigma=sigma, trueStiff=trueStiff,
                       plotRes=plotRes, plotAdditional=plotAdditional)
        
        # Plotting
        if plotRes:
            c_plot.plot_fromHomList(allRegs, Del_h0, whatToPlot="smoothed",  mode=mode, winTitle=winTitle)

    return True


'''
===============================================================================
                    DEBUG Routine
===============================================================================
'''
def DEBUG(string):
    if DEBUG_YN:
        print(string)
    return True

def frame(string):
    mid_line = "# " + string + " #"
    top_line = "#" + "="*(len(string)+2) + "#"
    bot_line = top_line
    return top_line + "\n" + mid_line + "\n" + bot_line

'''
===============================================================================
                    Start-up Routine
===============================================================================
'''
if __name__ == "__main__":
    main()