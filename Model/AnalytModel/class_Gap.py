# -*- coding: utf-8 -*-
"""
Created on Thu Mar 24 19:01:45 2022

@author: kohrn_simu
"""

from __future__ import division, absolute_import, \
                       print_function#, unicode_literals

# %matplotlib qt

''' import standard libraries'''       
import sys, os                  
import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import copy
import time
from scipy                 import optimize
from scipy.interpolate     import interp1d
from scipy.ndimage.filters import gaussian_filter1d
from contextlib            import contextmanager

''' import own function classes '''

# swith cwd to parent directory, if not yet done
if os.path.split(os.getcwd())[1] == "AnalytModel":
    os.chdir("..")
    
import AnalytModel.AM2_calcSingleReg  as c_calcReg
import AnalytModel.AM2_mapRegimes     as c_mapRegs
import AnalytModel.AM2_postprocessing as c_post
#import AnalytModel.AM2_main           as AM2_main

''' import own object classes''' 
import AnalytModel.class_Boundary              as c_boundary
import AnalytModel.class_Regime                as c_regime


'''
===============================================================================
                    Class: all regiems
===============================================================================
'''



class Gap:

#============= Constructor ===    
    def __init__(self, allRegs, bdy_gap):
        
        self.allRegs = allRegs
        self.bdy_gap = bdy_gap
        
        iReg = bdy_gap.idx - 1
        
        # Find characteristic indices
        self.reg_Bmin = self.allRegs.reg_list[self.__find_idx(iReg, "Bot", "min") ]
        self.reg_Bmax = self.allRegs.reg_list[self.__find_idx(iReg, "Bot", "max") ]
        self.reg_Tmin = self.allRegs.reg_list[self.__find_idx(iReg, "Top", "min") ]
        self.reg_Tmax = self.allRegs.reg_list[self.__find_idx(iReg, "Top", "max") ]
        self.reg_min  = self.allRegs.reg_list[min(self.reg_Bmin.idx, self.reg_Tmin.idx)]
        self.reg_max  = self.allRegs.reg_list[max(self.reg_Bmax.idx, self.reg_Tmax.idx)]
        
        self.hB_gap = self.__calc_h_gap(flag_BT = "B")
        self.hT_gap = self.__calc_h_gap(flag_BT = "T")
        self.tB_gap = self.__calc_t_gap(flag_BT = "B")
        self.tT_gap = self.__calc_t_gap(flag_BT = "T")
        
        self.regsInGap_list = self.allRegs.reg_list[self.reg_min.idx : self.reg_max.idx +1 ]
        
        self.f_C_h = None
        self.f_C_t = None
        
        # printout
        print(":Bot %2d - %2d | Top: %2d - %2d => t_Bgap =%7.2f, t_Tgap =%7.2f @bdy-idx =%3d" 
              %(self.reg_Bmin.idx, self.reg_Bmax.idx, 
                self.reg_Tmin.idx, self.reg_Tmax.idx, 
                self.tB_gap,  self.tT_gap,
                self.bdy_gap.idx) )
        
        # set all gap regimes on not-final
        self.set_allGapRegs_notFinal()
        
        # link the gap to all gap regimes
        self.link_gapToAllGapRegs()
            
            
    
#============= h/t gap functions ==============================================       
        
    def __calc_h_gap(self, flag_BT = "BT"):
        return self.__calc_x_gap(flag_BT, "h")

#=============

    def __calc_t_gap(self, flag_BT = "BT"):
        return self.__calc_x_gap(flag_BT, "t")

#=============

    def __calc_x_gap(self, flag_BT, flag_ht):

        x_B1   = self.bdy_gap.lReg.maps[flag_ht][-1, 0]
        x_B2   = self.bdy_gap.rReg.maps[flag_ht][ 0, 0]
        
        x_T1   = self.bdy_gap.lReg.maps[flag_ht][-1,-1]
        x_T2   = self.bdy_gap.rReg.maps[flag_ht][ 0,-1]
            
        if   flag_BT == "B" or flag_BT == "Bot":
            return x_B2 - x_B1
        
        elif flag_BT == "T" or flag_BT == "Top":
            return x_T2 - x_T1
        
        elif flag_BT == "BT" or flag_BT == "BotTop":
            return (x_B2 + x_T2)/2 - (x_B1 + x_T1)/2
        
        else: # flag error
            return False

#=============
            
    def update_h_gap(self):
        self.hB_gap = self.__calc_h_gap(flag_BT = "B")
        self.hT_gap = self.__calc_h_gap(flag_BT = "T") 

#=============
        
    def update_t_gap(self):
        self.tB_gap = self.__calc_t_gap(flag_BT = "B")
        self.tT_gap = self.__calc_t_gap(flag_BT = "T")
        
#============= find characteristic indices ====================================

    def __find_idx(self, idx, BotTop, MinMax):
        " helper function for determining characteristic gap indices, iterative"
        
        # gather base regs of Bottom and Top side
        baseReg_B = self.allRegs.reg(idx).B().replace("-","")
        baseReg_T = self.allRegs.reg(idx).T().replace("-","")

        # Case selection: Bot/Top, Min/Max -> return statement
        if BotTop == "Bot":
            if MinMax == "min": 
                if baseReg_B == "O": return self.__find_idx(idx-1, BotTop, MinMax)
                else:                return idx
            elif MinMax == "max":
                if baseReg_B == "O": return self.__find_idx(idx+1, BotTop, MinMax)
                else:                return idx
        elif BotTop == "Top": 
            if MinMax == "min":
                if baseReg_T == "O": return self.__find_idx(idx-1, BotTop, MinMax)
                else:                return idx
            elif MinMax == "max":
                if baseReg_T == "O": return self.__find_idx(idx+1, BotTop, MinMax)
                else:                return idx

#'''
#===============================================================================
#                    Set all gap regimes on not-final
#===============================================================================
#'''
    def set_allGapRegs_notFinal(self):
        "all gap regimes are set to not final"
        for idx in range(self.reg_min.idx, self.reg_max.idx +1):
            self.allRegs.reg(idx).final = False        
        return True

#'''
#===============================================================================
#                    link the gap to all gap regimes
#===============================================================================
#'''

    def link_gapToAllGapRegs(self):
        "the gap is linked to all gap regimes"
        for idx in range(self.reg_min.idx, self.reg_max.idx +1):
            self.allRegs.reg(idx).gap = self        
        return True

#'''
#===============================================================================
#             gap correction needed for Bot/Top at given index
#===============================================================================
#'''

    def needGapCorrection(self, regIdx, flag_BT):
        "True if regime with index needs gap correciton on Bot/Top, else False"
        if   flag_BT == "Bot" or flag_BT == "B":
            return (regIdx > self.reg_Bmin.idx) and (regIdx < self.reg_Bmax.idx)

        elif flag_BT == "Top" or flag_BT == "T":
            return (regIdx > self.reg_Tmin.idx) and (regIdx < self.reg_Tmax.idx)
        
        else:
            print("Error: invalid flag_BT in Gap/needGapCorrection")
        
#'''
#===============================================================================
#                    Correction Loop over one gap
#===============================================================================
#'''
            
    def recalcGapRegs(self, Del_h0):
        
        # initial values of gap fitting parameters           
        f_C_h = 0.0
        f_C_t = 0.0

        th_gapClosure = self.allRegs.paraObj.gc_threshold_relChange
        
        for loop in range(100):
            
            # copy current factors
            f_C_h_pre = f_C_h
            f_C_t_pre = f_C_t
            
            # calculating f_C_h
            with self.suppress_stdout():
                nullstelle = optimize.fsolve(self.__doRecalcGapRegs, f_C_h, args=("f_C_h_fitt", f_C_t, Del_h0))       
            f_C_h += (nullstelle[0] - f_C_h) *0.5     
            
            # calculating f_C_t
            with self.suppress_stdout():    
                nullstelle = optimize.fsolve(self.__doRecalcGapRegs, f_C_t, args=("f_C_t_fitt", f_C_h, Del_h0))       
            f_C_t += (nullstelle[0] - f_C_t) *0.5
            
            
            # check for early exit
            relChange_h = abs( (abs(f_C_h) + th_gapClosure*10**-5) / (abs(f_C_h_pre) + th_gapClosure*10**-5) -1 )
            relChange_t = abs( (abs(f_C_t) + th_gapClosure*10**-5) / (abs(f_C_t_pre) + th_gapClosure*10**-5) -1 )
            print("loop:%2d, f_C_h=%7.4f, f_C_t=%7.4f, rel.change[h/t]= %f / %f"
                  %(loop+1, f_C_h, f_C_t, relChange_h, relChange_t ) )
            if max(relChange_h, relChange_t) < th_gapClosure:
                    break

        # performe a full-size recalculation (all datapoints)
        with self.suppress_stdout():
            self.__doRecalcGapRegs([f_C_h, f_C_t], "both", None, Del_h0, noBdyAdjust=False)          
            
        return True

#===============================================================================

        
    def __doRecalcGapRegs(self, optiFactor, optiMode, secondFactor, Del_h0, noBdyAdjust=False):
        
        if   optiMode == "f_C_h_fitt":
            self.f_C_h = optiFactor
            self.f_C_t = secondFactor
            modStr = "calcReduced"
        elif optiMode == "f_C_t_fitt":
            self.f_C_h = secondFactor
            self.f_C_t = optiFactor
            modStr = "calcReduced"
        elif optiMode == "both":
            self.f_C_h = optiFactor[0]
            self.f_C_t = optiFactor[1]
            modStr = None
            
        
        idx_list_fromLeft  = np.arange(self.reg_min.idx, self.bdy_gap.lReg.idx +1,  1)
        idx_list_fromRight = np.arange(self.reg_max.idx, self.bdy_gap.rReg.idx -1, -1)
        
        for reg in self.regsInGap_list:
            reg.final = False

#==============================================================================
#       Correction of all Regimes left of gap except last regime
        
        # loop over left side of gap -> calculation from left
        print("===>| foreward loop")
        for idx in idx_list_fromLeft:
            print("===== REGIME: %d ========="%(idx))
                
            reg   = self.allRegs.reg(idx)         
             
            # jump over last element in gap
            if idx == max(idx_list_fromLeft):
                continue
                
            # try to solve the regime         
            isSolved, infoDict = reg.trySolve(Del_h0, modStr         = modStr, 
                                                      noBdyAdjust    = noBdyAdjust, 
                                                      modStr_gapCorr = "forward")
            
            # is a jump needed?
            if not isSolved and type(infoDict) == dict:
                print("== JUMP NEEDED ==")
                #self.jumpBounds(infoDict["jumpBdy"], infoDict["statBdy"])
                #sys.exit     
         
#==============================================================================
#       Correction of all regimes right of gap
                 
        # loop over right side of gap -> calculation from right
        print("|<=== backward loop")
        for idx in idx_list_fromRight:
            print("===== REGIME: %d ========="%(idx))
            
            reg   = self.allRegs.reg(idx)      
            
            # store correction dict of first element after gap
            if idx == min(idx_list_fromRight):
                continue
                
            # try to solve the regime
            isSolved, infoDict = reg.trySolve(Del_h0, modStr         = modStr, 
                                                      noBdyAdjust    = noBdyAdjust, 
                                                      modStr_gapCorr = "backward")
                
            # is a jump needed?
            if not isSolved and type(infoDict) == dict:
                print("== JUMP NEEDED ==")
                #self.jumpBounds(infoDict["jumpBdy"], infoDict["statBdy"])
                #sys.exit            
            
#==============================================================================
#       Correction of last regimes: left and right of gap 

        # plot the last regime: left of gap
        print("x|x last regime, left and right of gap")
        
        # try to solve last regime left of gap
        print("===== REGIME: %d ========="%(self.bdy_gap.lReg.idx))
        reg = self.bdy_gap.lReg
        reg.trySolve(Del_h0, modStr         = modStr, 
                             noBdyAdjust    = noBdyAdjust, 
                             modStr_gapCorr = "forward")
        
        # try to solve last regime right of gap
        print("===== REGIME: %d ========="%(self.bdy_gap.rReg.idx))
        reg = self.bdy_gap.rReg
        reg.trySolve(Del_h0, modStr         = modStr, 
                             noBdyAdjust    = noBdyAdjust, 
                             modStr_gapCorr = "backward")
            
        # set flag to final
        #self.final = True

            
        # return Statement
        DEL_hB = self.__calc_h_gap(flag_BT = "B") 
        DEL_hT = self.__calc_h_gap(flag_BT = "T")            
              
        DEL_tB = self.__calc_t_gap(flag_BT = "B") 
        DEL_tT = self.__calc_t_gap(flag_BT = "T") 
        
        
        if   optiMode == "f_C_h_fitt":                          
            return abs(DEL_hB) + abs(DEL_hT)                   
        elif optiMode == "f_C_t_fitt":                            
            return abs(DEL_tB) + abs(DEL_tT) 
        elif optiMode == "both":            
            return abs(DEL_hB) + abs(DEL_hT) + abs(DEL_tB) + abs(DEL_tT)            
        else:
            return False
                
#==============================================================================
#                        x / xLen funktions
#==============================================================================  
            
    def x0(self, reg, flag_BT):
        
        # initial value of x0
        x0 = 0        
        
        if reg.idx < self.bdy_gap.idx:
            "left side of gap -> not inverted"
            
            if   flag_BT == "Bot" or flag_BT == "B":
                iReg_min = self.reg_Bmin.idx
            elif flag_BT == "Top" or flag_BT == "T":
                iReg_min = self.reg_Tmin.idx
            elif flag_BT == "BotTop" or flag_BT == "BT":
                iReg_min = self.reg_min.idx
            else:
                print("Error: invalid flag_BT in Gap/x0")
                sys.exit
                
            # loop over all regimes [first free reg -> left-next to current reg]
            for idx in range(iReg_min+1, reg.idx):
                x0 += self.allRegs.reg(idx).xLen()

        else: # reg.idx >= self.bdy_gap.idx:
            "right side of gap -> inverted"
            
            if   flag_BT == "Bot" or flag_BT == "B":
                iReg_max = self.reg_Bmax.idx
            elif flag_BT == "Top" or flag_BT == "T":
                iReg_max = self.reg_Tmax.idx
            elif flag_BT == "BotTop" or flag_BT == "BT":
                iReg_max = self.reg_max.idx
            else:
                print("Error: invalid flag_BT in Gap/x0")
                sys.exit
                
            # loop over all regimes [right-next to current reg -> first free reg]          
            for idx in range(reg.idx+1, iReg_max):
                x0 += self.allRegs.reg(idx).xLen()
            
        #return statement
        return x0
            
#==============================================================================            
        
    def xLen(self, flag_BT = "BT"):
        
        if flag_BT == "B" or flag_BT == "Bot":
            return self.reg_Bmax.xStart()  - self.reg_Bmin.xEnd()
        
        elif flag_BT == "T" or flag_BT == "Top":
            return self.reg_Tmax.xStart() - self.reg_Tmin.xEnd()
        
        elif flag_BT == "BT" or flag_BT == "BotTop":
            return self.reg_max.xStart() - self.reg_min.xEnd()
        
        else: # flag error
            return False

#==============================================================================
    
    def xLen_side(self, reg, flag_BT):
        
        if reg.idx < self.bdy_gap.idx:
            "left side of gap -> not inverted"
            
            if flag_BT == "Bot" or flag_BT == "B":
                return self.bdy_gap.lReg.xEnd() - self.reg_Bmin.xEnd()
            
            elif flag_BT == "Top" or flag_BT == "T":
                return self.bdy_gap.lReg.xEnd() - self.reg_Tmin.xEnd()
            
            elif flag_BT == "BotTop" or flag_BT == "BT":
                return self.bdy_gap.lReg.xEnd() - self.reg_min.xEnd()

        else: # reg.idx >= self.bdy_gap.idx:
            "right side of gap -> inverted"
            
            if flag_BT == "Bot" or flag_BT == "B":
                return self.reg_Bmax.xStart() - self.bdy_gap.rReg.xStart()
            
            elif flag_BT == "Top" or flag_BT == "T":
                return self.reg_Tmax.xStart() - self.bdy_gap.rReg.xStart()
            
            elif flag_BT == "BotTop" or flag_BT == "BT":
                return self.reg_max.xStart()  - self.bdy_gap.rReg.xStart()

#==============================================================================
    
    def xLen_side_OO(self, reg, flag_BT):
        
        xLen_side_OO = 0
        
        if reg.idx < self.bdy_gap.idx:
            "left side of gap -> not inverted"
            
            for idx in range(self.reg_min.idx, self.bdy_gap.lReg.idx +1):
                if self.allRegs.reg(idx).BT().replace('-','') == "OO":
                    xLen_side_OO += self.allRegs.reg(idx).xLen()

        else: # reg.idx >= self.bdy_gap.idx:
            "right side of gap -> inverted"
            
            for idx in range(self.bdy_gap.rReg.idx, self.reg_max.idx +1):
                if self.allRegs.reg(idx).BT().replace('-','') == "OO":
                    xLen_side_OO += self.allRegs.reg(idx).xLen()
        
        return xLen_side_OO

#==============================================================================
    
    def xLen_side_notOO(self, reg, flag_BT):
        
        xLen_side    = self.xLen_side(   reg, flag_BT)
        xLen_side_OO = self.xLen_side_OO(reg, flag_BT)
        
        return xLen_side - xLen_side_OO
                                            
#==============================================================================
#                        helper functions
#==============================================================================  
    
    @contextmanager
    def suppress_stdout(self):
        with open(os.devnull, "w") as devnull:
            old_stdout = sys.stdout
            sys.stdout = devnull
            try:  
                yield
            finally:
                sys.stdout = old_stdout