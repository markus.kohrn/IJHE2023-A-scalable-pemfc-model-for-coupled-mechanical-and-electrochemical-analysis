# -*- coding: utf-8 -*-
"""
Created on Thu Dec  9 17:02:47 2021

@author: kohrn
"""

from __future__ import division, absolute_import, \
                       print_function#, unicode_literals

# %matplotlib qt

''' import standard libraries'''                       
import math
import numpy             as np
import matplotlib.pyplot as plt
import copy
import time


'''
===============================================================================
         Tester
===============================================================================
'''

def test_fkt():
    "provides test data for main funktion"
     
    # create paraDict
    paraDict = {'X_ribB' :  600,
                'X_ribT' : 1000,
                
                'X_chB'  :  400,
                'X_chT'  :  500,
        
                'R_ribB' :  150,
                'R_ribT' :  150,
                
                'H_mea'  :  400,
               }
     
    # start main-fkt with paraDict    
    return main(paraDict, doPlot=True)
   

'''
==============================================================================
        Main Method
==============================================================================
'''

def main(paraDict, doPlot=True):
    
    # extract data from paraDict
    X_ribB = paraDict['X_ribB'] *2  
    X_ribT = paraDict['X_ribT'] *2 
    X_chB  = paraDict['X_chB']  *2     
    X_chT  = paraDict['X_chT']  *2 
    R_ribB = paraDict['R_ribB']        
    R_ribT = paraDict['R_ribT']
    H_mea  = paraDict['H_mea'] 
    H_chT  = max(250,  R_ribT+100,  R_ribB+100)
    H_chB  = max(250,  R_ribT+100,  R_ribB+100)
    
    if X_ribB == X_ribT and X_chB == X_chT and R_ribB == R_ribT:
        xSym = True
    else:
        xSym = False
        
    # calc maximum x of recurring element
    x_max = kgV(X_ribB+X_chB, X_ribT+X_chT)
    
    # calc BPP values
    x_BPP  = np.linspace(0,x_max,10000)
    yB_BPP = BPP(x_BPP, X_ribB, X_chB, H_chB, R_ribB, H_mea, "Bot")
    yT_BPP = BPP(x_BPP, X_ribT, X_chT, H_chT, R_ribT, H_mea, "Top")
    
    # calc top and bottom regime changes
    rB = regimeChange(x_max, X_ribB, X_chB, H_chB, R_ribB, H_mea, "Bot")
    rT = regimeChange(x_max, X_ribT, X_chT, H_chT, R_ribT, H_mea, "Top")
    
    # combine top and bottom results to regimes
    regimes = combineRegimeArrays(rB, rT, xSym)
    print_regimes(regimes)
    
    plot_regimes(regimes, x_BPP, yT_BPP, yB_BPP, rT, rB, H_mea, H_chB, H_chT)
    
    # correct regimes
    regimes = correct_regimes(regimes, paraDict)
    print_regimes(regimes)
    
    # Plot regimes
    if doPlot:
        plot_regimes(regimes, x_BPP, yT_BPP, yB_BPP, rT, rB, H_mea, H_chB, H_chT)    
    
    # return statement
    return regimes
 
  

'''
==============================================================================
        Correct regimes
==============================================================================
'''

def correct_regimes(regimes, pD):
    

# ========== (1) Delete irrelevant boundaries =================================
    unfinished = True
    while unfinished:        
        
        for idx in range(1, len(regimes)-2):
             
            regL = regimes[idx-1]
            reg  = regimes[idx  ]
            regR = regimes[idx+1]
            
            #========== Solve bad R/-R transistion ======
            if   ((regL[1] == "R" and reg[1] ==  "R" and regR[1] == "-R") and 
                  (regL[2] == "R" and reg[2] == "-R" and regR[2] == "-R")):
                # delete
                regimes = np.delete(regimes, ( idx), axis=0)
                break
            
            elif ((regL[1] == "R" and reg[1] == "-R" and regR[1] == "-R") and 
                  (regL[2] == "R" and reg[2] ==  "R" and regR[2] == "-R")):
                # delete
                regimes = np.delete(regimes, ( idx), axis=0)
                break
            
            #========== Solve bad O/-O transistion ======            
            if (reg[1] == "O" and reg[2] == "-O") or (reg[1] == "-O" and reg[2] == "O"):
                
                if (regL[1] == regL[2]) and (regR[1] == regR[2]): # Left T=B AND Right T=B -> delete center
                    regimes = np.delete(regimes, ( idx), axis=0)
                    break                    
                            
                elif regL[1] == regL[2]:             # Left T=B -> change right
                    idx_change_LR = +1 
                    if regR[1] == reg[1]:               # Bot: C=R  -> change bot (R)
                        iBT = 1     
                    elif regR[2] == reg[2]:             # Top: C=R  -> change top (R)
                        iBT = 2
                
                elif regR[1] == regR[2]:           # Right T=B -> change left
                    idx_change_LR = -1         
                    if regL[1] == reg[1]:               # Bot: C=R  -> change bot (R)
                        iBT = 1
                    elif regL[2] == reg[2]:             # Top: C=R  -> change top (R)
                        iBT = 2                
                
                idx2 = idx
                if iBT == 1: iBT_anti = 2
                else:        iBT_anti = 1
                
                while idx2 < len(regimes) and regimes[idx2,iBT] != reg[iBT_anti]:
                    regimes[idx2,iBT] = reg[iBT_anti]
                    idx2 = idx2 + idx_change_LR
                break
                   
            #========== Delete double ====== 
            if reg[1] == regR[1] and reg[2] == regR[2]:
                if   "chaCenter" in reg[3]:
                    regimes = np.delete(regimes, ( idx), axis=0)
                elif "chaCenter" in regR[3]:
                    regimes = np.delete(regimes, ( idx+1), axis=0)
                else:
                    regimes = np.delete(regimes, ( idx), axis=0)
                    
                    
                    # ToDo: continue here!!!
                
                break
            
        #========== FOR-ELSE Statement: No further issue ====== 
        else:
            # statement will leave the while-loop
            unfinished = False
    

# ========== (1) Delete irrelevant boundaries =================================


    # unfinished = True
    # while unfinished:        
        
    #     for idx in range(1, len(regimes)-2):
             
    #         regL = regimes[idx-1]
    #         reg  = regimes[idx  ]
    #         regR = regimes[idx+1]
                   
    #         #========== Delete double ====== 
    #         if reg[1] == regR[1] and reg[2] == regR[2]:
    #             if   "chaCenter" in reg[3]:
    #                 regimes = np.delete(regimes, ( idx), axis=0)
    #             elif "chaCenter" in regR[3]:
    #                 regimes = np.delete(regimes, ( idx+1), axis=0)
    #             else:
    #                 regimes = np.delete(regimes, ( idx), axis=0)
                    
                    
    #                 # ToDo: continue here!!!
                
    #             break
            
    #     #========== FOR-ELSE Statement: No further issue ====== 
    #     else:
    #         # statement will leave the while-loop
    #         unfinished = False




# ========== (2) Move "chaCenter"-bdy in center between L and R Bdy ===========
    # 
    for idx in range(1,len(regimes)-1):
        
        regL = regimes[idx-1]
        reg  = regimes[idx  ]
        regR = regimes[idx+1]
        
        if reg[3] == "chaCenter":
            reg[0] = float(regR[0]) + ( float(regL[0]) - float(regR[0]) ) /2



# ========== (3) move rib-regime under rib ====================================
    
    
    # inner class
    def __x_RibEdgeStart(idx, idxBT, idxIncrLR): #====================
        "recursive function to determine x value of next rib edge"   #
        if regimes[idx+idxIncrLR, idxBT] == "F":                     #
            return float(regimes[idx, 0])                            #
        else:                                                        #
            return __x_RibEdgeStart(idx+idxIncrLR, idxBT, idxIncrLR) #
    #=================================================================
    
    # safety-distance from rib edge
    EPS_edgeDist = min(pD["R_ribB"], pD["R_ribT"]) * 0.01
    
    # loop over all regs
    for idx in range(1,len(regimes)-1):
        
        regL = regimes[idx-1]
        reg  = regimes[idx  ]
        regR = regimes[idx+1]
        
        xBdyMax, xBdyMin = None, None
        
        # identify boundaries to adjust
        if   reg[1] == "R"  or (reg[1] == "O" and regL[1] == "R"):
            xBdyMax = __x_RibEdgeStart(idx, 1, -1) + pD["R_ribB"]     # Bot, L <-        
        elif reg[2] == "R"  or (reg[2] == "O" and regL[2] == "R"):
            xBdyMax = __x_RibEdgeStart(idx, 2, -1) + pD["R_ribT"]     # Top, L <-        
        elif reg[1] == "-R":
            xBdyMin = __x_RibEdgeStart(idx, 1, +1) - pD["R_ribB"]     # Bot, -> R        
        elif reg[2] == "-R":
            xBdyMin = __x_RibEdgeStart(idx, 2, +1) - pD["R_ribT"]     # Top, -> R
         
            
        # adjust identified boundaries 
        if xBdyMax != None and float(reg[0]) > xBdyMax:
            print("Bdy. idx=%2d adjusted: x_old=%f , x_new=%f" 
                  %(idx, float(reg[0]), xBdyMax - EPS_edgeDist ))
            reg[0] = xBdyMax - EPS_edgeDist
        if xBdyMin != None and float(reg[0]) < xBdyMin:
            print("Bdy. idx=%2d adjusted: x_old=%f , x_new=%f" 
                  %(idx, float(reg[0]), xBdyMin + EPS_edgeDist ))
            reg[0] = xBdyMin + EPS_edgeDist
   
    
    # return statement
    return regimes
 
    
 
'''
==============================================================================
        Combine Bot and Top boundaries to Regime array
==============================================================================
'''

def combineRegimeArrays(rB, rT, xSym):
    
    iB, iT    = 0, 0   
    absD      = 3
    combiList = list()
    
    # perform combination coop
    while iT < len(rT) or iB < len(rB):
        
        print("iB=%d , iT=%d | %f, %f" %(iB, iT, float(rB[iB,0]), float(rT[iT,0])))
        
        #========== change at bot AND top =========
        if float(rT[iT,0]) == float(rB[iB,0]):
            
            # === 2 static
            if   rT[iT,2] == "stat" and rB[iB,2] == "stat":
                combiList.append([float(rT[iT,0]),      rB[iB,1],   rT[iT,1],   "stat_x2"]) # STATIC
            
            #=== 2 channel center
            elif rT[iT,2] == "chaCenter" and rB[iB,2] == "chaCenter":
                combiList.append([float(rT[iT,0]),      rB[iB,1],   rT[iT,1],   "chaCenter_x2"]) # move
                
            # === Bot static 
            elif rB[iB,2] == "stat":
                combiList.append([float(rT[iT,0])-absD, rB[iB-1,1], rT[iT,1],   "pushed(-)"]) # jump
                combiList.append([float(rT[iT,0]),      rB[iB,  1], rT[iT,1],   "stat"]) # STATIC

            # === Top static
            elif rT[iT,2] == "stat":
                combiList.append([float(rT[iT,0])-absD, rB[iB,1],   rT[iT-1,1], "pushed(-)"]) # jump
                combiList.append([float(rT[iT,0]),      rB[iB,1],   rT[iT,  1], "stat"]) # STATIC
                
            #=== 0 static
            else:
                if not xSym:
                    combiList.append([float(rT[iT,0])-absD/2, rB[iB  ,1], rT[iT-1,1], "pushed(-)"])
                    combiList.append([float(rT[iT,0])+absD/2, rB[iB  ,1], rT[iT  ,1], "pushed(+)"])
                else:
                    combiList.append([float(rT[iT,0])       , rB[iB  ,1], rT[iT  ,1], "move(xSym)"])
                
                # if   abs(rB[iB-1,1]) != "chaCenter":
                #     combiList.append([rT[iT,0]-absD/2, rB[iB  ,1], rT[iT-1,1], "move"])
                #     combiList.append([rT[iT,0]+absD/2, rB[iB  ,1], rT[iT  ,1], "move"])

                # elif abs(rT[iT-1,1]) != "chaCenter": 
                #     combiList.append([rT[iT,0]-absD/2, rB[iB-1,1], rT[iT  ,1], "move"])
                #     combiList.append([rT[iT,0]+absD/2, rB[iB  ,1], rT[iT  ,1], "move"])
                
                # elif abs(rB[iB+1,1]) != "chaCenter":
                #     combiList.append([rT[iT,0]-absD/2, rB[iB-1,1], rT[iT  ,1], "move"])
                #     combiList.append([rT[iT,0]+absD/2, rB[iB  ,1], rT[iT  ,1], "move"])

                # elif abs(rT[iT+1,1]) != "chaCenter": 
                #     combiList.append([rT[iT,0]-absD/2, rB[iB-1,1], rT[iT-1,1], "move"])
                #     combiList.append([rT[iT,0]+absD/2, rB[iB  ,1], rT[iT  ,1], "move"])

            iB += 1                    
            iT += 1
        
        #========== change at bot =========
        elif float(rT[iT,0]) > float(rB[iB,0]):
            combiList.append([float(rB[iB,0]), rB[iB,1],   rT[iT-1,1], rB[iB,2]])
            iB += 1                    
        
        #========== change at top =========
        elif float(rT[iT,0]) < float(rB[iB,0]):
            combiList.append([float(rT[iT,0]), rB[iB-1,1], rT[iT,1],   rT[iT,2]])
            iT += 1

 
    # return statement
    return np.array(combiList)

        
 
'''
==============================================================================
        Identifie regime change at top or bottom
==============================================================================
'''

def regimeChange(x_max, X_rib, X_ch, H_ch, R_rib, H_mea, TopBot):
    
    # distance of separation point from start of rounding as fraction of rib-radius
    f_dist = 0.33
    
    #  F = Flat Rib
    #  R = Round Rib UP
    #  O = Free GDL  UP
    # -O = Free GDL  DOWN
    # -R = Round Rib DONW
    
    reg_list = [[0,"F","stat"]] # origin [STATIC]
    
    n = 0
    while True:
        x = X_rib/2 + (X_rib + X_ch) * n - R_rib                   #  Flat  -> Round [STATIC]
        if x > x_max: break
        reg_list.append([x,"R","stat"])
        
        x = X_rib/2 + (X_rib + X_ch) * n - R_rib * f_dist          #  Round -> Open
        if x > x_max: break
        reg_list.append([x,"O","move"])
        
        x = X_rib/2 + (X_rib + X_ch) * n + X_ch/2                  #  Open  -> -Open [channel center]
        if x > x_max: break
        reg_list.append([x,"-O","chaCenter"])
        
        x = X_rib/2 + (X_rib + X_ch) * n + X_ch + R_rib * f_dist   # -Open  -> -Round
        if x > x_max: break
        reg_list.append([x,"-R","move"])

        x = X_rib/2 + (X_rib + X_ch) * n + X_ch + R_rib            # -Round -> Flat [STATIC]
        if x >= x_max: break   
        reg_list.append([x,"F","stat"])
        
        n += 1
        
    reg_list.append([x_max,"F","stat"])
    return np.array(reg_list)



'''
==============================================================================
        create curve of BPP
==============================================================================
'''

def BPP(x_array, X_rib, X_ch, H_ch, R_rib, H_mea, TopBot):
    
    y_array = np.zeros(x_array.shape)
    
    for x,iy in zip(x_array,range(y_array.size)):
        modulo = (x-X_rib/2) %(X_rib+X_ch)
        
        if modulo < X_ch:                                           # Channel
            y_array[iy] = H_ch     
        elif modulo < X_ch + R_rib:                                 # left Fillet
            x_rel = X_ch + R_rib - modulo
            y_array[iy] =   R_rib - np.sqrt(R_rib**2 - x_rel**2)
        elif modulo < X_ch + X_rib - R_rib:                         # Full Rib
            y_array[iy] = 0
        else:                                                       # rigth Fillet
            x_rel = X_ch + X_rib - R_rib - modulo
            y_array[iy] =   R_rib - np.sqrt(R_rib**2 - x_rel**2)
    
    if TopBot == "Top":
        return y_array + H_mea/2
    else: # TopBot == "Bot"
        return - y_array - H_mea/2 

 
'''
==============================================================================
        Supporting funcions
==============================================================================
'''
   
def kgV(m,n):
    """ calculate the least common multiple """
    if m == 0 or n == 0:
        print("kgV = 0")
    else:
        if m > n:
            m,n = n,m
        mCounter = m;
        while mCounter <= m*n:
            if mCounter % n == 0:
                return mCounter
            mCounter += m    
 
    
'''========================================================================='''


def print_regimes(regimes):
    "Print a chart of all regimes "
    
    print("------------------------------------")
    print("BdyIdx| x-Start |Bot Top| Annotation")
    print("------------------------------------")
    
    for idx in range(len(regimes)):
        x, B, T, anno = regimes[idx]
        print("  %2d  |%8.1f | %2s %2s | %s" %(idx, float(x), B, T, anno)) 

    return True

   
'''
==============================================================================
        Plot method
==============================================================================
'''

def plot_regimes(regimes, x_BPP, yT_BPP, yB_BPP, rT, rB, H_mea, H_chB, H_chT):
    
    # set-up plot
    fig, ax = plt.subplots(1)
    
    fig.canvas.manager.set_window_title("Fig. 0 - Mapped Regimes")
    
    ax.set_xlabel("$x\,/\,\mathrm{\mu m}$")
    ax.set_ylabel("$y\,/\,\mathrm{\mu m}$")
    
    # plot BPP outlines
    ax.plot(x_BPP,yT_BPP)
    ax.plot(x_BPP,yB_BPP)
    
    ax.scatter(np.float64(rT[:,0]), np.ones(len(rT))*H_mea/2)
    ax.scatter(np.float64(rB[:,0]), np.ones(len(rB))*H_mea/-2)
    
    ax.set_aspect(1)  
    
    
    # plot regime boundaries
    for r in regimes:
        if   "stat"      in r[3] : col="k" # Static
        elif "move"      in r[3] : col="r" # moveable
        elif "chaCenter" in r[3] : col="b" # channel center (moveable)
        elif "pushed"    in r[3] : col="g" # pushed (moveable)
        
        ax.vlines(float(r[0]), -H_mea-H_chB, H_mea+H_chT, color=col)
    
    for ix in range(regimes[:,0].size):       
        ax.text(float(regimes[ix,0]), -100, regimes[ix,1])
        ax.text(float(regimes[ix,0]), +100, regimes[ix,2]) 
        ax.text(float(regimes[ix,0]), +300, str(ix)      )
    
    return True


'''
==============================================================================
         start-up routine
==============================================================================
'''
 
if __name__ == "__main__":
    test_fkt()