# -*- coding: utf-8 -*-
"""
Created on Thu Mar 24 19:01:45 2022

@author: kohrn_simu
"""

from __future__ import division, absolute_import, \
                       print_function#, unicode_literals


# %matplotlib qt

''' import standard libraries'''       
import sys, os                  
import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import copy
import time
from scipy                 import optimize
from scipy.interpolate     import interp1d
from scipy.ndimage.filters import gaussian_filter1d
from contextlib            import contextmanager

''' import own function classes '''

# swith cwd to parent directory, if not yet done
if os.path.split(os.getcwd())[1] == "AnalytModel":
    os.chdir("..")

import AnalytModel.AM2_calcSingleReg           as c_calcReg
import AnalytModel.AM2_mapRegimes              as c_mapRegs
import AnalytModel.AM2_postprocessing          as c_post
import AnalytModel.AM2_smoothing               as c_smooth
#import AnalytModel.AM2_main

''' import own object classes''' 
import AnalytModel.class_Boundary              as c_boundary
import AnalytModel.class_Regime                as c_regime
import AnalytModel.class_Gap                   as c_gap


''' Globals '''
gEPS = 0.1


'''
===============================================================================
                    Class: all regiems
===============================================================================
'''



class AllRegimes:

#============= Constructor ===    
    def __init__(self, regimes, pD):
        
        self.pD       = pD # parameter Dictionary
        self.paraObj  = None
        
        self.bdy_list = list() # boundary list        
        self.reg_list = list() # regime list
        
        # Creating Boundary List (linked regimes empty)
        for idx in range(len(regimes)):
            newBdy = c_boundary.Boundary(pD, regimes[idx], idx, None, None)
            self.bdy_list.append(newBdy)
         
        # Creating Regime List & linking boundaries
        for idx in range(len(regimes)-1):
            newReg = c_regime.Regime(pD, regimes[idx], idx, self.bdy_list[idx], self.bdy_list[idx+1])
            self.reg_list.append(newReg)
            
        # Linking Regimes to boundaries
        self.bdy_list[ 0].rReg = self.reg_list[ 0] # first boundary
        self.bdy_list[-1].lReg = self.reg_list[-1] # last boundary
        
        for idx in range(1, len(self.bdy_list)-1): # inner boundaries
            self.bdy_list[idx].lReg = self.reg_list[idx-1]
            self.bdy_list[idx].rReg = self.reg_list[idx]
        
        # one homogenized map-set (x,h,u,s,t) of all regimes
        self.maps_hom = None
        
        # smoothed map-sets (x,h,u,s,t) of all regimes
        self.maps_smoothed = None

        # physical map-sets (...) of all regimes
        self.maps_phys = {}            
            
#============= getter Fkts ====================================================
    
    def bdy_list(self):
        "returning the list of all boundaries"
        return self.bdy_list

    def reg_list(self):
        "returning the list of all regimes"
        return self.reg_list     
    
    def nrOfRegs(self):
        "returning the total number of regimes"
        return len(self.reg_list)

    def nrOfBdys(self):
        "returning the total number of boundaries"
        return len(self.bdy_list)
    
    def xBdy_list(self):
        "returning a list of the x-values of all boundaries"
        x_list = list()
        for bdy in self.bdy_list:
            x_list.append(bdy.x)
        return x_list
    
    def reg(self, idx):
        "returning the regime with the requested index"
        return self.reg_list[idx]

    def bdy(self, idx):
        "returning the boundary with the requested index"
        return self.bdy_list[idx]

    def xMax(self):
        "returning the total lenght (maximum x-value) of the calculated element"
        return self.bdy_list[-1].x

    def xSep_list(self):
        "returning 2 lists (Bot, Top) of all x-separation points (first point contact->noContact)"
        xSepBot_list,       xSepTop_list       = list(),  list()
        BotHasContact_prev, TopHasContact_prev = True,    True
        
        # iteration over all regimes
        for reg in self.reg_list:
            
            # Bottom
            if   "O"     in reg.B() and BotHasContact_prev == True:
                xSepBot_list.append(reg.xStart())
                BotHasContact_prev = False
            elif "O" not in reg.B() and BotHasContact_prev == False:
                xSepBot_list.append(reg.xStart())
                BotHasContact_prev = True
            else:
                pass
            
            # Top
            if   "O"     in reg.T() and TopHasContact_prev == True:
                xSepTop_list.append(reg.xStart())
                TopHasContact_prev = False
            elif "O" not in reg.T() and TopHasContact_prev == False:
                xSepTop_list.append(reg.xStart())
                TopHasContact_prev = True
            else:
                pass
        
        # return statement
        return xSepBot_list, xSepTop_list



# =============== boolean function ============================================
    
    def allFinal(self):
        "returns True, if all regimes are final (=calculated)"
        for reg in self.reg_list:
            
            # as soon as one regime is NOT-final -> return: False
            if not reg.isFinal():
                return False
        
        # all regimes are final -> return: True
        return True
    

#============= delete all results =============================================

    def deleteAllRes(self):
        "deletes all results by setting the flag: isFinal = False & deleting homogenized maps"
        for reg in self.reg_list:
            reg.final = False        
        # delete homogenized maps
        self.maps_hom = None
        self.maps_smoothed = None
        self.maps_phys = {}  
        # return statement
        return True

#'''
#===============================================================================
#                    Jump Boundaries
#===============================================================================
#'''

    def jumpBounds(self, jumpBdy, statBdy):
        "jumperBoundary surpasses staticBoundary -> also regimes are modified"
        
        # safety check
        if jumpBdy.isStatic():
            print(">> Error at boundary jump: jumpBdy is static!")
            return False
        
        # jump from left to right
        if   jumpBdy.rReg == statBdy.lReg:
            print("jump from left to right")
            print("jumpBdy: idx=%d, BT-change: %s -> %s" %(jumpBdy.idx, jumpBdy.lReg.BT(), jumpBdy.rReg.BT()))
            print("statBdy: idx=%d, BT-change: %s -> %s" %(statBdy.idx, statBdy.lReg.BT(), statBdy.rReg.BT()))
    
            # Creating alias
            lReg = jumpBdy.lReg
            mReg = jumpBdy.rReg # = statBdy.lReg
            rReg = statBdy.rReg
            
            # Reconnecting boundaries to regions
            lReg.rBdy = statBdy
            mReg.lBdy = statBdy
            mReg.rBdy = jumpBdy
            rReg.lBdy = jumpBdy
            
            # Reconnecting regions to boundaries
            statBdy.lReg = lReg
            statBdy.rReg = mReg
            jumpBdy.lReg = mReg
            jumpBdy.rReg = rReg
            
            # Reset x-value of jumped boundary
            jumpBdy.x = statBdy.x + gEPS
            
            # Resetting regime indicators T and B for mid regime
            critSite = statBdy.critSite()
            print("critSite of stat bdy = %s" %critSite)
            if   critSite == "B":
                mReg.B = lReg.B
                mReg.T = rReg.T           
            elif critSite == "T":
                mReg.B = rReg.B
                mReg.T = lReg.T             
            elif critSite == "BT":
                mReg.B = lReg.B
                mReg.T = lReg.T     
            else:
                print(">> Error at boundary jump: no critial site found in static boundary")
                return False
        
        # jump from right to left
        elif jumpBdy.lReg == statBdy.rReg: 
            print("jump from right to left")
            print("jumpBdy: idx=%d, BT-change: %s -> %s" %(jumpBdy.idx, jumpBdy.lReg.BT(), jumpBdy.rReg.BT()))
            print("statBdy: idx=%d, BT-change: %s -> %s" %(statBdy.idx, statBdy.lReg.BT(), statBdy.rReg.BT()))
            
            # Creating alias
            lReg = statBdy.lReg
            mReg = statBdy.rReg # = jumpBdy.lReg
            rReg = jumpBdy.rReg
            
            # Reconnecting boundaries to regions
            rReg.lBdy = statBdy
            mReg.rBdy = statBdy
            mReg.lBdy = jumpBdy
            lReg.rBdy = jumpBdy
            
            # Reconnecting regions to boundaries
            statBdy.rReg = rReg
            statBdy.lReg = mReg
            jumpBdy.rReg = mReg
            jumpBdy.lReg = lReg
            
            # Reset x-value of jumped boundary
            jumpBdy.x = statBdy.x - gEPS
            
            # Resetting regime indicators T and B for mid regime
            critSite = statBdy.critSite()
            if   critSite == "B":
                mReg.B = rReg.B
                mReg.T = lReg.T           
            elif critSite == "T":
                mReg.B = lReg.B
                mReg.T = rReg.T             
            elif critSite == "BT":
                mReg.B = rReg.B
                mReg.T = rReg.T     
            else:
                print(">> Error at boundary jump: no critial site found in static boundary")
                return False
        
        # illegal jump
        else:
            print(">> Error at boundary jump: no shared regime!")
            return False
        
        # set involved regimes to false
        lReg.final = False
        mReg.final = False
        rReg.final = False
        
        # switching indices of boundaries
        idx_statOld = statBdy.idx
        statBdy.idx = jumpBdy.idx
        jumpBdy.idx = idx_statOld
        
        # sorting the boundaries
        self.sort_bdyList()
        
        # bdy jump successfully completed
        return True

#============= sort boundarie list by x-value =================================
        
    def sort_bdyList(self):
        "sort boundaries by increasing x-value"
        self.bdy_list.sort(key = lambda a: a.x)


#'''
#===============================================================================
#                    Homogenize
#===============================================================================
#'''

    def homogenize(self, nrOfPoints, mode="default"):
        "homogenizes results to one map-set from all regimes"
        
        # homogenizing the results
        maps_hom = c_post.homogenize(self, nrOfPoints, mode)
        
        # storing the results in object attribute
        self.maps_hom = maps_hom
            
        return True

#'''
#===============================================================================
#                    Smooth
#===============================================================================
#'''

    def smooth(self, Del_h0, smMethod, sigma, trueStiff, plotRes=False, plotAdditional=False):
        "smooth results from homogenized results"
        
        if smMethod == "physics":
            # physics-based smoothing: force equilibrium
            maps_sm = c_smooth.main(self, trueStiff=trueStiff, 
                                    doPlot=plotRes, doPlotAdditional=plotAdditional)
        else:
            # math based smoothing: gaussian
            maps_sm = c_post.smooth_phys(self, Del_h0, smMethod=smMethod, sigma=sigma)
        
        # storing the results in object attribute
        self.maps_smoothed = maps_sm
            
        return True


#'''
#===============================================================================
#                    Print regimes
#===============================================================================
#'''
        
    def print_regimes(self):        
        print("===")
        for reg in self.reg_list:
            anno = "      "
            if reg.lBdy.tag == "static": 
                anno = "static"
            print("Bdy-idx: %2d | x=%5.1f,   B:%2s,   T:%2s - %s" %(reg.idx, reg.lBdy.x, reg.B(), reg.T(), anno)) 
        
        print("Bdy-idx: %2d | x=%5.0d,   B:--,   T:-- - %s" %(self.bdy_list[-1].idx, self.bdy_list[-1].x, "static"))
        

#'''
#===============================================================================
#                    Create correction index list
#===============================================================================
#'''

    def create_gapList(self):
        "returns a list of dicts. Each dict contains information about one gap (heigt/shear/combined)"
    
        # create return list for gap-dictionaries
        gap_list = list()
        
        # loop over all regiem-indeices
        for bdy in self.bdy_list[1:-1]:
            
            # regime with both sides open
            if (    bdy.lReg.BT() == "OO"
                or (bdy.lReg.BT() == "OF" and bdy.rReg.BT() == "-OF")
                or (bdy.lReg.BT() == "FO" and bdy.rReg.BT() == "F-O")):
                # ... add more casees!
                
                # crate gap-object
                gap = c_gap.Gap(self, bdy)
    
                # Append new gap-Dictionary to result-list
                gap_list.append(gap)
            
         # Return list of gap-dictionaries   
        return gap_list


