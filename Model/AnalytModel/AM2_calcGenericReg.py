# -*- coding: utf-8 -*-
"""
Created on Wed Mar 30 15:11:01 2022

@author: kohrn_simu
"""

from __future__ import division, absolute_import, \
                       print_function#, unicode_literals

# %matplotlib qt
                       
''' import standard libraries'''                   
import numpy             as np
import matplotlib.pyplot as plt
import sys, os
import copy

''' imoprt own function classes '''

# swith cwd to parent directory, if not yet done
if os.path.split(os.getcwd())[1] == "AnalytModel":
    os.chdir("..")

import AnalytModel.AM2_calcSingleReg as c_calcReg
                   

# Flags
DEBUG_YN = False


'''
===============================================================================
                    Calculate n
===============================================================================
'''

C_n =  3.208258
a_H =  0.367815
a_R = -0.396213

def calc_n_local(paraDict, BotTop):
    
    if   BotTop == "Bot":
        n = C_n * paraDict['H_mea']**a_H * paraDict['R_ribB']**a_R
    elif BotTop == "Top":
        n = C_n * paraDict['H_mea']**a_H * paraDict['R_ribT']**a_R
    
    return n

'''
===============================================================================
                    Shear Distribution funktions
===============================================================================
'''
def f_T(y,H,n):
    return (0.5 + y/H)**n

def F1_T(y,H,n):
    return H   /(n+1)       * ( (0.5+y/H)**(n+1) - 0 )

def F2_T(y,H,n):
    return H**2/(n+1)/(n+2) * ( (0.5+y/H)**(n+2) - 0 )
    

#==============================================================================

def f_B(y,H,n):
    return (0.5 - y/H)**n

def F1_B(y,H,n):
    return - H   /(n+1)        * ( (0.5-y/H)**(n+1) - 1 )

def F2_B(y,H,n):
    return H/(n+1) * ( H/(n+2) * ( (0.5-y/H)**(n+2) - 1 ) + y + H/2 )


'''
===============================================================================
                    Test Shear Distribution Funktions
===============================================================================
'''
def Test_distFunktions(H,n,res=100):
    
    res = H/res
    y = np.arange(-H/2,H/2+res,res)
    
    fig2, subPlts2 = plt.subplots(3)
    
    # Top
    subPlts2[0].plot(y, f_T(y,H,n),  "-b", label="f_T(y)")
    subPlts2[1].plot(y, F1_T(y,H,n),"-b", label="F1_T(-H/2,y)")  
    subPlts2[2].plot(y, F2_T(y,H,n),"-b", label="F2_T(-H/2,y)")
    
    # Bot
    subPlts2[0].plot(y, f_B(y,H,n), "-r", label="f_B(y)")
    subPlts2[1].plot(y, F1_B(y,H,n),"-r", label="F1_B(-H/2,y)")  
    subPlts2[2].plot(y, F2_B(y,H,n),"-r", label="F2_B(-H/2,y)")
    
    F1_B_num, F2_B_num = np.zeros(y.size), np.zeros(y.size)
    F1_T_num, F2_T_num = np.zeros(y.size), np.zeros(y.size)
    for i in range(len(y)):
        yy = np.linspace(-H/2,y[i],101)
        
        F1_B_num[i] = np.trapz(f_B( yy,H,n), x=yy)
        F2_B_num[i] = np.trapz(F1_B(yy,H,n), x=yy)        
        F1_T_num[i] = np.trapz(f_T( yy,H,n), x=yy)
        F2_T_num[i] = np.trapz(F1_T(yy,H,n), x=yy)

    subPlts2[1].plot(y, F1_B_num,"--g", label="F1_B(-H/2,y)_num")  
    subPlts2[2].plot(y, F2_B_num,"--g", label="F2_B(-H/2,y)_num")
    subPlts2[1].plot(y, F1_T_num,"--g", label="F1_T(-H/2,y)_num")  
    subPlts2[2].plot(y, F2_T_num,"--g", label="F2_T(-H/2,y)_num")

    
    for i in range(len(subPlts2)):
        subPlts2[i].plot(y,np.zeros(y.size), "k")
        subPlts2[i].legend()
    
    return True

           
'''
===============================================================================
                    Calculation of Shear stress distribution
===============================================================================
'''
def f_t(pD, t_T, t_B, y, H) :
    
    nT = pD['calc_n'](pD, "Top")
    nB = pD['calc_n'](pD, "Bot")
        
    return t_T * f_T(y,H,nT) + t_B * f_B(y,H,nB)

'''
===============================================================================
                    Calculate Regimes
===============================================================================
'''

def singleRegime(thisReg, Del_h0, xLen, h_T0, h_B0, x_off, 
                 simuMode="full", whatAmI=False):
    
    regStr = thisReg.get_baseReg()
    
    # check if calculation of n is externally given
    if ("calc_n" not in thisReg.pD) or (thisReg.pD['calc_n'] == None):
        # not externally given, use local calculation
        thisReg.pD['calc_n'] = calc_n_local         
    
    # extract data from  para dict
    H  = thisReg.pD['H_mea']
    Ey = thisReg.pD['Ey']
    G  = thisReg.pD['G']
    k  = thisReg.pD['k']
    
    # calculate shear distribution coefficients
    nT = thisReg.pD['calc_n'](thisReg.pD, "Top")
    nB = thisReg.pD['calc_n'](thisReg.pD, "Bot")
    
    # in case xLen is single element in list TODO: not nice workaround
    try:    xLen = xLen[0]
    except: pass

    
    # cases: simulation mode
    if simuMode == "full":
        x_vec = np.linspace(   0, xLen, thisReg.pD['xNodes_reg'])   
        y_vec = np.linspace(-H/2,  H/2, thisReg.pD['yNodes'])    
    elif simuMode == "reduced":
        x_vec = np.array([   0, xLen])   
        y_vec = np.array([-H/2,  H/2])    
    elif simuMode == "coupledSim":
        x_vec = np.linspace( 0, xLen, thisReg.pD['xNodes_reg'])
        yLenElems = thisReg.geoDict["yLenElems"]   
        y_vec = np.array( [np.sum(yLenElems[0:idx])*10**6 -H/2 for idx in range(len(yLenElems)+1)] )
        # correction of rounding errors
        y_vec = np.where(y_vec >  H/2,  H/2, y_vec)
        y_vec = np.where(y_vec < -H/2, -H/2, y_vec)

    
    # create empty maps for results
    hMap, uMap, sMap, tMap, eMap, dtdxMap = getEmptyMaps([x_vec.size, y_vec.size])
    
    
    # in Gap Closure: create storrage for uncorrected values
    if  thisReg.gap == None:
        doGapCorrection = False
    else:
        doGapCorrection = True
        thisReg.maps_uncor = {"h" : np.zeros((len(x_vec),2)),
                              "t" : np.zeros((len(x_vec),2)) }
    
    
    # flip
    flipped = thisReg.isFlipped()
    
    
    # loop over all x
    for ix in range(x_vec.size):
        DEBUG("\n %s : ix=%d" %(regStr, ix))
        
        if flipped:
            h_B0, h_T0 = -h_T0, -h_B0
        
        # getx x-value
        x = x_vec[ix]

        #===== get top and bot key-values for each specific case ==============
         
        # (1) FF
        if   regStr == "FF":
            h_B, h_T, t_B, t_T, dtdx_B, dtdx_T = c_calcReg.cReg_FF( thisReg, h_B0, h_T0, x_off, x)

        # (2) FR
        elif regStr == "FR":
            h_B, h_T, t_B, t_T, dtdx_B, dtdx_T = c_calcReg.cReg_FR( thisReg, h_B0, h_T0, x_off, x, flipped)

        # (3) FO
        elif regStr == "FO":
            h_B, h_T, t_B, t_T, dtdx_B, dtdx_T = c_calcReg.cReg_FO( thisReg, h_B0, h_T0, x_off, x, flipped)

        # (4) RR
        elif regStr == "RR":
            h_B, h_T, t_B, t_T, dtdx_B, dtdx_T = c_calcReg.cReg_RR( thisReg, h_B0, h_T0, x_off, x, flipped)

        # (5) R-R
        elif regStr == "R-R":
            h_B, h_T, t_B, t_T, dtdx_B, dtdx_T = c_calcReg.cReg_R_R(thisReg, h_B0, h_T0, x_off, x, xLen, flipped)

        # (6) RO
        elif regStr == "RO":
            h_B, h_T, t_B, t_T, dtdx_B, dtdx_T = c_calcReg.cReg_RO( thisReg, h_B0, h_T0, x_off, x, flipped)    

        # (7) -RO
        elif regStr == "-RO":
            h_B, h_T, t_B, t_T, dtdx_B, dtdx_T = c_calcReg.cReg__RO(thisReg, h_B0, h_T0, x_off, x, xLen, flipped)

        # (8) OO
        elif regStr == "OO":
            h_B, h_T, t_B, t_T, dtdx_B, dtdx_T = c_calcReg.cReg_OO( thisReg, h_B0, h_T0, x_off, x, xLen)            

        # [ERROR] not existing base reg
        else:
            print("!!! ERROR: THIS REGIME CASE DOES NOT EXIST - REGIME: %s !!!" %(regStr))     
            sys.exit()

        #======================================================================
        
        # adjust key-values if filipped
        if flipped:
            h_B0,   h_T0   = -h_T0,   -h_B0
            h_B,    h_T    = -h_T,    -h_B
            t_B,    t_T    = -t_T,    -t_B
            dtdx_B, dtdx_T = -dtdx_T, -dtdx_B


#==========================================
#   gap correction      

        # Correct h, t, dt/dx for bot and top -> Gap closure
        if  doGapCorrection:
            
            # safe uncorrected data to regime
            thisReg.maps_uncor["h"][ix,0] = copy.deepcopy(h_B)
            thisReg.maps_uncor["h"][ix,1] = copy.deepcopy(h_T)
            thisReg.maps_uncor["t"][ix,0] = copy.deepcopy(t_B)
            thisReg.maps_uncor["t"][ix,1] = copy.deepcopy(t_T)

            if regStr=="OO":
                h_B, t_B, dtdx_B, h_T, t_T, dtdx_T = gapCorrection_OO(thisReg, x, h_B, t_B, dtdx_B, h_T, t_T, dtdx_T, h_B0, h_T0)
            else:
                h_B, t_B, dtdx_B, h_T, t_T, dtdx_T = gapCorrection(   thisReg, x, h_B, t_B, dtdx_B, h_T, t_T, dtdx_T)


#==========================================
#   calculate field values from key values
        
        # Shear
        t_vec = f_t(thisReg.pD, t_T, t_B, y_vec, H)
        
        u_T =  h_T - H/2
        u_B =  h_B + H/2      
        
        Del_h = u_T - u_B
        
        # Stress
        sB1 = 1/H * (Del_h * Ey)
        sB2 = 1/H * (dtdx_B * F2_B(H/2,H,nB) )
        sB3 = 1/H * (dtdx_T * F2_T(H/2,H,nT) )
        s_B = + sB1 + sB2 + sB3        
        
        s1 = dtdx_B * F1_B(y_vec,H,nB)
        s2 = dtdx_T * F1_T(y_vec,H,nT)
        s_vec  = s_B - s1 - s2
        
        # Strain
        e_vec = s_vec/Ey
        
        # Displacement
        u1 = 1/Ey * (s_B    * (H/2+y_vec)     )
        u2 = 1/Ey * (dtdx_T * F2_T(y_vec,H,nT))
        u3 = 1/Ey * (dtdx_B * F2_B(y_vec,H,nB))
        u_vec  = u_B + u1 - u2 - u3
        
        # absolute heigth
        h_vec   = u_vec + y_vec            
        
        # copy in maps
        hMap[ix,:] = h_vec 
        uMap[ix,:] = u_vec
        sMap[ix,:] = s_vec        
        tMap[ix,:] = t_vec
        eMap[ix,:] = e_vec
        
        # change in shear
        dtdxMap[ix,0] = dtdx_B
        dtdxMap[ix,1] = dtdx_T
        
#==========================================
#   prepare for return
                     
    # correct if inverted
    if thisReg.isInverted(): # Inversion of mirrored regimes 
        hMap    = np.flip(   hMap, axis=0)
        uMap    = np.flip(   uMap, axis=0)
        sMap    = np.flip(   sMap, axis=0)
        tMap    = np.flip(   tMap, axis=0) *(-1)  
        dtdxMap = np.flip(dtdxMap, axis=0)

        if doGapCorrection:
            thisReg.maps_uncor["h"] = np.flip(thisReg.maps_uncor["h"], axis=0)
            thisReg.maps_uncor["t"] = np.flip(thisReg.maps_uncor["t"], axis=0) *(-1)        


    # returning the vectors and maps
    return x_vec, y_vec, tMap, eMap, sMap, uMap, hMap, dtdxMap


'''
===============================================================================
                    Gap Correction
===============================================================================
'''

def gapCorrection_OO(thisReg, xReg, h_B, t_B, dtdx_B, h_T, t_T, dtdx_T, h_B0, h_T0):
   
    G  = thisReg.pD["G"]
    k  = thisReg.pD["k"]
    
    x_off   = 0
    xRegLen = thisReg.xLen()
    _, _, t_B_end, t_T_end, _, _ = c_calcReg.cReg_OO(thisReg, h_B0, h_T0, x_off, xRegLen, xRegLen)
    
    DEL_t_end = t_T_end - t_B_end 
    
    a = DEL_t_end/2  /(k*G) / xRegLen       
    b = 0
    c = 0
        
    DEL_h    =      (a/2 * xReg**2 + b * xReg + c )
    DEL_t    = k*G* (a   * xReg    + b            )
    DEL_dtdx = k*G* (a                            )
                     
    h_B    += DEL_h
    t_B    += DEL_t
    dtdx_B += DEL_dtdx

    h_T    -= DEL_h
    t_T    -= DEL_t
    dtdx_T -= DEL_dtdx

    return h_B, t_B, dtdx_B, h_T, t_T, dtdx_T 

#==============================================================================

def gapCorrection(thisReg, x, h_B, t_B, dtdx_B, h_T, t_T, dtdx_T):
    
    if thisReg.needGapCorrection("Bot"):
        h_B, t_B, dtdx_B = __gapCorrection(thisReg, x, h_B, t_B, dtdx_B, "Bot")
    
    if thisReg.needGapCorrection("Top"):
        h_T, t_T, dtdx_T = __gapCorrection(thisReg, x, h_T, t_T, dtdx_T, "Top")
        
    return h_B, t_B, dtdx_B, h_T, t_T, dtdx_T



#==============================================================================

def __gapCorrection(thisReg, x, h, t, dtdx, flag_BT):

    # get gap-factors
    f_C_h = thisReg.gap.f_C_h
    f_C_t = thisReg.gap.f_C_t
    
    # early exit, no change (if no gap factors)
    if f_C_h == None or f_C_t == None:
        print(">> no gap-paramters (f_C_h, f_C_t), no change!")
        return h, t, dtdx
    
    # extract paramters from paraDict    
    Ey = thisReg.pD["Ey"]
    G  = thisReg.pD["G"]
    H  = thisReg.pD["H_mea"]
    k  = thisReg.pD["k"]
    
    n  = thisReg.pD['calc_n'](thisReg.pD, flag_BT)
    
    a  = np.sqrt(k*G/Ey * F2_B(H/2,H,n))
    
# === Differenciation: Bot/Top Regime ===
    if flag_BT == "Bot":
        fact_BT =  1
        h_GAP   = thisReg.gap.hB_gap
        t_GAP   = thisReg.gap.tB_gap
    else: # flag_BT == "Top"
        fact_BT =  -1
        h_GAP   = thisReg.gap.hT_gap
        t_GAP   = thisReg.gap.tT_gap
    
# === Differenciation: not/inverted =====
    if not thisReg.isInverted():
        fact_LR = 1
    else: # is inverted
        fact_LR = -1
 
#================================================================== 
    
    x0          = thisReg.gap.x0(             thisReg,"BotTop")
    xLenGap     = thisReg.gap.xLen(                   "BotTop")
    xLenGapSide = thisReg.gap.xLen_side(      thisReg,"BotTop")
    xLen_OO     = thisReg.gap.xLen_side_OO(   thisReg,"BotTop")
    xLen_notOO  = thisReg.gap.xLen_side_notOO(thisReg,"BotTop")
    
#    print("regIdx = %3d - %s | x0 = %6.1f | xLenGap = %6.1f | xLenGapSide = %6.1f| xLen_OO = %6.1f | xLen_notOO = %6.1f" 
#          %(thisReg.idx, flag_BT, x0, xLenGap, xLenGapSide, xLen_OO, xLen_notOO))

    fract_site = xLenGapSide / xLenGap
    
    h_GAPsite = fract_site * h_GAP
    t_GAPsite = fract_site * t_GAP
    

    # calculate C-Values    
    C_h = h_GAPsite / (  np.exp( xLen_notOO/a) * (xLen_OO/a + 1)
                       + np.exp(-xLen_notOO/a) * (xLen_OO/a - 1) )              * fact_LR

    C_t =  t_GAPsite / (  np.exp( xLen_notOO/a) * (xLen_OO/a + 1)
                        + np.exp(-xLen_notOO/a) * (xLen_OO/a - 1) ) /(k*G) *a   * fact_BT


    C = C_h * f_C_h + C_t * f_C_t


    #print("C = %f" %C)

#================================================================== 
    
    xGap = x0 + x
    
    h    += C      * (np.exp(xGap/a) - np.exp(-xGap/a))     
    t    += C/a    * (np.exp(xGap/a) + np.exp(-xGap/a)) *k*G
    dtdx += C/a**2 * (np.exp(xGap/a) - np.exp(-xGap/a)) *k*G
    
#==================

    return h, t, dtdx
    
 
'''
===============================================================================
                    Calc h_T0 / h_B0 from off-set / x_off
===============================================================================
'''
def calc_h_T0(H, x_Toff, R_T, Del_h0):    
    h_T0 =  (H-Del_h0)/2 + (R_T - np.sqrt(R_T**2-(x_Toff)**2))
    return h_T0


def calc_h_B0(H, x_Boff, R_B, Del_h0):    
    h_B0 = -(H-Del_h0)/2 - (R_B - np.sqrt(R_B**2-(x_Boff)**2))    
    return h_B0


def calc_x_off(x_list, BT_list, i):
    if BT_list[i] in [0,2,-2]:                                                # flat or open
        return 0
    elif BT_list[i] == 1 and BT_list[i-1] == 1:
        return x_list[i-1] - x_list[i-2] + calc_x_off(x_list, BT_list, i-1)   # +round and left: +round
    elif BT_list[i] == -1 and BT_list[i+1] == -1:
        return x_list[i+1] - x_list[i+2] + calc_x_off(x_list, BT_list, i+1)   # -round and left: -round
    else:
        return 0                                                # else

 
'''
===============================================================================
                    create Empyt Maps
===============================================================================
'''
def getEmptyMaps(dims):
    hMap = np.zeros(dims)
    uMap = np.zeros(dims)
    sMap = np.zeros(dims)
    tMap = np.zeros(dims)
    eMap = np.zeros(dims)
    dtdxMap = np.zeros((dims[0],2))
    return hMap, uMap, sMap, tMap, eMap, dtdxMap
    
    
'''
===============================================================================
                    DEBUG Routine
===============================================================================
'''
def DEBUG(string):
    if DEBUG_YN:
        print(string)
    return True