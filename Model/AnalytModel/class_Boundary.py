# -*- coding: utf-8 -*-
"""
Created on Thu Mar 24 19:01:45 2022

@author: kohrn_simu
"""

from __future__ import division, absolute_import, \
                       print_function#, unicode_literals

# %matplotlib qt

''' import standard libraries'''                       
import math, os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import copy
import time
from scipy                 import optimize
from scipy.interpolate     import interp1d
from scipy.ndimage.filters import gaussian_filter1d
import copy

''' imoprt own function classes '''

# swith cwd to parent directory, if not yet done
if os.path.split(os.getcwd())[1] == "AnalytModel":
    os.chdir("..")

import AnalytModel.AM2_calcSingleReg  as c_calcReg
import AnalytModel.AM2_mapRegimes     as c_mapRegs
import AnalytModel.AM2_postprocessing as c_post
import AnalytModel.AM2_calcGenericReg as c_genReg


''' Globals '''
# none


'''
===============================================================================
                    Class: Boundary
===============================================================================
'''
    
class Boundary:

#============= Constructor ====================================================
    def __init__(self, pD, regime, idx, lReg, rReg):
           
        # overall
        self.pD      = pD # paraDict
        
        # boundary related
        self.idx   = idx
        self.x     = float(regime[0])  
        if "stat" in regime[3]:     # boundary is static
            self.tag   = "static"      
        else:                       # boundary is moveable (including jumpable)
            self.tag   = "move"

        
        # regimes
        self.lReg    = lReg
        self.rReg    = rReg

        
#============= getter Fcts ====================================================
    
    def x(self):
        return self.x

    def tag(self):
        return self.tag
    
    def h_final(self):
        # left regime is final
        if self.lReg.isFinal():
            return True
        # right regime is final
        if self.rReg.isFinal():
            return True
        # boundary is static
        if self.isStatic():
            # both connected regimes have no "open"
            if ("O" not in self.lReg.BT()) and ("O" not in self.rReg.BT()):
                return True
            
        # No True-Case has been selected    
        return False
    
    #============= Boolean ===
    
    def isStatic(self):
        "True, if the boundary is static (geometrical boundary)"
        if self.tag == "static":
            return True
        else:
            return False
        
    
#============= calculate x-offset =============================================
        
    def x_off(self, BT="BT", recursion=None):
        """
        calculate x-offset (distance from rounding-start), else 0
        BT = "BT" -> return [x_off_Bot, x_off_Top]
        B  = "B"  -> return x_off_Bot
        T  = "T"  -> return x_off_Top
        """
#        print("lReg.B = %s, lReg.T = %s, rReg.B = %s, rReg.T = %s" %(str(self.lReg.B()), str(self.lReg.T()), str(self.rReg.B()), str(self.rReg.T()) ))
        
        # Bot
        if   self.lReg.B() ==  "R" and (recursion=="BotLeft" or recursion==None):
            x_off_B = self.lReg.lBdy.x_off(BT="B", recursion="BotLeft") + self.lReg.xLen()
            
        elif self.rReg.B() == "-R"and (recursion=="BotRight" or recursion==None):
            x_off_B = self.rReg.rBdy.x_off(BT="B", recursion="BotRight") + self.rReg.xLen()
        else:
            x_off_B = 0
            
        # Top
        if   self.lReg.T() ==  "R" and (recursion=="TopLeft" or recursion==None):
            x_off_T = self.lReg.lBdy.x_off(BT="T", recursion="TopLeft") + self.lReg.xLen()
            
        elif self.rReg.T() == "-R" and (recursion=="TopRight" or recursion==None):
            x_off_T = self.rReg.rBdy.x_off(BT="T", recursion="TopRight") + self.rReg.xLen()
        else:
            x_off_T = 0
            
        if   BT == "BT": return {"B":x_off_B, "T":x_off_T}
        elif BT == "B" : return  x_off_B
        elif BT == "T" : return  x_off_T
        else:            
            print("Error: no identifier (B/T) submitted")
            return  None
        
#============= calculate h_B/h_T at boundary ==================================        

    def h_bdy(self, Del_h0):
        """
        calculate Top/Bot-height at boundary, else False
        -> return [h_Bot, h_Top]
        """
        
#        print(">bdy-idx=%d => lRegBT=%s, rRegBT=%s" %(self.idx, self.lReg.BT(), self.rReg.BT()))
#        print("lReg.isFinal=%s , rReg.isFinal=%s"   %(str(self.lReg.isFinal()),str(self.rReg.isFinal())))
        
        # left regime is final
        if   self.lReg.isFinal():
            return self.lReg.h_bdy("R")
        
        # right regime is final
        elif self.rReg.isFinal():
            return self.rReg.h_bdy("L")
        
        # boundary is static [AND] both connected regimes have no "open"
        
        #print(">bdy-idx=%d => lRegBT=%s, rRegBT=%s" %(self.idx, self.lReg.BT(), self.rReg.BT()))
        
        if self.isStatic() and ("O" not in self.lReg.BT()) and ("O" not in self.rReg.BT()):
            x_off = self.x_off()
            
            h_B = c_genReg.calc_h_B0(self.pD['H_mea'], x_off["B"], self.pD['R_ribB'], Del_h0)
            h_T = c_genReg.calc_h_T0(self.pD['H_mea'], x_off["T"], self.pD['R_ribT'], Del_h0)
            
            return [h_B, h_T]


#============ TEST ====
        if self.lReg.noOpenSide() or self.rReg.noOpenSide():
            
            x_off = self.x_off()   
            h_B = c_genReg.calc_h_B0(self.pD['H_mea'], x_off["B"], self.pD['R_ribB'], Del_h0)
            h_T = c_genReg.calc_h_T0(self.pD['H_mea'], x_off["T"], self.pD['R_ribT'], Del_h0)
            
            return [h_B, h_T] 
        
#============ TEST END ====

           
        # default result
        print("height at boundary cannot be calculated.")
        return  None        

        
#============= determine critical site of the boundary (regime change) ========
            
    def critSite(self):
        "returns the critical site (T/B/TB/None) of the boundary -> site, where the change occures"
        
        Bchange, Tchange = False, False
        
        if self.lReg.B() != self.rReg.B():
            Bchange = True
            
        if self.lReg.T() != self.rReg.T():
            Tchange = True
        
        if       Bchange and     Tchange: return "BT"
        elif     Bchange and not Tchange: return "B"
        elif not Bchange and     Tchange: return "T"
        else:                             return None
 
       
#============= moves bounday to metch shear in nabouring regimes ==============
        
    def adjustBdy(self, howToSolve, Del_h0):
        "Moves boundary until shear stresses on critical site (B/T) are equal"
        

        
        #return True, None
        
        # fail check
        if self.isStatic():
            print("Boundary (idx=%d) is static -> no adjustment!" %self.idx)
            return True, None
        
        # symmetric boundaries -> no adjustment!
        if self.lReg.BT().replace("-","") == self.rReg.BT().replace("-",""):
            return True, None        
        
        # determining critical site
        critSite = self.critSite()
        
        print("bdy to be adjusted: idx = %d [%s|%s] -> howToSolve: %s, critSite = %s" 
              %(self.idx, self.lReg.BT(), self.rReg.BT(), howToSolve, critSite))
        
        # calculating new x-value
        nullstelle = optimize.fsolve(self.__f_xSep, self.x, args=(critSite, howToSolve, Del_h0))       
        x_new = nullstelle[0]
        
        # return (jump nessessary?)
        if x_new < self.lReg.lBdy.x:
            print("=> Boundary jumps over left boundary!")
            return False, {"jumpBdy": self, "statBdy":self.lReg.lBdy}
        
        elif x_new > self.rReg.rBdy.x:
            print("=> Boundary jumps over right boundary!")
            return False, {"jumpBdy": self, "statBdy":self.rReg.rBdy}
        
        else:
            print("-|- boundary (idx=%d) moved: old x: %.2f | new x: %.2f" %(self.idx, self.x , x_new))
            self.x = x_new
            return True, None
        

#============= optimization function ==============
            
    def __f_xSep(self, x_sep, critSite, howToSolve, Del_h0):
       
        # store original x_value and set x to interation x (x_sep)
        x_orig = self.x
        self.x = x_sep
        
        
        lxLen = x_sep            - self.lReg.xStart()
        rxLen = self.rReg.xEnd() - x_sep
    
    #==========================================================================
        
        if   "fromLeft" in howToSolve:
            lh0_B, lh0_T = self.lReg.lBdy.h_bdy(Del_h0)
            lx_offs      = self.lReg.lBdy.x_off("BT")
            
            if howToSolve == "fromLeft_2sites":
                rh0_B, rh0_T = self.rReg.rBdy.h_bdy(Del_h0)
                rx_offs      = self.rReg.rBdy.x_off("BT")

        elif "fromRight" in howToSolve:
            rh0_B, rh0_T = self.rReg.rBdy.h_bdy(Del_h0)
            rx_offs      = self.rReg.rBdy.x_off("BT")
            
            if howToSolve == "fromRight_2sites":
                lh0_B, lh0_T = self.lReg.lBdy.h_bdy(Del_h0)
                lx_offs      = self.lReg.lBdy.x_off("BT")


    #==========================================================================          
        
        if "R-R_fromLeft" == howToSolve:
            
            # == Reg 1 (left): ===========           
            _, _, tMap, _, _, _, hMap, _ = c_genReg.singleRegime(self.lReg, Del_h0, lxLen, lh0_T, lh0_B, lx_offs,
                                                              simuMode = "reduced",
                                                              whatAmI  = "1.regInBdyAdj")            

            # shear is stored to provide information to some second regs in bdy-adjustment (eg. OO, OR, RO)            
            self.lReg.maps.update({'t':tMap})

            
            # == Reg2 (right) ===========
            h0_B = hMap[-1, 0]
            h0_T = hMap[-1,-1]
            x_offs     = self.x_off("BT")
            _, _, tMap, _, _, _, hMap, _    = c_genReg.singleRegime(self.rReg, Del_h0, rxLen, h0_T, h0_B, x_offs,
                                                              simuMode = "reduced",
                                                              whatAmI  = "2.regInBdyAdj")            
            t_B_Reg1_last  = tMap[-1, 0]
            t_T_Reg1_last  = tMap[-1,-1]  

            # shear is stored to provide information to some second regs in bdy-adjustment (eg. OO, OR, RO)            
            self.rReg.maps.update({'t':tMap})
       
            
            # == Reg3 (right-right) ===========
            h0_B   = hMap[-1, 0]
            h0_T   = hMap[-1,-1]
            x_offs = self.rReg.rBdy.x_off("BT")
            rrxLen = self.rReg.rBdy.rReg.xEnd() - self.rReg.xEnd()
            _, _, tMap, _, _, _, _, _ = c_genReg.singleRegime(self.rReg.rBdy.rReg, Del_h0, rrxLen, h0_T, h0_B, x_offs,
                                                              simuMode = "reduced",
                                                              whatAmI  = "3.regInBdyAdj")            
            t_B_Reg2_first = tMap[ 0, 0]
            t_T_Reg2_first = tMap[ 0,-1]

        #======================================================================
        elif  "R-R_fromRight" == howToSolve:
            
            # == Reg 3 (right) ========== -> is solft from elft to right!!!!
            rh0_B, rh0_T = self.h_bdy(Del_h0)
            x_offs       = self.x_off("BT")
            _, _, tMap, _, _, _, hMap, _ = c_genReg.singleRegime(self.rReg, Del_h0, rxLen, rh0_T, rh0_B, x_offs,
                                                              simuMode = "reduced",
                                                              whatAmI  = "1.regInBdyAdj")            
            
            # shear is stored to provide information to some second regs in bdy-adjustment (eg. OO, OR, RO)
            self.rReg.maps.update({'t':tMap})

            
            # == Reg 2 (left) ==========
            h0_B   = hMap[ 0, 0]
            h0_T   = hMap[ 0,-1]  
            
            x_offs = self.x_off("BT")
            _, _, tMap, _, _, _, hMap, _ = c_genReg.singleRegime(self.lReg, Del_h0, lxLen, h0_T, h0_B, x_offs,
                                                              simuMode = "reduced",
                                                              whatAmI  = "2.regInBdyAdj")            
            t_B_Reg2_first  = tMap[ 0, 0]
            t_T_Reg2_first  = tMap[ 0,-1]

            # shear is stored to provide information to some second regs in bdy-adjustment (eg. OO, OR, RO)            
            self.rReg.maps.update({'t':tMap})  


            # == Reg 1 (left-left) ==========
            h0_B   = hMap[ 0, 0]
            h0_T   = hMap[ 0,-1]
            x_offs = self.lReg.lBdy.x_off("BT")
            llxLen = self.lReg.xStart() - self.lReg.lBdy.lReg.xStart()
            _, _, tMap, _, _, _, _, _ = c_genReg.singleRegime(self.lReg.lBdy.lReg, Del_h0, llxLen, h0_T, h0_B, x_offs,
                                                              simuMode = "reduced",
                                                              whatAmI  = "3.regInBdyAdj")  
            t_B_Reg1_last = tMap[-1, 0]
            t_T_Reg1_last = tMap[-1,-1] 

            
        #======================================================================
        elif "2sites" in howToSolve:
            
            # Reg 1 (left):
            _, _, tMap, _, sMap, _, hMap, _ = c_genReg.singleRegime(self.lReg, Del_h0, lxLen, lh0_T, lh0_B, lx_offs,
                                                              simuMode = "reduced",
                                                              whatAmI  = "leftRegIn2SidesBdyAdj")          
            t_B_Reg1_last  = tMap[-1, 0]
            t_T_Reg1_last  = tMap[-1,-1] 
        
            # Reg 2 (right):
            _, _, tMap, _, _, _, _, _ = c_genReg.singleRegime(self.rReg, Del_h0, rxLen, rh0_T, rh0_B, rx_offs, 
                                                              simuMode = "reduced",
                                                              whatAmI  = "rightRegIn2SidesBdyAdj")              
            t_B_Reg2_first  = tMap[ 0, 0]
            t_T_Reg2_first  = tMap[ 0,-1]                   
        
        #======================================================================
        elif "fromLeft" in howToSolve:
            
            # Reg 1 (left):            
            _, _, tMap, _, _, _, hMap, _ = c_genReg.singleRegime(self.lReg, Del_h0, lxLen, lh0_T, lh0_B, lx_offs,
                                                              simuMode = "reduced",
                                                              whatAmI  = "1.regInBdyAdj")            
            t_B_Reg1_last  = tMap[-1, 0]
            t_T_Reg1_last  = tMap[-1,-1]

            # shear is stored to provide information to some second regs in bdy-adjustment (eg. OO, OR, RO)            
            self.lReg.maps.update({'t':tMap})
            
            
            # Reg2 (right):
            h0_B = hMap[-1, 0]
            h0_T = hMap[-1,-1]
            x_offs     = self.x_off("BT")
            _, _, tMap, _, _, _, _, _ = c_genReg.singleRegime(self.rReg, Del_h0, rxLen, h0_T, h0_B, x_offs, 
                                                              simuMode = "reduced",
                                                              whatAmI  = "2.regInBdyAdj")            
            t_B_Reg2_first = tMap[ 0, 0]
            t_T_Reg2_first = tMap[ 0,-1]               
        
        #======================================================================
        elif  "fromRight" in howToSolve:
            
            # Reg 2 (right):
            _, _, tMap, _, _, _, hMap, _ = c_genReg.singleRegime(self.rReg, Del_h0, rxLen, rh0_T, rh0_B, rx_offs,
                                                              simuMode = "reduced",
                                                              whatAmI  = "1.regInBdyAdj")            
            t_B_Reg2_first  = tMap[ 0, 0]
            t_T_Reg2_first  = tMap[ 0,-1]
            
            # shear is stored to provide information to some second regs in bdy-adjustment (eg. OO, OR, RO)            
            self.rReg.maps.update({'t':tMap})
            
            
            # Reg1 (left):
            h0_B = hMap[ 0, 0]
            h0_T = hMap[ 0,-1]
            x_offs     = self.x_off("BT")
            _, _, tMap, _, _, _, _, _ = c_genReg.singleRegime(self.lReg, Del_h0, lxLen, h0_T, h0_B, x_offs,
                                                              simuMode = "reduced",
                                                              whatAmI  = "2.regInBdyAdj")            
            t_B_Reg1_last = tMap[-1, 0]
            t_T_Reg1_last = tMap[-1,-1]          
              
        #======================================================================       
        else:
            self.x = x_orig # setting x_value back to original
            print("none is returned, unknown >howToSolve< = %s" %howToSolve)
            return None
     
        
    #=================================================================================
        
        # setting x_value back to original
        self.x = x_orig


    #=== Return Statement =====================================================

        # critical site: TOP or BOTH
        if   critSite == "Bot" or critSite == "B":
            return t_B_Reg1_last - t_B_Reg2_first
        
        # critical site: BOT
        elif critSite == "Top" or critSite == "T":
            return t_T_Reg1_last - t_T_Reg2_first
        
        # critical site: Both (OO > -O-O)
        elif critSite == "BT":
            return t_B_Reg1_last - t_B_Reg2_first # for BT -> bot is used!
        
        # else
        else:
            print("none is returned (no critSite)")
            return None     
             
        
        
        
        
        
        