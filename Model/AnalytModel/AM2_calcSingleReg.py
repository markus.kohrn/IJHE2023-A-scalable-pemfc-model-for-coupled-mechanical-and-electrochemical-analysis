
#
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 19 20:40:01 2022

@author: kohrn_simu
"""
from __future__ import division, absolute_import, \
                       print_function#, unicode_literals

# %matplotlib qt

''' import standard libraries'''                     
import numpy as np
import sys, os

''' imoprt own function classes '''

# swith cwd to parent directory, if not yet done
if os.path.split(os.getcwd())[1] == "AnalytModel":
    os.chdir("..")
    
import AnalytModel.AM2_calcGenericReg as c_genReg




'''
===============================================================================
                   recover uncorrected Parameters
===============================================================================
''' 


def get_dhdx_T0(reg, flipped, k, G):
    
    if not flipped: 
        
        if not reg.isInverted():
            dhdx_T0 = + reg.lBdy.lReg.maps['t'][-1,-1] /(k*G)
        else: # isInverted           
            dhdx_T0 = - reg.rBdy.rReg.maps['t'][ 0,-1] /(k*G)
        
    else: # if flipped
    
        if not reg.isInverted():
            dhdx_T0 = - reg.lBdy.lReg.maps['t'][-1, 0] /(k*G)
        else: # isInverted
            dhdx_T0 = + reg.rBdy.rReg.maps['t'][ 0, 0] /(k*G)
    
    # return statement
    return dhdx_T0


'''
===============================================================================
'''


def tryCorrect_h0_dhdx_T0(reg, flipped, h_T0, dhdx_T0, k, G):
    
    #====================
    # check if correction dict exists:
        
    if not reg.isInverted():
        maps_uncor = reg.lBdy.lReg.maps_uncor
    else:
        maps_uncor = reg.rBdy.rReg.maps_uncor
    
    if maps_uncor == None:
        # no uncorrected values, returning initial
        return  h_T0, dhdx_T0

    
    #====================
    # correction dict exist -> extract data

    if not flipped: 
        
        if not reg.isInverted():
            h_T0    = + maps_uncor["h"][-1,-1]
            dhdx_T0 = + maps_uncor["t"][-1,-1] /(k*G)
        else: # isInverted  
            h_T0    = + maps_uncor["h"][ 0,-1]        
            dhdx_T0 = - maps_uncor["t"][ 0,-1] /(k*G)
        
    else: # if flipped
    
        if not reg.isInverted():
            h_T0    = - maps_uncor["h"][-1, 0]
            dhdx_T0 = - maps_uncor["t"][-1, 0] /(k*G)
        else: # isInverted
            h_T0    = - maps_uncor["h"][ 0, 0]
            dhdx_T0 = + maps_uncor["t"][ 0, 0] /(k*G)
    
    
    #====================
    # return statement        
    
    return h_T0, dhdx_T0



'''
===============================================================================
                   Standard curves: Flat & Round
===============================================================================
'''

def bot_F(reg, h_B0, h_T0, x_off, x):
    ''' bottom: flat'''

    h_B    = h_B0
    t_B    = 0
    dtdx_B = 0   
    
    return h_B, t_B, dtdx_B

#==============================================================================

def top_F(reg, h_B0, h_T0, x_off, x):
    ''' top: flat '''

    h_T    = h_T0
    t_T    = 0
    dtdx_T = 0   
    
    return h_T, t_T, dtdx_T    

#==============================================================================

def bot_R(reg, h_B0, h_T0, x_off, x, inverted=False, flipped=False):
    
    # extracting data from parameter dictionary
    G   = reg.pD['G']
    k   = reg.pD['k']   
    
    if not flipped: 
        R_B = reg.pD['R_ribB']
        x_Boff = x_off["B"] 
    else:       
        R_B = reg.pD['R_ribT']
        x_Boff = x_off["T"]
    
    # correcting zero-height   
    h_B0   = h_B0 + (R_B - calc_w(x_Boff, R_B))

    # x-relativ    
    if inverted: x_rel = x_Boff - x
    else:        x_rel = x_Boff + x

    # calculate w       
    w_B    = calc_w(x_rel, R_B)

    # Calculate characteristic values    
    h_B    =   h_B0 - (R_B - w_B )
    t_B    = - k*G *         x_rel   / w_B
    dtdx_B = - k*G *(1/w_B + x_rel**2/(w_B**3))    

    return h_B, t_B, dtdx_B

#==============================================================================
    
def top_R(reg, h_B0, h_T0, x_off, x, inverted=False, flipped=False):
    
    # extracting data from parameter dictionary
    G   = reg.pD['G']
    k   = reg.pD['k']   
    
    if not flipped:
        R_T    = reg.pD['R_ribT']
        x_Toff = x_off["T"]
    else:
        R_T    = reg.pD['R_ribB']
        x_Toff = x_off["B"]

    # correcting zero-height   
    h_T0   = h_T0 - (R_T -calc_w(x_Toff, R_T)) # correction of h_T0 with offset 

    # x-relative   
    if inverted: x_rel = x_Toff - x
    else:        x_rel = x_Toff + x
    
    # calculate w
    w_T    = calc_w(x_rel, R_T)
    
    # Calculate characteristic values
    h_T    = h_T0 + (R_T - w_T )
    t_T    = k*G *         x_rel   / w_T
    dtdx_T = k*G *(1/w_T + x_rel**2/(w_T**3))
    
    return h_T, t_T, dtdx_T

#==============================================================================
    
def calc_w(x, R):
    return np.sqrt(R**2-x**2)

'''
===============================================================================
                   (1) Flat : Flat
===============================================================================
'''
def cReg_FF(reg, h_B0, h_T0, x_off, x):
    
    # ===Bot===
    h_B, t_B, dtdx_B = bot_F(reg, h_B0, h_T0, x_off, x)
    
    # ===Top===
    h_T, t_T, dtdx_T = top_F(reg, h_B0, h_T0, x_off, x)
    
    return h_B, h_T, t_B, t_T, dtdx_B, dtdx_T 

'''
===============================================================================
                   (2) Flat : Round
===============================================================================
'''
def cReg_FR(reg, h_B0, h_T0, x_off, x, flipped):
    
    # ===Bot===
    h_B, t_B, dtdx_B = bot_F(reg, h_B0, h_T0, x_off, x)
    
    # ===Top===
    h_T, t_T, dtdx_T = top_R(reg, h_B0, h_T0, x_off, x, flipped=flipped)

    return h_B, h_T, t_B, t_T, dtdx_B, dtdx_T 

'''
===============================================================================
                   (3) Flat : Open
===============================================================================
'''
def cReg_FO(reg, h_B0, h_T0, x_off, x, flipped):

    # ===Bot===
    h_B, t_B, dtdx_B = bot_F(reg, h_B0, h_T0, x_off, x)  

    # ===Top===
    
    #REGIME ZIEHT SICH DEN FALSCHEN T0, da es denkt, es ist ablösepunkt, stimmt aber nicht!!
    
    Ey = reg.pD['Ey']
    G  = reg.pD['G']
    H  = reg.pD['H_mea']
    k  = reg.pD['k']

    if not flipped: nT = reg.pD['calc_n'](reg.pD, "Top")
    else:           nT = reg.pD['calc_n'](reg.pD, "Bot")

    a   = h_B0 + H
    b   = k*G/Ey * c_genReg.F2_B(H/2,H,nT) # here the bottom-fkt must be used, 
                                           # because the integration direction 
                                           # is turned upside-down

    h_T    =      (h_T0-a)                  *np.exp(-x/np.sqrt(b)) + a
    t_T    = k*G *(h_T0-a) *(-1/np.sqrt(b)) *np.exp(-x/np.sqrt(b))    
    dtdx_T = k*G *(h_T0-a) *( 1/b)          *np.exp(-x/np.sqrt(b))
    
    return h_B, h_T, t_B, t_T, dtdx_B, dtdx_T 


'''
===============================================================================
                   (4) Round : Round
===============================================================================
'''
def cReg_RR(reg, h_B0, h_T0, x_off, x, flipped):
    
    # ===Bot===    
    h_B, t_B, dtdx_B = bot_R(reg, h_B0, h_T0, x_off, x, flipped=flipped)  
      
    # ===Top===
    h_T, t_T, dtdx_T = top_R(reg, h_B0, h_T0, x_off, x, flipped=flipped)
    
    return h_B, h_T, t_B, t_T, dtdx_B, dtdx_T 

'''
===============================================================================
                   (5) Round : - Round
===============================================================================
'''

def cReg_R_R(reg, h_B0, h_T0, x_off, x, xLen, flipped):
    
    # ===Bot===    
    h_B, t_B, dtdx_B = bot_R(reg, h_B0, h_T0, x_off, x, flipped=flipped)  
      
    # ===Top===
    h_T, t_T, dtdx_T = top_R(reg, h_B0, h_T0, x_off, x, inverted=True, flipped=flipped)  
    
    t_T    *= -1
    
    return h_B, h_T, t_B, t_T, dtdx_B, dtdx_T 



'''
===============================================================================
                   (6) Round : Open
===============================================================================
'''
def cReg_RO(reg, h_B0, h_T0, x_off, x, flipped):

    # ===Bot===    
    h_B, t_B, dtdx_B = bot_R(reg, h_B0, h_T0, x_off, x, flipped=flipped) 

    
# Top =========================================================================    
    
    # Calculation Parameters
    Ey  = reg.pD['Ey']
    G   = reg.pD['G']
    H   = reg.pD['H_mea']
    k   = reg.pD['k']
    
    # get R_B and x-offset
    if not flipped: 
        R_B    = reg.pD['R_ribB']
        x_Boff = x_off["B"]
    else: 
        R_B    = reg.pD['R_ribT']
        x_Boff = x_off["T"]
                                        
    # correction of h_B0 with offset                                   
    h_B0   = h_B0 + (R_B - np.sqrt(R_B**2-(x_Boff)**2)) 

    # y-integrals
    nB = reg.pD['calc_n'](reg.pD, "Bot")
    nT = reg.pD['calc_n'](reg.pD, "Top")
    
    F2_B = c_genReg.F2_T(H/2,H,nB)
    F2_T = c_genReg.F2_B(H/2,H,nT)
    
    # Calculation Constants
    a  = h_B0 - R_B + H
    bB = np.sqrt(k*G/Ey * F2_B)
    bT = np.sqrt(k*G/Ey * F2_T)
          

    # get dhdx_T0 from previouse regime
    dhdx_T0       = get_dhdx_T0(          reg, flipped,                k, G)
    h_T0, dhdx_T0 = tryCorrect_h0_dhdx_T0(reg, flipped, h_T0, dhdx_T0, k, G)
    

# = inner functions ===========================================================#
                                                                               #
    def w(z):                                                                  #
        return calc_w(z, R_B)                                                  #
                                                                               #
    def func1(z):                                                              #
        "integral 1 from z=0 -> z=x"                                           #
        z_corr = x_Boff + z                                                    #
        return np.exp(-z/bT) * (a + w(z_corr) + (bB*R_B)**2 / (w(z_corr)**3) ) #
                                                                               #
    def func2(z):                                                              #
        "integral 2 from z=0 -> x"                                             #
        z_corr = x_Boff + z                                                    #
        return np.exp(+z/bT) * (a + w(z_corr) + (bB*R_B)**2 / (w(z_corr)**3) ) #
                                                                               #
    def integrate(func, x_start, x_end):                                       #
        "integrating over <func> from x_start to x_end"                        #
                                                                               #
        try:    x_end = x_end[0]                                               #
        except: pass                                                           #
                                                                               #
        x_vec = np.linspace(x_start, x_end, 100)                               #
        y_vec = func(x_vec)                                                    #
                                                                               #
        integral = np.trapz(y_vec, x=x_vec)                                    #
                                                                               #
        return integral                                                        #
                                                                               #
# = END inner functions =======================================================#
    

    # calculate the integrals
    integral_1 = integrate(func1,0,x)
    integral_2 = integrate(func2,0,x)
    
    # calculate characteristic values    
    h_T    = 1/2         * (+ np.exp(+x/bT) * (h_T0 + dhdx_T0 * bT - integral_1 / bT )
                            + np.exp(-x/bT) * (h_T0 - dhdx_T0 * bT + integral_2 / bT ) )
    
    t_T    = 1/(2*bT)    * (+ np.exp(+x/bT) * (h_T0 + dhdx_T0 * bT - integral_1 / bT )
                            - np.exp(-x/bT) * (h_T0 - dhdx_T0 * bT + integral_2 / bT ) ) *k*G
    
    dtdx_T = 1/(2*bT**2) * (+ np.exp(+x/bT) * (h_T0 + dhdx_T0 * bT - integral_1 / bT )
                            + np.exp(-x/bT) * (h_T0 - dhdx_T0 * bT + integral_2 / bT )
                            - 2* (a + w(x+x_Boff) - (bB*R_B)**2 / w(x+x_Boff)**3)      ) *k*G


    
    
    # return statement
    return h_B, h_T, t_B, t_T, dtdx_B, dtdx_T 

'''
===============================================================================
                   (7) -Round : Open                                           - UNFINISHED!!!!
===============================================================================
'''


def cReg__RO(reg, h_B0, h_T0, x_off, x, xLen, flipped):
    

    # ===Bot===    
    h_B, t_B, dtdx_B = bot_R(reg, h_B0, h_T0, x_off, x, inverted=True, flipped=flipped)  
    
    t_B    *= -1
    
# Top =========================================================================    
    
    # Calculation Parameters
    Ey  = reg.pD['Ey']
    G   = reg.pD['G']
    H   = reg.pD['H_mea']
    k   = reg.pD['k']
    
    if not flipped: 
        R_B    = reg.pD['R_ribB']
        x_Boff = x_off["B"]
    else: 
        R_B    = reg.pD['R_ribT']
        x_Boff = x_off["T"]
        
        
                                        
    # correction of h_B0 with offset                                   
    h_B0   = h_B0 + (R_B - np.sqrt(R_B**2-(x_Boff)**2)) 

    # y-integrals
    nB = reg.pD['calc_n'](reg.pD, "Bot")
    nT = reg.pD['calc_n'](reg.pD, "Top")
    
    F2_B = c_genReg.F2_T(H/2,H,nB)
    F2_T = c_genReg.F2_B(H/2,H,nT)
    
    # Calculation Constants
    a  = h_B0 - R_B + H
    bB = np.sqrt(k*G/Ey * F2_B)
    bT = np.sqrt(k*G/Ey * F2_T)
        
    
#=============================    
    
    # get dhdx_T0 from previouse regime
    dhdx_T0       = get_dhdx_T0(          reg, flipped,                k, G)    
    h_T0, dhdx_T0 = tryCorrect_h0_dhdx_T0(reg, flipped, h_T0, dhdx_T0, k, G)


# = inner functions ===========================================================#
                                                                               #
    def w(z):                                                                  #
        z_corr = x_Boff - z    # rib curve is inverted                         #
        return calc_w(z_corr, R_B)                                             #
                                                                               #
    def func1(z):                                                              #
        "integral 1 from z=0 -> z=x"                                           #
        return np.exp(-z/bT) * (a + w(z) + (bB*R_B)**2 / (w(z)**3) )           #
                                                                               #    
    def func2(z):                                                              #
        "integral 2 from z=0 -> x"                                             #
        return np.exp(+z/bT) * (a + w(z) + (bB*R_B)**2 / (w(z)**3) )           #
                                                                               #                                                                                   #        
    def integrate(func, x_start, x_end):                                       #
        "integrating over <func> from x_start to x_end"                        #
                                                                               #            
        try:    x_end = x_end[0]                                               # 
        except: pass                                                           # 
                                                                               #            
        x_vec = np.linspace(x_start, x_end, 100)                               #     
        y_vec = func(x_vec)                                                    #
                                                                               #            
        integral = np.trapz(y_vec, x=x_vec)                                    #
                                                                               #        
        return integral                                                        #
                                                                               #
# = END inner functions =======================================================#
    
   
    # calculate the integrals
    integral_1 = integrate(func1,0,x)
    integral_2 = integrate(func2,0,x)
    
    # calculate characteristic values    
    h_T    = 1/2         * (+ np.exp(+x/bT) * (h_T0 + dhdx_T0 * bT - integral_1 / bT )
                            + np.exp(-x/bT) * (h_T0 - dhdx_T0 * bT + integral_2 / bT ) )
    
    t_T    = 1/(2*bT)    * (+ np.exp(+x/bT) * (h_T0 + dhdx_T0 * bT - integral_1 / bT )
                            - np.exp(-x/bT) * (h_T0 - dhdx_T0 * bT + integral_2 / bT ) ) *k*G
    
    dtdx_T = 1/(2*bT**2) * (+ np.exp(+x/bT) * (h_T0 + dhdx_T0 * bT - integral_1 / bT )
                            + np.exp(-x/bT) * (h_T0 - dhdx_T0 * bT + integral_2 / bT )
                            - 2* (a + w(x) - (bB*R_B)**2 / w(x)**3)                    ) *k*G
    
    # return statement
    return h_B, h_T, t_B, t_T, dtdx_B, dtdx_T 



'''
===============================================================================
                   (8) Open : Open
===============================================================================
'''
def cReg_OO(reg, h_B0, h_T0, x_off, x, xLen):  
    
    E  = reg.pD['Ey']
    G  = reg.pD['G']
    H  = reg.pD['H_mea']
    k  = reg.pD['k']

    nB = reg.pD['calc_n'](reg.pD, "Bot")
    nT = reg.pD['calc_n'](reg.pD, "Top")
    
    F1_B = c_genReg.F1_B(H/2,H,nB)
    F1_T = c_genReg.F1_T(H/2,H,nT)
    F2_B = c_genReg.F2_B(H/2,H,nB)
    F2_T = c_genReg.F2_T(H/2,H,nT)
    
    if reg.BT() == "OO":  # try for left regime
        dhdx_B0    =   reg.lBdy.lReg.maps['t'][-1, 0] /(k*G)
        dhdx_T0    =   reg.lBdy.lReg.maps['t'][-1,-1] /(k*G)
    elif reg.BT() == "-O-O": # go for right regime
        dhdx_B0    = - reg.rBdy.rReg.maps['t'][ 0, 0] /(k*G) 
        dhdx_T0    = - reg.rBdy.rReg.maps['t'][ 0,-1] /(k*G)
    else:
        print("Error in calcReg_OO: could not decide for picking t_B0/t_T0")
        sys.exit()

#================    
    
    del_h0 = (h_T0 - h_B0) - H
    z = F1_T / F1_B
    
    a = - del_h0 / (1 + 1/z)
    b = np.sqrt((z*F2_B-F2_T)/(z+1) * k*G/E)
    c = 1/2 *( (dhdx_B0 + dhdx_T0) - a/b * (1/z - 1) )
    d = h_B0 - a
    f = h_T0 + a/z    

    h_B    =        a           * np.exp(-x/b) + c * x + d
    t_B    = k*G* (-a/b         * np.exp(-x/b) + c         )
    dtdx_B = k*G* ( a/b**2      * np.exp(-x/b)             )
    
    h_T    =       -a      *1/z * np.exp(-x/b) + c * x + f
    t_T    = k*G* ( a/b    *1/z * np.exp(-x/b) + c         )
    dtdx_T = k*G* (-a/b**2 *1/z * np.exp(-x/b)             )
    
    return h_B, h_T, t_B, t_T, dtdx_B, dtdx_T 


#==============================================================================


