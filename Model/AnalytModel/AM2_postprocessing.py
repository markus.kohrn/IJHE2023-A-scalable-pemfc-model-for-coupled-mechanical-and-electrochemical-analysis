# -*- coding: utf-8 -*-
"""
Created on Fri Jan 28 15:19:38 2022

@author: kohrn
"""

from __future__ import division, absolute_import, \
                       print_function#, unicode_literals

# %matplotlib qt

''' import standard libraries'''
import math
import numpy                 as np
from   matplotlib.animation  import FuncAnimation
import matplotlib            as mpl
import copy
from   scipy                 import optimize
from   scipy.interpolate     import interp1d
from   scipy.ndimage.filters import gaussian_filter1d


'''
===============================================================================
                    Homogenize the plotList -> equal x-Distance
===============================================================================
''' 

def homogenize(allRegs, nrOfPoints=None, mode="default"):

#================================================================
    def do_homogenize(allRegs, mode, key='h'):                  #
                                                                #
        x = np.array(allRegs.reg(0).maps['x'])                  #
        y = np.array(allRegs.reg(0).maps[key])                  #
                                                                #
        for idx in range(1,allRegs.nrOfRegs()):                 #
            reg = allRegs.reg(idx)                              #
                                                                #
            try:
                
                x_new = copy.deepcopy(reg.maps['x'])                                                #
                y_new = copy.deepcopy(reg.maps[key])
                
                x[-1  ] = ( x[-1  ] + x_new[0  ] )/2
                y[-1,:] = ( y[-1,:] + y_new[0,:] )/2
                                                                
                x = np.concatenate((x, x_new[1:  ]))          #
                y = np.concatenate((y, y_new[1:,:]))          #   
                
            except:                                             #
                pass                                            #
                                                                #
        if   mode=="default":                                   #
            x1_vec = np.linspace(0,x[-1],int(nrOfPoints +1))    #
        elif mode=="coupledSim":                                #
            xLenElems = reg.geoDict["xLenElems"]                #
            x1_list   = [np.sum(xLenElems[0:idx])*10**6 for idx in range(len(xLenElems)+1)]
            x1_vec    = np.array(x1_list)                       #
                                                                #
        y1_list = []                                            #
                                                                #
        xElems, yElems = y.shape                                #
        for idx in range(yElems):                               #
            f  = interp1d(x,y[:,idx])                           #
            y1 = f(x1_vec)                                      #
            y1_list.append(y1)                                  #
                                                                #
        y1_arr = np.transpose(np.array(y1_list))                #
                                                                #
        return x1_vec, y1_arr                                   #
#================================================================
    
    x, h_hom = do_homogenize(allRegs, mode, key='h')
    x, u_hom = do_homogenize(allRegs, mode, key='u')  
    x, s_hom = do_homogenize(allRegs, mode, key='s')
    x, t_hom = do_homogenize(allRegs, mode, key='t')
    
    y = allRegs.reg(0).maps['y']
     
    maps_hom = {'x':x, 'y':y, 'h':h_hom, 'u':u_hom, 's':s_hom, 't':t_hom}
    
    return maps_hom

'''
===============================================================================
                    Smooth: based on physical correct behavioure
===============================================================================
'''     

def smooth_phys(allRegs, Del_h0, smMethod, sigma):
    
    # fail check: if results not yet homogenized
    if allRegs.maps_hom == None:
        print("Error: Results not yet homogenized")
        return False
    
    # Extracting data from allRegs
    x_list   = allRegs.xBdy_list()
    pD       = allRegs.pD
    
    x     = allRegs.maps_hom['x']
    h_old = allRegs.maps_hom['h']
    u_old = allRegs.maps_hom['u']
    s_old = allRegs.maps_hom['s']
    t_old = allRegs.maps_hom['t']

# Stress => smoothing method selection
    if   smMethod == "gaus":
        x, s = do_smooth_Gaus(allRegs, sigma, key='s')
    elif smMethod == "gaus+yBal":
        x, s = do_smooth_GausYbal(allRegs, sigma, key='s')
    elif smMethod == "gaus+yBal+xBdys":
        x, s = do_smooth_GausYbalXbdy(allRegs, sigma, key='s')
    
    del_s = s - s_old
  
    
# Lenght of one x and y element
    pD.update({'X_gdl':max(x_list)})
    xElems, yElems = del_s.shape
    xElemLen = pD['X_gdl']/(xElems-1)
    yElemLen = pD['H_mea']/(yElems-1)
    print("Total Nr of x-Elements: %d | x-Len of one element: %f" %(xElems, xElemLen)) # 1001 -> x-Direction
    print("Total Nr of y-Elements: %d | y-Len of one element: %f" %(yElems, yElemLen)) # 11   -> y-Direction


# Displacement  &  Heigth  
    u2    = np.copy(u_old)
    h2    = np.copy(h_old)

    for ix in range(xElems):
        for iy in range(1, yElems-1):
            #del_u[ix,iy] = (np.sum(del_s[ix,:iy]) - del_s[ix,0]/2 + del_s[ix,iy+1]/2) * yElemLen / pD['Ey']
            u2[   ix,iy] = (np.sum(    s[ix,:iy]) -     s[ix,0]/2 +     s[ix,iy+1]/2) * yElemLen / pD['Ey'] + u_old[ix,0]
            h2[   ix,iy] = u2[ix,iy] + iy * yElemLen - pD['H_mea']/2

# Shear    

    # === forward loop ===
    ds_dy_fw = np.zeros(t_old.shape)
    t_fw     = np.zeros(t_old.shape)
    
    for ix in range(xElems):
        for iy in range(yElems-1):
            ds_dy_fw       = (s[ix,iy+1] - s[ix,iy]) / yElemLen
            t_fw[ix,iy]    = - ds_dy_fw              * xElemLen + t_fw[ix-1,iy]
      
    # === backward loop ===
    ds_dy_bw = np.zeros(t_old.shape)
    t_bw     = np.zeros(t_old.shape)
    
    # prevents ix to be larger then xElems-1 (max value)
    ix_safe = lambda ix : ix if ix<xElems-1 else ix-xElems
    
    for ix in range(xElems):
        ix = xElems-1 - ix # ix = [xElems-1, 0]
        for iy in range(yElems-1):
            iy = yElems-1 - iy # iy = [yElems-1, 1]       
            ds_dy_bw       = (s[ix,iy-1] - s[ix,iy]) / yElemLen
            t_bw[ix,iy]    = - ds_dy_bw              * xElemLen + t_bw[ix_safe(ix+1),iy]
            
    # figures 1
#    yLim = [np.min(t_old), np.max(t_old)]
#    fig, axs = plt.subplots(4)
#    for ax in axs: ax.set_ylim(yLim)
#    axs[0].plot(x, t_old[ :,::10])
#    axs[1].plot(x, t_fw[  :,::10])
#    axs[2].plot(x, t_bw[  :,::10])

    # correction of deviation    
    corrVec_fw = copy.deepcopy(t_fw[-1, :])
    corrVec_bw = copy.deepcopy(t_bw[ 0, :])

    for ix in range(xElems):
        #print("ix=%d | fw = %f | bw= %f" %(ix, ix/(xElems-1), (xElems-1 - ix)/(xElems-1))) 
        for iy in range(yElems):    
            t_fw[ix,iy] -= corrVec_fw[iy] *             ix /(xElems-1)
            t_bw[ix,iy] -= corrVec_bw[iy] * (xElems-1 - ix)/(xElems-1)
            

    # merge forward and backward
    t_fwbw = np.zeros(t_old.shape)
    t_fwbw[:, 0         ] =  t_fw[:, 0       ]
    t_fwbw[:, 1:yElems  ] = (t_fw[:, 1:yElems] + t_bw[:, 1:yElems  ]) / 2
    t_fwbw[:,   yElems-1] =                      t_bw[:,   yElems-1]

    
    t = t_fwbw
         
    # Return    
    res_dict = {'x':x, 'h':h2, 'u':u2, 's':s, 't':t}      
    
    return res_dict


'''
===============================================================================
                   do smooth [1] - Gaussian : NO y-balancing
===============================================================================
'''
def do_smooth_Gaus(allRegs, sigma, key='s'):

    x        = np.array(allRegs.maps_hom['x'])
    y0_array = np.array(allRegs.maps_hom[key])    
    y1_array = np.zeros(y0_array.shape)

    xElems, yElems = y0_array.shape
    print("xElems = %d, yElems = %d" %(xElems, yElems))
   
    interval = int(8*sigma)
    
    for ix in range(xElems):  # x-element from where to take the stress
        for jx in range(ix - interval, ix + interval+1): # x-element to distribute the stress onto

            Del_idx_x     = ix-jx

            if   jx <  0:      jjx = jx + xElems
            elif jx >= xElems: jjx = jx - xElems
            else:              jjx = jx
            
            y1_array[jjx,:] += y0_array[ix,:] * (2 * math.pi * sigma**2)**(-0.5) * np.exp(-1 *  (Del_idx_x)**2 / (2*sigma**2))
    
    return x, y1_array

'''
===============================================================================
                   do smooth [2] - Gaussian : WITH y-balancing
===============================================================================
'''
def do_smooth_GausYbal(allRegs, sigma, key='s'):

    x        = np.array(allRegs.maps_hom['x'])
    y0_array = np.array(allRegs.maps_hom[key])  

# === Smooth ==========================
    
    _, y1_array = do_smooth_Gaus(allRegs, sigma, key)
     
# === Balance ==========================
    
    xElems, yElems = y0_array.shape
    
    # creating difference-array of y (new-old)
    del_y_array             = (y1_array - y0_array) / (yElems-1)
    del_y_array[:,0]        = del_y_array[:,0]        / 2
    del_y_array[:,yElems-1] = del_y_array[:,yElems-1] / 2
    
    # distribute difference in one element on all others in y-direction
    for ix in range(xElems):
        for iy in range(yElems):
            y1_array[ix,    :iy] -= del_y_array[ix,iy]
            y1_array[ix,iy+1:  ] -= del_y_array[ix,iy]

    # print average pressure
    print("p_mean = %7.4f MPa" %( y1_array.sum()/(xElems*yElems) ) )

#    for iy in range(yElems):
#        yFract = iy/(yElems-1)
#        print("yFract: %4.2f | sum(balancedLine) = %7.1f" %(yFract, sum(y2_array[:,iy])))
#        
# === Return ==========================
        
    return x, y1_array


'''
===============================================================================
                   do smooth [3] - Gaussian : WITH y-balancing & x-Boundaries
===============================================================================
'''
def do_smooth_GausYbalXbdy(allRegs, sigma, key='s'):

# === Create homogeniouse x-y-Arrays ===
    x_vec  = np.array(allRegs.maps_hom['x'])
    y0_arr = np.array(allRegs.maps_hom[key])
    y2_arr = np.zeros(y0_arr.shape)

    xElems, yElems = y0_arr.shape
    print("xElems = %d, yElems = %d" %(xElems, yElems))
    
    jx_interval = int(8*sigma)
   
# === Smooth ===========================
    
    # gather boundary arrays (bot/top)
    x_bdy_B_list, x_bdy_T_list  = allRegs.xSep_list()
    x_bdy_B_list = [0] + x_bdy_B_list + [x_vec[-1]]
    x_bdy_T_list = [0] + x_bdy_T_list + [x_vec[-1]]
    
    # initialize weight-arrays
    weight_B_arr = np.linspace(1,0,yElems)
    weight_T_arr = np.linspace(0,1,yElems)
    
    
    # originating index ix
    for ix in range(x_vec.size):
        
        w_B_reflx_vec = np.zeros(xElems)
        w_T_reflx_vec = np.zeros(xElems)
        
        x_bdy_BL, x_bdy_BR = get_xBdyLR(x_vec[ix], x_bdy_B_list)
        x_bdy_TL, x_bdy_TR = get_xBdyLR(x_vec[ix], x_bdy_T_list)
        
        # receiving index jx
        for jx in np.arange(ix - jx_interval, ix + jx_interval+1, 1):
            
            # Difference in x
            Del_x     = get_Del_x(ix, jx, x_vec)
            Del_idx_x = ix-jx
       
            # boundary reflection x-value
            x_B_reflx = get_xBdyReflex(x_vec, jx, x_bdy_BL, x_bdy_BR)
            x_T_reflx = get_xBdyReflex(x_vec, jx, x_bdy_TL, x_bdy_TR)
            
            # boundary reflection idx
            jx_B_reflx = find_nearest_idx(x_vec, x_B_reflx)
            jx_T_reflx = find_nearest_idx(x_vec, x_T_reflx)
            
            # start-end-overflow (after bdy reflection)
            jx         = overflow(jx,         xElems)
            jx_B_reflx = overflow(jx_B_reflx, xElems)
            jx_T_reflx = overflow(jx_T_reflx, xElems)
                   
            # add waight factor on result arrays    
            w_B_reflx_vec[jx_B_reflx] += (2 * math.pi * sigma**2)**(-0.5) * np.exp(-1 *  (Del_idx_x)**2 / (2*sigma**2)) 
            w_T_reflx_vec[jx_T_reflx] += (2 * math.pi * sigma**2)**(-0.5) * np.exp(-1 *  (Del_idx_x)**2 / (2*sigma**2)) 
        
        
        # creat intermediate
        y1_B_arr     = np.outer(w_B_reflx_vec, weight_B_arr)
        y1_T_arr     = np.outer(w_T_reflx_vec, weight_T_arr)
        
        # summ up bot and top weight array
        y1_tot_arr = (y1_B_arr + y1_T_arr) / 2
        
        # multiply in the vector of original y-values and summ-up to result vector
        y2_arr += y1_tot_arr * np.outer(np.ones(xElems), y0_arr[ix,:])
        
# === Balance ==========================
    
    # creating difference-array of y (new-old)
    del_y_arr             = (y2_arr - y0_arr) / (yElems-2) # it is -2 because the del_y is distributed on all elements EXCAPT the on the del_y originated from!
    del_y_arr[:,0]        = del_y_arr[:,0]        / 2
    del_y_arr[:,yElems-1] = del_y_arr[:,yElems-1] / 2
    
    # distribute difference in one element on all others in y-direction
    for ix in range(xElems):
        for iy in range(yElems):
            y2_arr[ix,    :iy] -= del_y_arr[ix,iy]
            y2_arr[ix,iy+1:  ] -= del_y_arr[ix,iy]

    
    print("p_mean = %7.4f MPa" %( y2_arr.sum()/(xElems*yElems) ) )

#    for iy in range(yElems):
#        yFract = iy/(yElems-1)
#        print("yFract: %4.2f | sum(balancedLine) = %7.1f" %(yFract, sum(y2_array[:,iy])))
#        

# === Return ==========================
        
    return x_vec, y2_arr

# helper functions

# === define class: getting bdys ========================
                                                        #
def get_xBdyLR(x, x_bdy_list):                          #
    for iBdy in range(1,len(x_bdy_list)):               #
        x_bdy = x_bdy_list[iBdy]                        #
        if x_bdy >= x:                                  #
            return x_bdy_list[iBdy-1], x_bdy_list[iBdy] #
    return x_bdy_list[0], x_bdy_list[1]                 #
                                                        #
# === overflow =========================================#
                                                        #
def overflow(j, jMax):                                  #
    if   j <  0:    return -j                           #
    elif j >= jMax: return jMax *2 - j-1                #
    else:           return j                            #
                                                        #
# === boundary reflexion ===============================#
                                                        #
def get_xBdyReflex(x_vec, idx, x_bdy_L, x_bdy_R):       #
                                                        #
    # calculate x with extrapolatin: avoiding overflow  #
    x = x_vec.max() / (x_vec.size-1) * idx              #
                                                        #
    if x < x_bdy_L:                                     #
        return x_bdy_L *2 - x                           #
    elif x > x_bdy_R:                                   #
        return x_bdy_R *2 - x                           #
    else:                                               #
        return x                                        #
#========================================================
                                                        #
def find_nearest_idx(array, value):                     #
    array = np.asarray(array)                           #
    idx = (np.abs(array - value)).argmin()              #
    return idx                                          #
                                                        #
#========================================================
                                                        #
def get_Del_x(ix, jx, x_vec):                           #
    return np.abs(ix - jx) / (x_vec.size-1) *x_vec.max()#
                                                        #
#========================================================  
